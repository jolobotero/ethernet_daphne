# This file is automatically generated.
# It contains project source information necessary for synthesis and implementation.

# Block Designs: bd/design_1/design_1.bd
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==design_1 || ORIG_REF_NAME==design_1} -quiet] -quiet

# IP: bd/design_1/ip/design_1_axi_ethernet_0_0/design_1_axi_ethernet_0_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==design_1_axi_ethernet_0_0 || ORIG_REF_NAME==design_1_axi_ethernet_0_0} -quiet] -quiet

# IP: bd/design_1/ip/design_1_fifo_generator_0_1/design_1_fifo_generator_0_1.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==design_1_fifo_generator_0_1 || ORIG_REF_NAME==design_1_fifo_generator_0_1} -quiet] -quiet

# IP: bd/design_1/ip/design_1_myip_ethernet_0_2/design_1_myip_ethernet_0_2.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==design_1_myip_ethernet_0_2 || ORIG_REF_NAME==design_1_myip_ethernet_0_2} -quiet] -quiet

# IP: bd/design_1/ip/design_1_write_fifo_0_1/design_1_write_fifo_0_1.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==design_1_write_fifo_0_1 || ORIG_REF_NAME==design_1_write_fifo_0_1} -quiet] -quiet

# IP: bd/design_1/ip/design_1_clk_wiz_0_0/design_1_clk_wiz_0_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==design_1_clk_wiz_0_0 || ORIG_REF_NAME==design_1_clk_wiz_0_0} -quiet] -quiet

# IP: bd/design_1/ip/design_1_GATE_NOT_0_0/design_1_GATE_NOT_0_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==design_1_GATE_NOT_0_0 || ORIG_REF_NAME==design_1_GATE_NOT_0_0} -quiet] -quiet

# IP: bd/design_1/ip/design_1_axi_ethernet_0_fifo_0/design_1_axi_ethernet_0_fifo_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==design_1_axi_ethernet_0_fifo_0 || ORIG_REF_NAME==design_1_axi_ethernet_0_fifo_0} -quiet] -quiet

# IP: bd/design_1/ip/design_1_xbar_0/design_1_xbar_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==design_1_xbar_0 || ORIG_REF_NAME==design_1_xbar_0} -quiet] -quiet

# IP: bd/design_1/ip/design_1_myip_ethernet_0_axi_periph_3/design_1_myip_ethernet_0_axi_periph_3.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==design_1_myip_ethernet_0_axi_periph_3 || ORIG_REF_NAME==design_1_myip_ethernet_0_axi_periph_3} -quiet] -quiet

# IP: bd/design_1/ip/design_1_rst_clk_wiz_0_200M_1/design_1_rst_clk_wiz_0_200M_1.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==design_1_rst_clk_wiz_0_200M_1 || ORIG_REF_NAME==design_1_rst_clk_wiz_0_200M_1} -quiet] -quiet

# XDC: bd/design_1/design_1_ooc.xdc
