# This file is automatically generated.
# It contains project source information necessary for synthesis and implementation.

# IP: E:/Proyectos_vhdl_vivado/ethernet_2/ethernet_2.srcs/sources_1/bd/design_1/ip/design_1_axi_ethernet_0_0/design_1_axi_ethernet_0_0.xci
# IP: The module: 'design_1_axi_ethernet_0_0' is the root of the design. Do not add the DONT_TOUCH constraint.

# Block Designs: e:/Proyectos_vhdl_vivado/ethernet_2/ethernet_2.srcs/sources_1/bd/design_1/ip/design_1_axi_ethernet_0_0/bd_0/bd_929b.bd
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==bd_929b || ORIG_REF_NAME==bd_929b} -quiet] -quiet

# IP: e:/Proyectos_vhdl_vivado/ethernet_2/ethernet_2.srcs/sources_1/bd/design_1/ip/design_1_axi_ethernet_0_0/bd_0/ip/ip_0/bd_929b_eth_buf_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==bd_929b_eth_buf_0 || ORIG_REF_NAME==bd_929b_eth_buf_0} -quiet] -quiet

# IP: e:/Proyectos_vhdl_vivado/ethernet_2/ethernet_2.srcs/sources_1/bd/design_1/ip/design_1_axi_ethernet_0_0/bd_0/ip/ip_1/bd_929b_mac_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==bd_929b_mac_0 || ORIG_REF_NAME==bd_929b_mac_0} -quiet] -quiet

# IP: e:/Proyectos_vhdl_vivado/ethernet_2/ethernet_2.srcs/sources_1/bd/design_1/ip/design_1_axi_ethernet_0_0/bd_0/ip/ip_2/bd_929b_pcs_pma_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==bd_929b_pcs_pma_0 || ORIG_REF_NAME==bd_929b_pcs_pma_0} -quiet] -quiet

# IP: e:/Proyectos_vhdl_vivado/ethernet_2/ethernet_2.srcs/sources_1/bd/design_1/ip/design_1_axi_ethernet_0_0/bd_0/ip/ip_3/bd_929b_xlconstant_phyadd_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==bd_929b_xlconstant_phyadd_0 || ORIG_REF_NAME==bd_929b_xlconstant_phyadd_0} -quiet] -quiet

# IP: e:/Proyectos_vhdl_vivado/ethernet_2/ethernet_2.srcs/sources_1/bd/design_1/ip/design_1_axi_ethernet_0_0/bd_0/ip/ip_4/bd_929b_xlconstant_config_vec_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==bd_929b_xlconstant_config_vec_0 || ORIG_REF_NAME==bd_929b_xlconstant_config_vec_0} -quiet] -quiet

# IP: e:/Proyectos_vhdl_vivado/ethernet_2/ethernet_2.srcs/sources_1/bd/design_1/ip/design_1_axi_ethernet_0_0/bd_0/ip/ip_5/bd_929b_xlconstant_config_val_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==bd_929b_xlconstant_config_val_0 || ORIG_REF_NAME==bd_929b_xlconstant_config_val_0} -quiet] -quiet

# IP: e:/Proyectos_vhdl_vivado/ethernet_2/ethernet_2.srcs/sources_1/bd/design_1/ip/design_1_axi_ethernet_0_0/bd_0/ip/ip_6/bd_929b_c_shift_ram_0_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==bd_929b_c_shift_ram_0_0 || ORIG_REF_NAME==bd_929b_c_shift_ram_0_0} -quiet] -quiet

# IP: e:/Proyectos_vhdl_vivado/ethernet_2/ethernet_2.srcs/sources_1/bd/design_1/ip/design_1_axi_ethernet_0_0/bd_0/ip/ip_7/bd_929b_c_counter_binary_0_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==bd_929b_c_counter_binary_0_0 || ORIG_REF_NAME==bd_929b_c_counter_binary_0_0} -quiet] -quiet

# IP: e:/Proyectos_vhdl_vivado/ethernet_2/ethernet_2.srcs/sources_1/bd/design_1/ip/design_1_axi_ethernet_0_0/bd_0/ip/ip_8/bd_929b_xlconstant_0_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==bd_929b_xlconstant_0_0 || ORIG_REF_NAME==bd_929b_xlconstant_0_0} -quiet] -quiet

# IP: e:/Proyectos_vhdl_vivado/ethernet_2/ethernet_2.srcs/sources_1/bd/design_1/ip/design_1_axi_ethernet_0_0/bd_0/ip/ip_9/bd_929b_util_vector_logic_0_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==bd_929b_util_vector_logic_0_0 || ORIG_REF_NAME==bd_929b_util_vector_logic_0_0} -quiet] -quiet

# XDC: e:/Proyectos_vhdl_vivado/ethernet_2/ethernet_2.srcs/sources_1/bd/design_1/ip/design_1_axi_ethernet_0_0/bd_0/bd_929b_ooc.xdc

# XDC: e:/Proyectos_vhdl_vivado/ethernet_2/ethernet_2.srcs/sources_1/bd/design_1/ip/design_1_axi_ethernet_0_0/design_1_axi_ethernet_0_0_board.xdc
# XDC: The top module name and the constraint reference have the same name: 'design_1_axi_ethernet_0_0'. Do not add the DONT_TOUCH constraint.
set_property DONT_TOUCH TRUE [get_cells U0 -quiet] -quiet

# XDC: e:/Proyectos_vhdl_vivado/ethernet_2/ethernet_2.srcs/sources_1/bd/design_1/ip/design_1_axi_ethernet_0_0/synth/design_1_axi_ethernet_0_0_ooc.xdc
# XDC: The top module name and the constraint reference have the same name: 'design_1_axi_ethernet_0_0'. Do not add the DONT_TOUCH constraint.
#dup# set_property DONT_TOUCH TRUE [get_cells U0 -quiet] -quiet

# IP: E:/Proyectos_vhdl_vivado/ethernet_2/ethernet_2.srcs/sources_1/bd/design_1/ip/design_1_axi_ethernet_0_0/design_1_axi_ethernet_0_0.xci
# IP: The module: 'design_1_axi_ethernet_0_0' is the root of the design. Do not add the DONT_TOUCH constraint.

# Block Designs: e:/Proyectos_vhdl_vivado/ethernet_2/ethernet_2.srcs/sources_1/bd/design_1/ip/design_1_axi_ethernet_0_0/bd_0/bd_929b.bd
#dup# set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==bd_929b || ORIG_REF_NAME==bd_929b} -quiet] -quiet

# IP: e:/Proyectos_vhdl_vivado/ethernet_2/ethernet_2.srcs/sources_1/bd/design_1/ip/design_1_axi_ethernet_0_0/bd_0/ip/ip_0/bd_929b_eth_buf_0.xci
#dup# set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==bd_929b_eth_buf_0 || ORIG_REF_NAME==bd_929b_eth_buf_0} -quiet] -quiet

# IP: e:/Proyectos_vhdl_vivado/ethernet_2/ethernet_2.srcs/sources_1/bd/design_1/ip/design_1_axi_ethernet_0_0/bd_0/ip/ip_1/bd_929b_mac_0.xci
#dup# set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==bd_929b_mac_0 || ORIG_REF_NAME==bd_929b_mac_0} -quiet] -quiet

# IP: e:/Proyectos_vhdl_vivado/ethernet_2/ethernet_2.srcs/sources_1/bd/design_1/ip/design_1_axi_ethernet_0_0/bd_0/ip/ip_2/bd_929b_pcs_pma_0.xci
#dup# set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==bd_929b_pcs_pma_0 || ORIG_REF_NAME==bd_929b_pcs_pma_0} -quiet] -quiet

# IP: e:/Proyectos_vhdl_vivado/ethernet_2/ethernet_2.srcs/sources_1/bd/design_1/ip/design_1_axi_ethernet_0_0/bd_0/ip/ip_3/bd_929b_xlconstant_phyadd_0.xci
#dup# set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==bd_929b_xlconstant_phyadd_0 || ORIG_REF_NAME==bd_929b_xlconstant_phyadd_0} -quiet] -quiet

# IP: e:/Proyectos_vhdl_vivado/ethernet_2/ethernet_2.srcs/sources_1/bd/design_1/ip/design_1_axi_ethernet_0_0/bd_0/ip/ip_4/bd_929b_xlconstant_config_vec_0.xci
#dup# set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==bd_929b_xlconstant_config_vec_0 || ORIG_REF_NAME==bd_929b_xlconstant_config_vec_0} -quiet] -quiet

# IP: e:/Proyectos_vhdl_vivado/ethernet_2/ethernet_2.srcs/sources_1/bd/design_1/ip/design_1_axi_ethernet_0_0/bd_0/ip/ip_5/bd_929b_xlconstant_config_val_0.xci
#dup# set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==bd_929b_xlconstant_config_val_0 || ORIG_REF_NAME==bd_929b_xlconstant_config_val_0} -quiet] -quiet

# IP: e:/Proyectos_vhdl_vivado/ethernet_2/ethernet_2.srcs/sources_1/bd/design_1/ip/design_1_axi_ethernet_0_0/bd_0/ip/ip_6/bd_929b_c_shift_ram_0_0.xci
#dup# set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==bd_929b_c_shift_ram_0_0 || ORIG_REF_NAME==bd_929b_c_shift_ram_0_0} -quiet] -quiet

# IP: e:/Proyectos_vhdl_vivado/ethernet_2/ethernet_2.srcs/sources_1/bd/design_1/ip/design_1_axi_ethernet_0_0/bd_0/ip/ip_7/bd_929b_c_counter_binary_0_0.xci
#dup# set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==bd_929b_c_counter_binary_0_0 || ORIG_REF_NAME==bd_929b_c_counter_binary_0_0} -quiet] -quiet

# IP: e:/Proyectos_vhdl_vivado/ethernet_2/ethernet_2.srcs/sources_1/bd/design_1/ip/design_1_axi_ethernet_0_0/bd_0/ip/ip_8/bd_929b_xlconstant_0_0.xci
#dup# set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==bd_929b_xlconstant_0_0 || ORIG_REF_NAME==bd_929b_xlconstant_0_0} -quiet] -quiet

# IP: e:/Proyectos_vhdl_vivado/ethernet_2/ethernet_2.srcs/sources_1/bd/design_1/ip/design_1_axi_ethernet_0_0/bd_0/ip/ip_9/bd_929b_util_vector_logic_0_0.xci
#dup# set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==bd_929b_util_vector_logic_0_0 || ORIG_REF_NAME==bd_929b_util_vector_logic_0_0} -quiet] -quiet

# XDC: e:/Proyectos_vhdl_vivado/ethernet_2/ethernet_2.srcs/sources_1/bd/design_1/ip/design_1_axi_ethernet_0_0/bd_0/bd_929b_ooc.xdc

# XDC: e:/Proyectos_vhdl_vivado/ethernet_2/ethernet_2.srcs/sources_1/bd/design_1/ip/design_1_axi_ethernet_0_0/design_1_axi_ethernet_0_0_board.xdc
# XDC: The top module name and the constraint reference have the same name: 'design_1_axi_ethernet_0_0'. Do not add the DONT_TOUCH constraint.
#dup# set_property DONT_TOUCH TRUE [get_cells U0 -quiet] -quiet

# XDC: e:/Proyectos_vhdl_vivado/ethernet_2/ethernet_2.srcs/sources_1/bd/design_1/ip/design_1_axi_ethernet_0_0/synth/design_1_axi_ethernet_0_0_ooc.xdc
# XDC: The top module name and the constraint reference have the same name: 'design_1_axi_ethernet_0_0'. Do not add the DONT_TOUCH constraint.
#dup# set_property DONT_TOUCH TRUE [get_cells U0 -quiet] -quiet
