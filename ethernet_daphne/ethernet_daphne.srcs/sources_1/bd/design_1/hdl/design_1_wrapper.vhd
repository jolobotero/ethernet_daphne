--Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
--Date        : Tue Jul  6 22:47:36 2021
--Host        : DESKTOP-QTM1A6I running 64-bit major release  (build 9200)
--Command     : generate_target design_1_wrapper.bd
--Design      : design_1_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_wrapper is
  port (
    clk_100MHz : in STD_LOGIC;
    diff_clock_rtl_1_clk_n : in STD_LOGIC;
    diff_clock_rtl_1_clk_p : in STD_LOGIC;
    mdio_rtl_0_mdc : out STD_LOGIC;
    mdio_rtl_0_mdio_io : inout STD_LOGIC;
    reset_rtl_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    rst_0 : in STD_LOGIC;
    sgmii_rtl_0_rxn : in STD_LOGIC;
    sgmii_rtl_0_rxp : in STD_LOGIC;
    sgmii_rtl_0_txn : out STD_LOGIC;
    sgmii_rtl_0_txp : out STD_LOGIC
  );
end design_1_wrapper;

architecture STRUCTURE of design_1_wrapper is
  component design_1 is
  port (
    rst_0 : in STD_LOGIC;
    clk_100MHz : in STD_LOGIC;
    mdio_rtl_0_mdc : out STD_LOGIC;
    mdio_rtl_0_mdio_i : in STD_LOGIC;
    mdio_rtl_0_mdio_o : out STD_LOGIC;
    mdio_rtl_0_mdio_t : out STD_LOGIC;
    reset_rtl_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    sgmii_rtl_0_rxn : in STD_LOGIC;
    sgmii_rtl_0_rxp : in STD_LOGIC;
    sgmii_rtl_0_txn : out STD_LOGIC;
    sgmii_rtl_0_txp : out STD_LOGIC;
    diff_clock_rtl_1_clk_n : in STD_LOGIC;
    diff_clock_rtl_1_clk_p : in STD_LOGIC
  );
  end component design_1;
  component IOBUF is
  port (
    I : in STD_LOGIC;
    O : out STD_LOGIC;
    T : in STD_LOGIC;
    IO : inout STD_LOGIC
  );
  end component IOBUF;
  signal mdio_rtl_0_mdio_i : STD_LOGIC;
  signal mdio_rtl_0_mdio_o : STD_LOGIC;
  signal mdio_rtl_0_mdio_t : STD_LOGIC;
begin
design_1_i: component design_1
     port map (
      clk_100MHz => clk_100MHz,
      diff_clock_rtl_1_clk_n => diff_clock_rtl_1_clk_n,
      diff_clock_rtl_1_clk_p => diff_clock_rtl_1_clk_p,
      mdio_rtl_0_mdc => mdio_rtl_0_mdc,
      mdio_rtl_0_mdio_i => mdio_rtl_0_mdio_i,
      mdio_rtl_0_mdio_o => mdio_rtl_0_mdio_o,
      mdio_rtl_0_mdio_t => mdio_rtl_0_mdio_t,
      reset_rtl_0(0) => reset_rtl_0(0),
      rst_0 => rst_0,
      sgmii_rtl_0_rxn => sgmii_rtl_0_rxn,
      sgmii_rtl_0_rxp => sgmii_rtl_0_rxp,
      sgmii_rtl_0_txn => sgmii_rtl_0_txn,
      sgmii_rtl_0_txp => sgmii_rtl_0_txp
    );
mdio_rtl_0_mdio_iobuf: component IOBUF
     port map (
      I => mdio_rtl_0_mdio_o,
      IO => mdio_rtl_0_mdio_io,
      O => mdio_rtl_0_mdio_i,
      T => mdio_rtl_0_mdio_t
    );
end STRUCTURE;
