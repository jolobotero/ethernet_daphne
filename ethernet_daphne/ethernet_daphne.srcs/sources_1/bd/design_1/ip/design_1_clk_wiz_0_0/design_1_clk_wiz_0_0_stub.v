// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Tue Jul  6 23:34:40 2021
// Host        : DESKTOP-QTM1A6I running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               e:/Proyectos_vhdl_vivado/ethernet_2/ethernet_2.srcs/sources_1/bd/design_1/ip/design_1_clk_wiz_0_0/design_1_clk_wiz_0_0_stub.v
// Design      : design_1_clk_wiz_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7a200tfbg676-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
module design_1_clk_wiz_0_0(clk_200M, clk_65M, clk_120M, reset, clk_in1)
/* synthesis syn_black_box black_box_pad_pin="clk_200M,clk_65M,clk_120M,reset,clk_in1" */;
  output clk_200M;
  output clk_65M;
  output clk_120M;
  input reset;
  input clk_in1;
endmodule
