-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Tue Jul  6 22:57:48 2021
-- Host        : DESKTOP-QTM1A6I running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim
--               e:/Proyectos_vhdl_vivado/ethernet_2/ethernet_2.srcs/sources_1/bd/design_1/ip/design_1_GATE_NOT_0_0/design_1_GATE_NOT_0_0_sim_netlist.vhdl
-- Design      : design_1_GATE_NOT_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a200tfbg676-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_GATE_NOT_0_0 is
  port (
    NOT_IN : in STD_LOGIC;
    NOT_OUT : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_1_GATE_NOT_0_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_1_GATE_NOT_0_0 : entity is "design_1_GATE_NOT_0_0,GATE_NOT,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_1_GATE_NOT_0_0 : entity is "yes";
  attribute ip_definition_source : string;
  attribute ip_definition_source of design_1_GATE_NOT_0_0 : entity is "module_ref";
  attribute x_core_info : string;
  attribute x_core_info of design_1_GATE_NOT_0_0 : entity is "GATE_NOT,Vivado 2019.1";
end design_1_GATE_NOT_0_0;

architecture STRUCTURE of design_1_GATE_NOT_0_0 is
  signal \^not_in\ : STD_LOGIC;
begin
  NOT_OUT <= \^not_in\;
  \^not_in\ <= NOT_IN;
end STRUCTURE;
