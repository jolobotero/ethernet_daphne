-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Tue Jul  6 23:25:19 2021
-- Host        : DESKTOP-QTM1A6I running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim
--               e:/Proyectos_vhdl_vivado/ethernet_2/ethernet_2.srcs/sources_1/bd/design_1/ip/design_1_axi_ethernet_0_0/bd_0/ip/ip_6/bd_929b_c_shift_ram_0_0_sim_netlist.vhdl
-- Design      : bd_929b_c_shift_ram_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a200tfbg676-1
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2019.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
klQ5h9hgPT4p1YBkLUYG8lguTKRhX0z2xODTfvC0iZ4ANS2ufXQ4XxirL31R7wyoNNR1hd7rMjwq
bhFP3bRYRg==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
m0Wn8/fGtoK5/3nNxNBBvNhfylkjw8jAoTERkIzypaD+FUTSQmf52TpAbVY1LPDo/g6d8tzIrR8n
vZ2G5ckAbcwDRGFF5wpZ/kzyANCTceWo7S+WbzLbzhWkG5fkJbJk/tafbSvqruxodmgtrUI2IZDv
n7Mrf4GRIdC1iTgv+oM=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
mC2bSkADVOO0t7I1BTHPnOuMcPIdeypycvBkfsUtXFD/WSov2Pt6qF5xMTcs6woq+465yEDvey00
mpX9JC1ypEuHaZI+dtH7s+A4vYLXy34kFLryTKJtqycAy6XqNtTegvy4giaDFwDn7LgsamHSf/4D
cVGL06Ul6gm+ZOMvzok6Y9TBltlEkZOZ4GncKspHPcn3EJar4GdDxKcPll1RqSFbA2m7bWz6jPV3
uOD6IWnIflyobdLIsuB8hse3kjXBG5sl6X8AP4armpo9TNNnRIRG2FoI6g16Sj6zldS9XF54ge/w
jnsP0vce7kfzvP1LjtFrWdW0raW61dIddZVz5A==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
LC8srWyP8+H+elFwWxNw8UhEF94Tx3vV4OlwBcwFszd9xuvwYRipLEwY4ar7HEoCwJI7beW6k6I1
ntJSXGEnPMgIDe7PadSQIz6gi9ErRHWRhWD6bEDNd/ZjFQuOakXk78jzxOTKuOpQ+ExjdHImyPzK
mDR0BnkVc/QiSO7zpOf3yjx9Vzb0Fba6OgXg0iLPz1MUr0ahYIc0MAkw4pRBo6IH5ilqVbkgNQcT
G4z/0OSzsBxipE9o1elAez7CnehtEQ66g6i3JpIIwqwnpZNQ9otSnS8LN/spRcQLOUuBq9ynGkPL
sF7EUF1gLkTFFMGgnZgawWi0V0jhiHQkJuLKPQ==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_02", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
ijV0yStg7uRIl3uzK8/hlbIaWGHa9aPC5Eu/o1vErrwtArYsGFt3RCyG/S90FB6jkuLgqwPR8ZlQ
P9t/F2FWmEkwwjGbdrRKFfpbkjh5HVn0vvLKCP3SiVHXCOWxxb5z8BV+yCNdpgdnsHFecK1M8ydQ
C530kRu3UD1LcnZcWJi41LcJAc5rvlw/SP1gbl+I1qsRNEHsb+MK5vyjgwBZAqKyqi7/UK1VEPdq
myeWeCRrU0GqEq5y/PHBMknv1SqNe0d5qzG1rmAtC4df+iivMCc9xuHsCA7iqoe+ZKnMmnA/8F6+
nY+gx8AQNplCeFxWppKH952fIYplHtE2rKQyVQ==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
saHlQAxOuJkLItpv5N3fJoJP3EdR34QNYHtUBFx7fcQy8HAUaZ/9Tlt2kfpn9r/pk1MmJ7uf4z73
LyaG/PWw7v3yH/KIVSLeIxiaSHoVfdWH1/RDH6k3DTDNbWSJDOjGSvMTThUKe899F8IXTD5jKxCh
frGB3Io0W4klQV7ADEw=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
RFiy0rrSiVrZMC725S3XkUSDuMS7Z+xog1sOwADnIBmdCChtYLUQVO9oPjXIujfg3bBFTH3qzII3
Y63fFMSZnnzk+ups3RPMBqEPPdMQSwlpDzsvhCOwYHe/rsOPZvqd7lL6QOKoA3mS9TZIP+mOaTKd
vzQiVyfS0rs+QHdJcw49jD5y7Dc3clQHD9xZMAMUHa5v8hzX4IOcnkSUOLpsZptR/WM7rcMnTzCL
x6m2UC+xSrwrb5vEz9cePMHx9NJyO2DBG0HTEDzGQQkrOCJJjvBxtB3r/E7/3eWGHvMwphSJnfCd
PfvYXz88X+ZVCW9dTNfc75jI7ogVMSunmtUuMw==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
jZWmI+mVC4/bTncBAkIoG/PRQsjvdb8ZZOsQNiJJ2IzacnV3keY46rZ/P4UUHNDQRCyBjNinMDLx
QakOjNCIn/LX9ZAxrmfqth4YpuFpxLP9SikzgvJMevGlRFNVtaZSqUXxd5WhvBm3EQTmThPYDi7H
UHZyCROB8iZBMBcSjImjRGfCiKoC9V0zbsFU4ZZ9rdqtKPYmJi4Y2+he6JkIOKBfO6HJX2Vfu0Pt
pALoElDYzB1a2XrnUtQwD70OYwLadJ6no0uorPwBZicqd70WMOhGeSxhDl4whZ+D2A48t5gv0LSD
QR8YbYxJPWf3+KkLJWL7CVQ8ebG8oGIdTqDuAQ==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
R2h/nacdlQin0lYnqUgLbpalRkarFpEWnV/uLTXGccLG2Uk48MNdSM6CMd1XMAtw0KE6xcX+UQZc
G8L5MUv0pZOvgRdH9CzI7mL7sReIwePMQTkuQ4dy+pTHdw0mvuBZHI4TEyTEuhEKzLdWuaJ0lAol
z/TA+hWznGKf8LqbLttBIHMbXDWvfFj/T7McJ+o4Hy+Dw7MJWCq44dXOQvRaWfIhQsUctJh+cXiZ
57JXxXCtuQ4FU+en4Tv++2ARd+m9YZM7OycwDjgRHP55Ct0JKSEKvKgo3rGcBQnKcVqcKHAgNSNS
7RGGlX1BY7pW4eHqwZBClps4X9CS1NBaVAgYig==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 13376)
`protect data_block
e1fBwzOeMNfrbTsGT214mtvSykIxNVmmErd2wVVJIRocZs6fRSHDKBA14U7gxCN2CQv0QljD16i0
+WACKcW6fh8A3J0WPyvcGCxMWbFe0/g0y7lQDq4n/Ncy/qkk9fzpIFlpsVa8PgOxEUliCVzajc3i
olUksmAdc8GAOgSilmcHI1zAUK7OMOtxjbEXlK+6EGEN1sXo9eXjUwqRLD2GkweiLrKgkjghUSey
n2iLiorNa0SKsllATAxzexQhwvc2TYapo0i16giyBeuAsw5fsi69DPeTmxOKsXsEVmbpuU/HCmVN
zoKqG1VQLIRint9DfKaoXpPsrtWJJMdN3POCGVm+8JNrNb28kigTufByA2La3sJmAegD5DRA+7o2
kiz9oA4AKgTcDcaEebgT75px4ziHQmUckLpR61ZMe+Ttvc8Tq6NZDgtHeS8s/zH7VJdP0Jnm9MXO
YeuXdc1i/VPN3DBj9gjpjGO2risZU+GU1zl4mt1R//QzpLl0TbL/H7eo4MAUJ+CrrL4+kwBsC7Xs
yDKI5r5/fjqLdRuq3z72Vk70qwU20w+v8dZgLahmdZfT+gJRu/mJszoAUDW6kR+771Q1Btk2lvoI
WjquUSGCYI8lGahpxOpCGl2Ua0yMsU03Ac4Uj6ohNGrWOWgarS9aEJeUC9UGOlRjo5jkvQY5YhmY
7NpBzgZwa7GlwobPtZrBj7JXDLcDctGDyqgDsnGuTW6BBhS9Q/Tf33FrB/Qu/1m4mPJG3uAV3vPS
xmdt0MT4arR77tpfyK//8IlX6LYtaPG5+Lqf/IQxVXa+rB9Sv+NM74QRt9LzTMiyGQUThM/Pc0cC
MqwjasFkSCuejaWBFHb1gEQmgWrbuO5tYwypCq5IjpkVzX76E+vcVcgVDJv3WdwluyPxjDMp2Qak
KbXM/fj9gyaruZBnGKRnIT4g0YJTi4cxxlDZPOCTaiJTeZcvlQXpO6ffdso7e7RJmmmQ8R1eOIhR
o2zgznQAK9JWVnHqEeU7cIqyrJpJqiwtHDI+anDzfjXfj5It8XBnwALeCOnQK+T7fhaBV2qwCcDJ
g5fRwpd2ONXvkim54illGl4qeRS/0BfTEKE03QogU8f3EbwTE3YuYvWUG+U7r2StcQXwHz7TJR4m
ko8FJDmHuH5myB0EpVabqna7avUytYmzbw84xWsKW1glCifZsM3avKQzYN270WuRlDcXIFUTH2iQ
XQsFpzMmCILBIEu8LIQrM4exJwQ4N//xOqI2yKmek7KWEVhiQzK7G9sJCx2fxNGsdRm5cR9/t63J
1UxIHMoTZiB3+yiJsJvwCd72GVKVklGTdKvW2pPHsuIxNKPCEgyTUoDe6rSm4zQUKYMCjwl05Qqb
2i/29KMqiEzzb27Go/flBlgZKV18/WGo2chx0MnAqzvte3HYV3HjsW5AWRp31egX6Dt2BNb9GruP
xofP5Z3rV1CTCsZdbdMvQZUKFamWC73fiD6WWufcSG8ceIKLDIousKBkwfRmUrWkEqPQGRobCKyf
10HMtTPx3I5XvVSBbIbSXV+17BEl4MsrfcrapzTvJ0FNRJ0gbfdvfF2bz/nvC3T6ykiQEGX9GyqB
bHvndncncP1mhmp5zDVnaRAcbYXPA0dVp7jdpfJ1kiCxcw9+0+xxFaNzZRugvmEg5VHitYR328G5
o1A0rRPb3VhC6HiVLKTwA/qoEYZybx6z3J9UBb2yruuFhe+pcpNoX8A2kaTP4J4RyxJ1doQdHNzj
RsmgxmYq8uVLGjLG2SC6Ur8+BI3gQSB6yCqFsNC1qj4DOuzqDMZs6RLloCfmAf19S9+E716t7UGN
Z8+SJDq5aHV0mAiauVF991aFJTYFAcLmVGtruSSGi8hU49EYop+k+6/rbUSqFcLwm/BEoamEe+at
/tEtT4rV2sjhGnIyo4voCRUbyasiE9Xq1WmRQtZ1janictzOBEJkCfzK/EhlkwCJvwVxXOPXxMil
3GcdYpXOgIT6U/odXjYpeO26JyO6NLevjYCYvvQ+jlRjYuegAS0RPlg7jazhgFRlpvAWRnX26Ygz
jQs8JMBkJcQ6Ry01MQp9XZwocFjLc5RrOwESWsoxDHt0Piep5X6Dg41Aysrk5fPM0KSmxwr4pG4P
tZtVcNAd8JkuP98NRlzZn4WHSLSkW8EMaGHBVbvfYT65tdDAdbwbWrE60IUPD3iOzXRr0Rs0M7vg
rcHZ9W/G6FPLbIuhL6qPmNemlQTv0Bg0ljgXLJunhSANgDzZPXw7Qjs2fDriBTn6kPO0bTV2HTbF
jkgnOVZIky+VWEwuz55b09gAHxmc1uEtLBQSyXUU2nzNMHiewwfMEVySQoBmn/61NrrbOP/FzHxA
8g/KnxGadknhGJklDBOkpjPB7ZP0O4CnZHb7vqc0/6gLbgBL6MdoYA872anw3xKyM3ZJFAUu3Sf+
vLv1NMbu6JbRevAopb7M9GvUdECMedvFdZYuaEj7IQvtGW4OwHQ0wcV2qQqA8Xaput7SZfncXQBM
Jv79r7CPN7oZB+4iTyTCnHBOrpwAw+eQC+OPHqu5kNUpCXtQVgC69aI4F+HkqPx6pXnuTu1u7jOB
IJIrAtpEP2ATSvAIMbM8INzKxkEGWh90f98rNN18aPXbPrmlZeYwZkhiRkk9NQmSWxmuEYqXy8qd
ymIVb/mdaLwZAKSEfpNrKZnLXg8aNUqkCNGNWWDvOKxA5DiufVDOZ82K7LNeM658JW8AAT/tbpuo
uDstzraq2AaYmVpC3lZV5nkGxF1iOUv5Y7k0rPJcnN6sEXdyNrZlcgtCnKF/BaOf7LbxLkljhH9Q
ZwtaTyPnJNrKrG8MSelNME7PhIUXESgGPOB1wGBANXUQ6R/TrqbKHlUzvvZt8L72W9hSTpVeX7G5
iDyvgMyeCYKC6ujzY5ysv1egmQc4oyr2KUEb67sMAFrRR0Z2C0b7/T+qSGrOTmpsZQSq96YOOihj
f8p6cpZbDres0rdQxLu8l+kx3ygQv5JK5Hdgjiw1VMMaExAdttRrZmj0tIsk35ELRthbNx8FwHof
1zh9j2MDXpboJa4Fd/MPkTTbG15ROdhnAor1RHOLYWhZJKBAMDMfhye/cwZ1mp+dcd53ha7o+W9V
1pE6QbGh9fO/jjH+QPm23dHbGIUwDd1Q/az/wleW7OoHxPpuVryTmIhbSpccapjBTk6UFCstOaNC
hZZIPBsG1KOLzLAdlxxRYYajFd2iAA8h+cSphhATPXVQLjKWeZmo8r81gCK7arg2goup8d3CII3N
xScPjCMCHWQdGwge70lbmuQ1Hvh6aGx4iaaz50XCKEcwkpyiYLwpZuRt9+VC2/43GTJ/2EecYrQW
ViGKSiectRwpBQhaV7SmAgos/SMqP7hEGcvay/Vb+l2jzce+cZYbjFLE/X94xut+9ZswOG0IltzP
KDvyOirz+9ctfTT8VkjelBaEuDku+C4UizjxKb7P6QuOINdZs0g05uPRGZh3FO/Rp7hrm2vJ66dN
0NksiX5FkBCuF98I0QPUHOMfIXqIaNSsGHsq2kCDy5qOsTadM7h8SBq8qYg8la1CIMf1yPGJ2RVB
STqhjVMcn8NnfEWa8Vz6qe7aGVet28HWYlu48bhWh9jsGAS4Sc3Z6bFAgXT6DOtljOkvOftXfk4Z
aE9gXGawJ7hjeclYxgqIHUfAETu1pRk1+cDBlvPsD7PVaWylOUql4jMg+gGkPVU5axPUaKRxDLMa
veoOSwcCMl0JajEsgSU/qVveOBVHHQC0tnRLSymlQhO+QQ49OKgYqg0En+FEdvUbeRviJ6bRZrpk
ak/Ix5/GdvnzHItp/WtDGVLY6I/U6DPObjbCjEialwHnbjxZSfC3K5wiFUJCAEwWQUhbJ5Am19Kb
R6JfMr3Ey8z/xDzgHt/JnPs0xt1cJl0k+Inz9ly67ywccNc3l9FBfiFk1bgqRIRSICxZuTkvZ4jh
7sRsZkv2+MpsaSx93qXDGF+64wpg7anIxg5Az6QpcXjOmj46GyAip3drqvsFFrPx7okIeogk+W6F
TxKytb5uMYIPndt1XrOgA5roXpeUuLh745LAFvZ0yn3+KmxBbOqKPbwSkfidmGffGXdMTwAzWLs+
YWapqzQ8IrM4/9cGOs0J+I8QYQD/vcSQ6sKQjQPpDE5UtayMGeo0Vnaa+VcrSJmyeTOlDKIEVxXS
z9JHe/vPrKcz4G92PWKHMwnEH3pnml3fZOaLX8AiZTX3JGEfjO0Lj10CLgg82PSYXbSYhh5HLh+6
j9TlpTabDvXtpLK7u9Gx9jzBxONSlb7dkw/cAIbz9cY1f9xoyVHNe9nab3Q1LETi/gmUwMJ7QmGW
MPE3d/lnjTNsEBfxjbiDYBrLp5CwrIr/XX4YwiEuPxlgci6+dBmGmlw0mdZWPP145BohCroiLbWZ
JRZj2jzor9dsxaeaxyOGnhGo4QjXrB2ZJUWiNMyBjqMwrkjfcGQTuEH3bb840ZJW0ChNzXbGIIZJ
ORF4EGEsDfBJsrIRAvZCDb6xmqoUSqr/UFeA/zffYYh1GETtTeBcaoxRHzajRE01arOhFWb0iLzn
oiWGbNE0nRyalBMieXonLsTJ1X1YMwSiCIgfZMEvOF5DZ3lJjbZQxAM36+m3nvEL3mR74o6i9LJs
Ilq5iRZrjfRHhRx13RMi63ih89FDSgVV6rNSiF4sRtfiEZHQMKTWJZqXsf6hPxGhsXDrJeCvKxAD
xfLF2mBl+TiXnnRsV35K3aU62nvvFk+XGrqAh3OTIMRrGQ9bDbt48Ec187GdceKfQwkJpylpeUwe
JGAGbPS2yTCEkPeXR5szsWXQCWz+z/IwDnqW7T9vatKQfXLcxS/aQ7KgAq+w7UU3thEDow7BpAT2
+3AhYRj+aACA+wnNh0iysBUOcnPuCA9cS85hLI1H/s2O/yccylxwyePx9ghIgx1wOG4j4JhiGO3/
Cr/yNPKjS1Kb4GJFopXxmqrsPtvW5DscKlWqnv68HD7I1l8pzdd2N3W1W2md0AqFysktoOx3KoFK
MrRcXUWZG1U92YlRpOEwjR0Z61axh+iSDmQsV8fdzmtNhBuLlEClyxwRS1DtECSEz+dXHXC/IJv2
IYHxwDpoj7bGIi55hzTf8PHer///bs06j+0/XM0zR2jiCEXSRb9dPHb3koqDdxW1T7coMi6DzbSe
arnz1JKf0su8audG4UZUkFOABVUr5CwkwjLNsiuLbhtsKrA7jPo8aNe53UTEDfCI9jlfetdJcurX
v0EVhrROOkz87xKugJxIrkvUvr9EoG0eWv3MnYZR/+rMwYDEqKBPaoLfURKqYFYJ9tNNPiFPXPPh
qzgAPdKOFG6smfcisOk87yeLCgbXtIgB/vbggUWfN13DPuYEDkmL6Hoz6KzNHR6f2xHBfG2OCbj+
BW6VwqIUXeHB0DUBK4jWpcL3vxa5XAeKWejDtgqICYJrtS6E65c3pGqPJz4BHPpStI44LXRyqVmY
0Lg7mObFpQ/YZNMwU1ePlwezOeQW1xoANI9E4Zb35Fj0lgESfSDZGv0nGTm584eNr/vjDHkREWzQ
cd2ON3vGZXxh141x83BuO5bKNTUWbv9MjMrErx5rc0LUGp0SwgIo/lRPXbf1xWjpeHMxKY9gY3w7
NzUwpk87FetcbNUHQZiZMYxYPSuaY77kDCCqcTh/jVZGy2ZuIPJDt+3n4xqxTeG3b42oMa6SV+wV
Vdyjo9pdPo8VOTR0XL2BfI/XxwQuwP3zWByPni/E3fasyXyC+k5DrJV9E19CU5VhdDXBGHOemeYy
xq7PaAEbtAk5KWGboF6pilpduZ+ve8B+g54LINoGCBich/Pff/TncO+M5qdg7ZHlS1LJLjYUoCyu
vXD47VzVTloVEndP47PonSr9MT/bY9FWY+wBYC1ky06CVc0I2ess851aulXhSrimadlGFcB+QZNV
17QY5V2bajYyy+SjP2/Zy9n9+GCe6LNDU0eAbVmqiKzJrUyJMkxGmI21+htAVJLM1g9goVt8MDum
SoOD8VGSWZMvvI7l4CP8nGgZVYQ2B0htWD9zjZqkvjal445RJ12endxsgazXMv9X7GV5Cxd0DKe2
wXeihEG5ntDpQcXwtfhAPjFG9pGvb5yXmOMSoGZBzjgH+15cveS4gMG7tKZmwDz9Zpjpw3tmNy3Q
Ntubi7Uwkk+5ymn04Z4QBlzZpN0tG1Rzqq7ZqFH2hM0qdbJlyQy3Gid4TMzV3mYKB4nGxABWVIKj
EfnFtKKEMBM2ltIk6rVA+J5baAyX6RyCqxNJRlZCzCDIovQKuH6G3EOPC5GoZcLaaNX4nQJb7Z2m
rHKELy9d9eBp8KFvzg9EKPwXibSKzWGOeHvlGJ+LZYMw3JB8/93yaJiK1mDsQzCKEn7QBvoG18gR
P3+zqeZK4oOS+0RemQgxIwT5DbK1OulFfCd6jUTpKnNf4MGmYxaAbW07yg9abPygcz8eeF1O4o8P
mmG/UZhKbLdcsiQZMA8+HuIUy7Lj4Tovnyu0E0Oya5kqdJK2EjFMPTf4uLqesChoqyUZERpylW3b
2Im4sQ8fRsbalwMSNhxJayLo9Qg7wODL2Mh2RZYMPlaKtLHYOvy0fa1IW+FQJWCQ4E4LdkWOP2sS
/UkvmGHhubSmDD6vhx5Xr2RiZ0LP4RbP31yuy2NJt6W7KnPNvTdjKbS5s0tHUdGwvJ3IBzC2KsfZ
whUjZsqhf42/J9+G3/b/R2Fo0+3TwrAP+0uE3wwKPBNm4L6xIBejHEK91BGg9xzUF68zHsdXn4yG
8tU2GFszCw+60yOo09ovlsF3F2HzL1IEsLalIpiAiblx5vX6rRxxu0cU228EkLJRhgZRTqBt3iMg
UqqKRR15ryTuqAUvGul7ql8/jS5RTVex2AuC64qUCt6uJqeSjyfE7BoVGVcjzAwYb3irxRhYHsje
jlSJrMEGiKUVb1U2AA5CHcCVPIiS4EL82p2H51ggcvidFJ067gJ+luOcmMLeYCUo0cubY+uhaKxG
XJeQuXdKU+/bcVk0UZE35MrAC1sI8wtFUAezmNgzKgRhHQmKlsePGtRbUH2ZLLswDAYLIC3zVgWp
qcR+nUvTkd5KTBfQy1yPXBOLVU/9zzwdZ5gadHN/9HhjhpLx8vegfOcNBX+kI/shpoxkuejSBKOb
YC/jHWsT+wWFRLXT1vfqrrnB7DUPyUGDBS/3m65UyOVYjMIc0IGmIvkzOzYrOvkjkatcJPhZp0RB
TDJHITDrlrwJdm2jzN2jMsQPmNTfo2ICngmtPYspPKSoa9xfQxSp3Py1CbkMnk3yVW/B9Q0NKPub
8jaJ4oTSbLlf7AfhWfGBf//eYdD6s+FeFNJAIvYuTsedyab4NPFNYcpYoivYqIknpmp1VY32U2Jg
/tFv5VghY5AzqbTPp+PstVFgf5jlbp5BvtmxGIgxQBwwMzOo+bVhbMZHFXfWQRXfQgQ51fg9Hl9G
1vKiYm8x8HExBGTuKA1gUM2Z1V/ivRlebTX0qCV42L0ZUAJscyHVEionXs7Rj/Bl/zO+JXjCVM3n
w1OMa1tJsz6g80cHVUsYD3S+JSVnAV+GO5zR/gBhDczWatDc+WQ39ZsYVeVm5oWJDU7C/o+zsXx4
na+zqPC0Wxok6Z6BdPaziQTjHeAypzMlUuzXc1KaEsMFOrXrcol8shkqBSJh2RCgGqlbhW7raN08
etZse2rhBSaHFrr9IyyQBbHZfNTlmNWNf4prTtym+YU3UGHUaMG44FsLb+CDXaN2ciI6iPOJi8Pr
F0XaRVxrOGtRYKDJG84jey7qQM5mcElIOTbCw8jDbrkdIKjOhPFIiqAGyQC6T8Vz/ra2hMHPXMuf
DJW4H0yDUdoxSbBuygENrY1ppq4+d0yRTeUcZrGgG3CXcLEzdFhfmFfFCrbgKVKyHrxmPrY3+RW2
osh2VKq/lMVGUPJg6y/2QpEeqCaLZiPkC/ZRasOjEpd25pHw3J3/HAqfYN7bHuCepbgFbz0LvAng
yUXduVvcC8JrmLzkwxG2pOcE7LFd6JFMOr6mS9fWCwX+Ns8Oj6GcY/DMd+SqEeYuakLQ9x7FEabC
D1HUgRchmn8oeBK/+bYw7ZbbuoAM/ABd5BdbkGROfqOzpmWd+8R0oW0Z0/opzOW2kdck6i4m6Aba
br+mnDhL8I+VFLGqtWnECeJ5DEGjbZcwmd5Hc1eNPZ3BPt2kSb5ff0QLcUokfJdpk15Yfxvh7zr+
haYZJFnAqDQmOVExsB17Ib50etoXuhflVgKlw6X05PHwGk81U4oaPia7YdQO+t50By18Iv+5MPz+
8+av15W0QqTuYy1unEhr/L/1aCpjDrptypUPB4LRUSZquv1NNWpV1ciYnT2v/dYs9BrwXrgdQkYR
h/uintAL0xHCoi4PaBSEGzCGFLnwlkkpvhBoVhitGfOmDWvqx2fMPxWwo268WLiaKKmmlXBB/pVy
e7U9VQtpdY8evC/zz7peBA5CglodoLYcQD8L70TxNfqP2Soiz+PKNmxKRmwCFiCN9K8hr8JfrfbP
66hymliwMHuYER3bimXpxEybzm9/qkAXP0agEvbhEnvBFiF3pjJBuTye1YlzvtwVFdb30wCDzSr2
IreD7u1CIyLxGdzO4g/EFivt27o961sNdkBuJ/T2BF98kx5anqGtpc9CTk9+NrHWMzYBo+PlAr6U
sWHPVGbHyzhEGf49Zt1cLVEQFXT04hgtZI3L+6mTsAb7UrJmz280YsxZZv9HSI6+hZ9m21fq8581
fuDCfjZ4EE7GlKa41iIh0CoLLrPwlHseD9mFtyYjP3r6YZLozYTLbgzRRHQlF8iITjbTW34sqFB5
tC6U8nrGPmAabpIvmvxDGT0ry77U8YRTj3A+Q1VazO6QTXgH8ejz8MgA8g+Z7eMIAgyt/x72gHFT
9gHHpu3mu8Z8ti9LpRP7lcx1Cx5LiJgbBAy51a9vPx7QMr9QZmqPxtPpdlZv9ZOASBxcQdoIruHC
94wGnMg1woimzYPepY84hYcuK3l3SkF6iTW7c4Rur2gyQeaSr0lU7Veete42c+WeuQnYtgWklFRV
YQ6vSonCAsLpZNhezCA47FzCJJDLE/RiBBeODIG5TQQuPrMYst/CY8SXRib/LPqF+Md9ORxh+xCr
QFnBVcMEv9IUx17OYg+HiNlBZ6nvvB7E/nupC+9LjjBcAMPJsHV1BABfyasIXDyR+4tGK+fXUuz7
5+1sJxVR310GL3/Om/letAAlZORZ2mnm5Rnv6JhBviQ8NbePuaYZaAyWVsdrMzLTs34iAE6T7soW
DWHG9NcT1mqKccNAUc8nGOnSnHB7l3N3VtANBQd9TDi3tv/fxgbc14MCVMuGntcDsX10MfWnS/DI
Oa4VxpoP2ktxel6U3uuhAxdkssDBXgnUG84dC5kFfEa5smlyf/cmMUBTtEDvq4y8Xck67Pd0lOOR
h/Q9aQ1Ve2j11YkXQe5qwmaiYHlbxG65fCe5lBJ66owsJ86rFCI6mdfTEIz3M+yDx8//uue5GQ6z
tKqQaiZFXdCPK3mBx02ZAYA4zEdDG91YKLzZ2TTfBwgbsU/URQPmCEnz3GviLchImcDbgb0Ak3WJ
9t7/QyRrTy8RK314OE+n9jqTh+90yqtgM1R1M1YyPpEc/MK0e0ZtAPGzsCpHladblnBVu+M9aU0m
51SBIcO4mV7Alrh4cKXeIk/RDuwHieLuIN7iqoLJpT2Cw6cRk+K4KrHPP6tTDzmKNjiET3R/gDVY
1fKFhGTDX5lk/gLI/DYXYZOP7WbwSzwH7Wj72oPmkCWHqlt5ZvI8wL+S5cHBAw7vFSRqN5Wt/Fy0
qq4xw7GEDTGNTiYhQcC1YyFHK4KJ1pdLPfa48QDdRIh7X2KXu8VyF5I+CxeqeRZOyK0UwHomFkE+
b3GkPu18jY9M6zQlR7//6xWWd76spPkbe4C8kUBKnkxYm1wYHhRPjn2E/Ff8TRkDnJ2tsBCd2bWr
TLihjgSfamCEZ89ok/7qxxtIf+0gzf2rd0Mv3YnKSJFq4blDwFJYf3YtaTE6b+RunxegmMSR3PgO
5QLi5nD7CqVzneTgPt/jKL/zxQK2aix1QQJmc6isxGPOLsGIc61ZxsUPk1nCd3IwNfmM39JOdN4+
JO009n/yJSLSO38xmHI07pVOms66ounr9909Tp59cJRLjfdImJ9W3tDzM115zyf1CuUA3ZZpHzxb
v53zu6aKjs4Je4VPu8Lmbz89hyiEhoKZhV3MSjCP3/i29NCNjMLx+R1MEjjyOe+OtNhJWsKJCTnD
SXN2flLnNZSItkgrLEjs0x1r2uhoTCV1UitazrK4TRqzdxxJu8TOzHAN9cxAGVEr2qEevesMZz9c
P/T2eI3604F6BPuJvEuWDcYLcLksydWffhuQEXiEr+aH5Jm+ife1rv1b5CgxiTr49Hy9rn1mSsVz
9vUtjUdnKr4H31CTWZarX4mUpR3p5Ax4JPYknTXbbIAFzMvENPl1KEglDBeUFdUtNk2Q2OCKAFjy
D5XpghFCc+AMGCyil6DrkB9o2hccWlsBIsUPnwTAKiegUOIGyKVMv1JTJ/9msXLXnbsIuPvCsbQc
LBKmKB4eGDMIIOOK0D7pnH+NCVR67rQE7KOOaMx9xCRbzTtwS+WdaSzFTZ1LZTYbf047kqPo/PR8
KvbNdSP27qfJflx6Bd7Wu2wVE80jb0BrJsoIA+fREr9FyAlJU+cWyx5lRFGtWrCuIcv0h41iDD+p
FakoPMzZS7WPO6jkHivjFCm+7sKDU//44fqtxYE6G/mSDGlgALWRqgYVllB3vWc+G7hDSuBq1J5J
1vGPVFsKviQiR2QStPmlpJxrEGQvrx5gIbEYtfvuMla9hnEItOxNaw0C7MY7BJti3y5BDSqyZpa0
9v/vLcRsrFkuMkv4PwljZpaR5ge7snLEfBuLQqPogclkPS6JzN07QUu4I4KCerDekAettKZTVFpp
enikaAzHbdQ/fJOoTjFctvg0Y8FOyPfJ+OifVm/LQ2H5jLppKi9BemxPjJXpW1zMyoZjJHVlZHdg
jytB7gCMmtuYSrlhLYqCcd3k5da0wk2irJSVloHs2BcjJ7GtNthFg4PEjxJRFJhJ+6OLHyf/WBno
tdVHTVoPILAiCzxoZArasYU4W1xnTmgs/OVYngRIqSSy82wTbiTQMucsT4rkgrUj33fz9NeTEADY
SQw6XAPzTuYmCojP+4C/26TXBtRQgaCatKMy0s6iiIZeRUryZ0XvMamux59OeLjTSXGxEy7beW9H
rmJIXzVy10F6+NN4kqNMvA2LMwQBuoFKSbTB2QZesUyXTnQvNBCiQbit2LxXGgqnUPiMXaEQU/Tb
DIn6PdewvvlhkD7xuyVrjpE340ssOcN8zMDrGmfIdMqjeltEp8QI8hpWoMO9RvdjxrEgs9ZgOSBp
l0OdahgslGIvpgMr90Z4Hg0OtY6hnIIYobZVRSGcdH5sgv6xrPuPBXYiq5CmkRAjQqrfH0P3/dGc
4daelGIgH3/9CPrb4Am6NiXRLC3tWLtgshm4PJZeaMcx0hSW3BHdTd47J+Cc4ucqG/joh0zogaIt
of90yHwmwhJ6d9PzVxIs2qsw88Zq7w6udcW5tcZo1Umt9I8ErbF8RSsOmkUhDM0+Ka1qsSyOpJip
L3r2lgjAlEOO8DBpuFsdIkT054m/jxMv0b0YjhffNF32n2dYTb1MXBwgVewYmORQDiddpWYyADoV
fNeWxjMeWXlZRM5kgiK6/00YBo4IHxiZJuTMiRWY3h5RuPlQ/N9lG45JNGVYvpK+AVCeMg9nOtwS
ISpHxRMwHnFzTw+AH/O2fzWsfuUaKGc60CAdA4LPszrUrnLKU7fCxJvFU//jUqOyEqdO9YxO25jX
DxKIIEfaS7ZNVw6IwE7lBo85S1ljYBdiLD/Bou5eHKV2bkrDiR5xaK5uC//94h74mOfJ/XF7S5Wv
16QNCtyngk3NRUTCy8YR+DUhSY2ZKNBbsY6VZucUtdG/pt8s2L5HbAIezgdRP5x8NlD+CXed1X/l
oHpi8BdnLa0x2+zmgWzkcM6iJEJDfnnHJLHNVx3xg2oMgqjY+aowFVuYtq6bZ7J+BV1tMX/6fFTb
UBJmOywWF9fgyfPSBuOCjNkFV5y2El/ItYOwFDIdDQrdToWW9SQfQ9+YIjXUYm1F5Gx8g+Ct7Cd2
wgT+LvVSOTwxn7m6yDESlG/Vp6r8hqJ6tdsSQARFFUqFpNX6JSG73IjzJ3QseofxOHxsqmREJjBV
gtBpjEReQQqBJKSTC/MDlJEbhG/quykV/mVdTqHb4uNujUMInbI85X0ZgkmcIM+F3rGbVCQ9Fmfh
vvFqp2eXkP4FtxTr7BIkVMn5ooTuDrxZ7RY6UYmE8utcAyx/ysKj2La5qHqQmKz//OCJb2DmF5I/
n4jH7KXdJDUpWMF/iRlDetJXzbut6EB0WbfH/SJxLjSSJ8fvc1TOeND8Vmy6xCu4bpcqNs2lxqIQ
8rricZyEyX7Ho5ZvV5hAOvXBac5NUn/5Di7mpQEWJbgJ8pDH/Is0e85tlb8fSFuAnc/+63d5g81J
zAqivOoEy8YJVaRbWdW9dlTXZ7ZnEb3qQ6gRdkVpcbalBYfXcPEvk/jmKh+wnv9ExuuDzKUc6Mmd
L+0P1mkGchni+UZrvNYH+QDuMg4y2t6kZcXmWCf05WV1bzz+PoVtyrqm0CLPlzDFq774hZzxYg+x
qToKF1Q5mKggBlEJ14Al4lALBul1O3CSOk1AJ8Iln0jPKyIfJscrqllKx9GjBl44dCKVm69cYBAk
4MJQxcu1PSabrkYn/O4JsnUMJcxO1nm6z5HZvD0gp9iv+c3VTvh3gUtvkPjMGDhRd8QPMHc0uxFS
nzJ/2VyUvLLQh+FTdmMWP+Ukyn/KhORhSbaGUW31vL/2ViBCuHoyPr+f5zHQUrgVbbqPdwpr5616
Jvbwn1QONDuKLMP4bKMAzIfdiaZ/9ug16H6Elnx/wtzvklQfPnjfaQ28uj8STz2Jf1zZfIYwLi3m
G6+HaueeVKLEaq0el6ww3U52+HjdPOfcE1YsE8LYWvjtHDygcQ+YwMaNSinHV+IToR89+523Ha/V
UJlJUofdA5Pogy/6Ke5nwkvoiZQN1mpnWSKp9nlDbnYvT8yOqTjYHskPTnQ0zwa73Ht+Uhz2P9v4
OpsMOj3q1nKU2+yOy1YqwlK/7hrq3vQXVXNaw/JQQF8cpbxaAm0gF3ccMQkZeW9vxkvTeaBCrGSG
8Hc3YLxN/ev7MqkIOUuBM8R1XBovFr02siX8h76jfEh5DkC7ci7M+SuxM+fdN1/O+KzzKL684RlN
EqKn2olCV+uyh+ewPX8ysaGqWcJ559e7wspJz7MK8EFXugTkeebw9TXjy7/qAHPmrqkXZewWIYVA
I5vSxvlDLb77Bo9O6CpF42pL0IUahHemPKrMZ5AJG4n2RRvyDqlVNLgLm5I7M/dUz507uqi5+cdh
IPM0SDPWnX7l8+N+bVt8JVEA0DCjS052WYQuyIBX2kYtmWyWSZHWbzhwAv906x2oBulfTFLvRTb1
dvyCO7NZ+Wjwha5UuPPcSjaZ2hmutt277MbXmjnUQl7VchhfJ0GPXxlq0Vr0tZcyNHFuiH6826gS
trgtu0U4yQkufL0kw1x1rz9gJ/SrwQdZNwZbiyQHAMq+OrQVEnbgIIqQPB/CrYi4/kDMg8Xn092s
/QiCT7Pd+4WljkQ8MrqtqDGcGyK9yE9nmcrSM03MvmQxs/fqUSDMoX/kwxmVKofjaIllFGHF+0GN
hBPFlIbug71bCxC5Fv2+FBL30WlymOR6WcG/8eILO+hNK+ePDOqbuZ9O12+PiFyVW+fgG8WQLMKE
zZ4V+Ocx1CRi0jiMqCtufnE3h7TmZt0WQMRhPauSMYBJ0uSa7tBso2Y7p1ewIoPDdj+9kUlnIkQY
u9nskVPQRkP0R91cmKBoZJ6yOHyqpBefZ7Ypg8fgTYl/LaXEWNL26KmrXeGafnCyExkG0+Uqp7Yl
MDbkitD+vdDiUXHfx+nlxBGaFIGF9wjK5Di71549uvnCSbC4FeBsZgMlYydRKhRWFNsSjrxLnsu2
T7d8QoooQ+gCsmyUo9FNrjfy/F70SZFCkMcjHGhlhCOMZODKx2V4qCQZj3JFKu8XVKxHM5SUh/bJ
/nDTpXbbeGEW5VZTdP8GMyA58VgH5MJYRu2KGyJ0XrbiJXch2RUax1lHmTLAsT36ihA0DxnFeH0z
pQ5RAbFF8J763cbWKB8UEs5yN3Q59cF4UoCbOlI1+FxK6JBTLPa+I+LUInU6jzLDGYOlvulvwDzE
j6huTOe5rAjgxUYLXKt8ZPnV1uDZXBM9KGLNYVZgBVoP9hSVm0aqnk08JEDRY17WjOIdgNlc4L5w
GCY1A3NRzbi8gr4w4MQoMpvWHA4B/cQquGxLa4PrvIo3KJ7NdUDZo3kcUOQlOi0MyENe/Ps6TOlD
+IrnmEgI/t2RYcdQCVXMpPrdeea10TPtSbKK5zG0BXOkxNTjcr28wbhKVZ5oRmGFpllJxe6HsAe5
H48SaQgDLrBSyXXtPqsii6LX3cfrtDo5eaAx82FlCoZAFfyT95u1JkhBfB70DvUHnobq12dCXuFh
ePdCtWqYivMS4JfsBxxsMUDnS84Qhi/yaaIMJX5Py+WC3tFdaNHRXjidEQIbwEBh3oFftwCUP3Pn
XQRwOf4/XXBpAEClXWB/RG3PsGxdPKSwhheqe/CVyQ4r/K4gLZwGauDCEMHFhneKre96GBUitrT2
BNJGHBQujk8XUnMsXPiPyHiNJP5iQzeSL0OEHBRoMbkYTdBG2tFw51BkkrPvs3O+k6SB3a5UawJV
aMIfJZrSylgLdi9u3bsIeXV0M1WWGqIvFa42X1t473MCkQT6jLTAP29ewo/ndOehXBRF9lbWNo40
Zbt23EShnEjsEWMgZJv8Zpd5GWTjqc9lxBO1YpvrpCHKjiXJ/N1Q/saSoxTuRWNOLU92PeyVtBcX
vKS/PRKAUHOxX4hZMqTPC9ynR4LKaNc+ZQ3EGHyCu8CpKzO6B+Unx8dJaaLxhGdL2XG4BtYrdrez
oL3qUbB8FUx7OtjySJi9RGNJtlP6zYeXVhOOt8GziI5F9pGdUg3Ow0a6QdWacxSrfbh/mgXB5StI
O0j2cbsM00mKKD8ITf1+9Y1J/jTHBK9jckD/Q3hI/fGbxqS3aOhpMCw9BtT/CIEGgZ1vjjyU0LAn
8C0I2a/orzrFqKUvQDkzZzQMq/3tWXqyrD8RWf2P3U+3MT+vq6J980VjZPnaoZqXE+uWIEHZBkY6
wbtGpag4V4gZZbPJkgNmeil8UaYYSxQbu/Tydq+gYGmBv0q7pwbwnd4qdYOqzvPhVFqxDzGkWtJb
6YpwX+PbPN8ygRfQ1Sd6rRb3vEkBBGakt4QPmkNfqug4Zpo9Bmu92aj2zl+A2USLNYE+ArAV+Pv1
wl0tJMfTzs/vfbYlUJvrCLiZyN1GI/XfCGiLU7k3uG/F6pl+fF5f45YC2428/yd/NWtjA1QJ5EPJ
Q/mA3cpe/iZZw7aBYnBrLTggw4vrqioEqBiS23ahFNsNXdJpGT+CVJ1SGgJcraqZdEv7xnmdd52q
EMRrvshBE5BLXIZTw0gp63bvy9Pb8WQy6Iqao77ThscFFWpI2fQqccTMjMiFoXJiin62QkJA+724
iMZXq8HR8R+DGsckc7cJ88wvPGEut/LX+UUCN+pJqUKojqumjZx53rhGm70CEr7JNcH1213v54I/
FpboEspUXlx5nJ3p/ng0+BhW93R+UVNsR/RwwZ5kDuVbcxmJIJ1rEL3oD22cgOUvXq7OMQLFPwGh
/VC4zh1UEpC7nGQCyX+Yro2efbR6WQduB9xM7Lof7yZaPJywnTbr44JY2Y458PMWSJ0LSCT3bOIY
swI3P0GMqliq5tMbmeiYFEhMa4P3ZlCT0U4oESy2u8jcXRUAQDBOtT7d5xsOg/mL8JSndiZeM8Sr
+tTEMEVZ/LhbFJdmtS54onsMQEVjKb3VyM6vui4HdC6aAWJ6hNdxOD9laxM940wBRvKk/T1+ARRf
RztkiX6DvRpwp8zfZl2ytI/sarosgx1Qu6YaPJwQuNdMUAocSu2XidLdiUx0iEM9Y1wAh88USdtk
a+Q5N1LczJfxZW7SWyPSIAVKxQ2VaUgeBasFCtNBxVIe6L9xCutM3ck/ORJoULXz2h0Q7gbd0GOi
uEyYPjtOYdc0l6ksixcRCTU/RKsmgJGY0Sm6HwzjyPD0y3Hah+9s4UN4X4FzAbbpFQewvfw7tm4G
ak8AcdR7S6bevm/tmp81pYl2M2a1yteMowFeq2daN7mQn3SvocnFRxFTP9zHyZV3LBkYf1QXzNym
anGcJZyqP9Rv2CpthEHKN8LfKuRX2htLlNvvSysv7/mZKoYAFemDbBv1Df7s+dtqN53kQbuHF0F7
CQ+v2IDsIRM4Lc/x67Q9XWivEPGfoXhkQy7v4yoLmPvb6RGAoZBa0hcpcmcEV18DPpbKlbU+9WUZ
jdRUWiNT+p9lV1e0WYUbVzyLSxqT5PoJrrhn6bYJork9tcW60g9NSd3b175EW28RCv28SQZB1WZc
2dXfZv+5EbmQUfTW7oYR3Pz9ke67gbbAWNDtzoqhPcbSEtZdXR1tf4W4TAq2pSfbrOmwlcHc0+CI
qWW6uHxqlpG3IYKaVoB+0nyPv4+IxzloPTr2ZrYIJGNtZojGBvcXmEvFYeV8n63M4UeFgtuHggh3
4HNgTw7ndY0eVM26J10y0NIja39RfwRbbIzWL2jRlsPQ30XHALK8whNXzRUmoJqhAwy78xIDl6Yk
HSly0O80zYCttZIWPxcTwhv4Ds4o2cTbla3JODIRWP4Z18MNhQGznWuHlUGcPcR/kHM5MIMCYMGr
p92VKca6wpYffHSuQv4c3AxNlT3NFbI6NKiDuliNQudr82lpmJWatFHImANgsV++RfHuyOvt3qoG
5Yoxq9IHkvFuv3wk4Lnx9qztTPgUGZTnAo8QmE15st9cw46ZDKMft/a4C39SvzybrEHGaxd+q+pK
P9oGDZnLh66dM3IgWY2GWJBJcEWQs0qmLzf1Y6eG0d0R7lgXXYq6+5FLd3Bd7l0ny3PgbsYowmbz
HHl5Q2tNwljJhlIaerr2ruoUlr/724vpNm8iLyxu6DW/V9fhglytYcgZUpc5/kZITUU7qwNAA84G
+gGNN5zmKkZTYnaqkPjlftfXnMEGpiBWCfnLmh2gdxw47DxrMNZUn5hgaRKEDkRZ4OhYOn0mJQ0P
nw159HpzkqmaPBT39gZsSO/3mU3kRCtQOxNCzqGRoD4mq1N9IYSg+9uNtQk3ZGbMYo5XmJQ26BsY
ja6Mbpd81ptTwjEfWFaU60DdWlbjxYCbezy6USXGY2QkJWKZwM55dGLeQJ7Bl08unyCSJcg8AaE3
CwfIjlCOvUH2GJEOPKtHVCdMFWit8XTHbg7kko9rFKUO6NeqNWBbNaUBVKAYC5h+HRy+x5Zr0hui
3PAFnxN9LyZY0EQhCh4beZHPg9QISweMN5uHTGJMyFYQprVhjc6Mh1mCHPOq9CBf0qmc1izbI6tl
9mTG9z664kyyWi6J6mkKWOg8XcbWUQ8+XzR4BGcAEvZ+tU7qWKPus48PEeGFhnJd4wMdFMwYX3Qx
R89IYY2EQj603i9qMVrSq9VVWnhv7u2+I8HWmwBYQrx1pQgrZqc1CXm4a05Yy33qua1nXoVl0Nyt
s209FiW8VL+vI/yRwkdhuENbloQUz+kL/Vvr/sdNztiv19WLfxzhOoWE8YmVk7ZFpvGyfO0SjcF1
4qM8J9zCEUcM3Co7pW+lrGS8SLlNsIvuAAFA+53z9i/p+Jr5C8Q=
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity bd_929b_c_shift_ram_0_0_c_shift_ram_v12_0_13 is
  port (
    A : in STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute C_ADDR_WIDTH : integer;
  attribute C_ADDR_WIDTH of bd_929b_c_shift_ram_0_0_c_shift_ram_v12_0_13 : entity is 4;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of bd_929b_c_shift_ram_0_0_c_shift_ram_v12_0_13 : entity is "0";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of bd_929b_c_shift_ram_0_0_c_shift_ram_v12_0_13 : entity is "0";
  attribute C_DEPTH : integer;
  attribute C_DEPTH of bd_929b_c_shift_ram_0_0_c_shift_ram_v12_0_13 : entity is 1;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of bd_929b_c_shift_ram_0_0_c_shift_ram_v12_0_13 : entity is "./";
  attribute C_HAS_A : integer;
  attribute C_HAS_A of bd_929b_c_shift_ram_0_0_c_shift_ram_v12_0_13 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of bd_929b_c_shift_ram_0_0_c_shift_ram_v12_0_13 : entity is 1;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of bd_929b_c_shift_ram_0_0_c_shift_ram_v12_0_13 : entity is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of bd_929b_c_shift_ram_0_0_c_shift_ram_v12_0_13 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of bd_929b_c_shift_ram_0_0_c_shift_ram_v12_0_13 : entity is 0;
  attribute C_MEM_INIT_FILE : string;
  attribute C_MEM_INIT_FILE of bd_929b_c_shift_ram_0_0_c_shift_ram_v12_0_13 : entity is "no_coe_file_loaded";
  attribute C_OPT_GOAL : integer;
  attribute C_OPT_GOAL of bd_929b_c_shift_ram_0_0_c_shift_ram_v12_0_13 : entity is 0;
  attribute C_PARSER_TYPE : integer;
  attribute C_PARSER_TYPE of bd_929b_c_shift_ram_0_0_c_shift_ram_v12_0_13 : entity is 0;
  attribute C_READ_MIF : integer;
  attribute C_READ_MIF of bd_929b_c_shift_ram_0_0_c_shift_ram_v12_0_13 : entity is 0;
  attribute C_REG_LAST_BIT : integer;
  attribute C_REG_LAST_BIT of bd_929b_c_shift_ram_0_0_c_shift_ram_v12_0_13 : entity is 1;
  attribute C_SHIFT_TYPE : integer;
  attribute C_SHIFT_TYPE of bd_929b_c_shift_ram_0_0_c_shift_ram_v12_0_13 : entity is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of bd_929b_c_shift_ram_0_0_c_shift_ram_v12_0_13 : entity is "0";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of bd_929b_c_shift_ram_0_0_c_shift_ram_v12_0_13 : entity is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of bd_929b_c_shift_ram_0_0_c_shift_ram_v12_0_13 : entity is 1;
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of bd_929b_c_shift_ram_0_0_c_shift_ram_v12_0_13 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of bd_929b_c_shift_ram_0_0_c_shift_ram_v12_0_13 : entity is 1;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of bd_929b_c_shift_ram_0_0_c_shift_ram_v12_0_13 : entity is "artix7";
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of bd_929b_c_shift_ram_0_0_c_shift_ram_v12_0_13 : entity is "c_shift_ram_v12_0_13";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of bd_929b_c_shift_ram_0_0_c_shift_ram_v12_0_13 : entity is "yes";
end bd_929b_c_shift_ram_0_0_c_shift_ram_v12_0_13;

architecture STRUCTURE of bd_929b_c_shift_ram_0_0_c_shift_ram_v12_0_13 is
  attribute C_AINIT_VAL of i_synth : label is "0";
  attribute C_HAS_CE of i_synth : label is 1;
  attribute C_HAS_SCLR of i_synth : label is 1;
  attribute C_HAS_SINIT of i_synth : label is 0;
  attribute C_HAS_SSET of i_synth : label is 0;
  attribute C_SINIT_VAL of i_synth : label is "0";
  attribute C_SYNC_ENABLE of i_synth : label is 0;
  attribute C_SYNC_PRIORITY of i_synth : label is 1;
  attribute C_WIDTH of i_synth : label is 1;
  attribute c_addr_width of i_synth : label is 4;
  attribute c_default_data of i_synth : label is "0";
  attribute c_depth of i_synth : label is 1;
  attribute c_elaboration_dir of i_synth : label is "./";
  attribute c_has_a of i_synth : label is 0;
  attribute c_mem_init_file of i_synth : label is "no_coe_file_loaded";
  attribute c_opt_goal of i_synth : label is 0;
  attribute c_parser_type of i_synth : label is 0;
  attribute c_read_mif of i_synth : label is 0;
  attribute c_reg_last_bit of i_synth : label is 1;
  attribute c_shift_type of i_synth : label is 0;
  attribute c_verbosity of i_synth : label is 0;
  attribute c_xdevicefamily of i_synth : label is "artix7";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
i_synth: entity work.bd_929b_c_shift_ram_0_0_c_shift_ram_v12_0_13_viv
     port map (
      A(3 downto 0) => B"0000",
      CE => CE,
      CLK => CLK,
      D(0) => D(0),
      Q(0) => Q(0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity bd_929b_c_shift_ram_0_0 is
  port (
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of bd_929b_c_shift_ram_0_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of bd_929b_c_shift_ram_0_0 : entity is "bd_929b_c_shift_ram_0_0,c_shift_ram_v12_0_13,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of bd_929b_c_shift_ram_0_0 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of bd_929b_c_shift_ram_0_0 : entity is "c_shift_ram_v12_0_13,Vivado 2019.1";
end bd_929b_c_shift_ram_0_0;

architecture STRUCTURE of bd_929b_c_shift_ram_0_0 is
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "0";
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 1;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "0";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of U0 : label is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of U0 : label is 1;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 1;
  attribute c_addr_width : integer;
  attribute c_addr_width of U0 : label is 4;
  attribute c_default_data : string;
  attribute c_default_data of U0 : label is "0";
  attribute c_depth : integer;
  attribute c_depth of U0 : label is 1;
  attribute c_elaboration_dir : string;
  attribute c_elaboration_dir of U0 : label is "./";
  attribute c_has_a : integer;
  attribute c_has_a of U0 : label is 0;
  attribute c_mem_init_file : string;
  attribute c_mem_init_file of U0 : label is "no_coe_file_loaded";
  attribute c_opt_goal : integer;
  attribute c_opt_goal of U0 : label is 0;
  attribute c_parser_type : integer;
  attribute c_parser_type of U0 : label is 0;
  attribute c_read_mif : integer;
  attribute c_read_mif of U0 : label is 0;
  attribute c_reg_last_bit : integer;
  attribute c_reg_last_bit of U0 : label is 1;
  attribute c_shift_type : integer;
  attribute c_shift_type of U0 : label is 0;
  attribute c_verbosity : integer;
  attribute c_verbosity of U0 : label is 0;
  attribute c_xdevicefamily : string;
  attribute c_xdevicefamily of U0 : label is "artix7";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute x_interface_info : string;
  attribute x_interface_info of CE : signal is "xilinx.com:signal:clockenable:1.0 ce_intf CE";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of CE : signal is "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_LOW";
  attribute x_interface_info of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute x_interface_parameter of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 200000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, INSERT_VIP 0";
  attribute x_interface_info of SCLR : signal is "xilinx.com:signal:reset:1.0 sclr_intf RST";
  attribute x_interface_parameter of SCLR : signal is "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0";
  attribute x_interface_info of D : signal is "xilinx.com:signal:data:1.0 d_intf DATA";
  attribute x_interface_parameter of D : signal is "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef";
  attribute x_interface_info of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute x_interface_parameter of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}";
begin
U0: entity work.bd_929b_c_shift_ram_0_0_c_shift_ram_v12_0_13
     port map (
      A(3 downto 0) => B"0000",
      CE => CE,
      CLK => CLK,
      D(0) => D(0),
      Q(0) => Q(0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
