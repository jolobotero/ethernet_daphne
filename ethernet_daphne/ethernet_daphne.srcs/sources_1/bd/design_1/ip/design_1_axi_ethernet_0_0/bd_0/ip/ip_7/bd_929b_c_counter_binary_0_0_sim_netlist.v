// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Tue Jul  6 23:47:24 2021
// Host        : DESKTOP-QTM1A6I running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               e:/Proyectos_vhdl_vivado/ethernet_2/ethernet_2.srcs/sources_1/bd/design_1/ip/design_1_axi_ethernet_0_0/bd_0/ip/ip_7/bd_929b_c_counter_binary_0_0_sim_netlist.v
// Design      : bd_929b_c_counter_binary_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a200tfbg676-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "bd_929b_c_counter_binary_0_0,c_counter_binary_v12_0_13,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_counter_binary_v12_0_13,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module bd_929b_c_counter_binary_0_0
   (CLK,
    SCLR,
    THRESH0,
    Q);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:thresh0_intf:l_intf:load_intf:up_intf:sinit_intf:sset_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 200000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 thresh0_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME thresh0_intf, LAYERED_METADATA undef" *) output THRESH0;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency bitwidth format long minimum {} maximum {}} value 24} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}} DATA_WIDTH 24}" *) output [23:0]Q;

  wire CLK;
  wire [23:0]Q;
  wire SCLR;
  wire THRESH0;

  (* C_AINIT_VAL = "0" *) 
  (* C_CE_OVERRIDES_SYNC = "0" *) 
  (* C_FB_LATENCY = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_WIDTH = "24" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* c_count_by = "1" *) 
  (* c_count_mode = "0" *) 
  (* c_count_to = "1100000000000000000000" *) 
  (* c_has_load = "0" *) 
  (* c_has_thresh0 = "1" *) 
  (* c_latency = "1" *) 
  (* c_load_low = "0" *) 
  (* c_restrict_count = "1" *) 
  (* c_thresh0_value = "1100000000000000000000" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  bd_929b_c_counter_binary_0_0_c_counter_binary_v12_0_13 U0
       (.CE(1'b1),
        .CLK(CLK),
        .L({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .LOAD(1'b0),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0),
        .THRESH0(THRESH0),
        .UP(1'b1));
endmodule

(* C_AINIT_VAL = "0" *) (* C_CE_OVERRIDES_SYNC = "0" *) (* C_COUNT_BY = "1" *) 
(* C_COUNT_MODE = "0" *) (* C_COUNT_TO = "1100000000000000000000" *) (* C_FB_LATENCY = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_LOAD = "0" *) (* C_HAS_SCLR = "1" *) 
(* C_HAS_SINIT = "0" *) (* C_HAS_SSET = "0" *) (* C_HAS_THRESH0 = "1" *) 
(* C_IMPLEMENTATION = "0" *) (* C_LATENCY = "1" *) (* C_LOAD_LOW = "0" *) 
(* C_RESTRICT_COUNT = "1" *) (* C_SCLR_OVERRIDES_SSET = "1" *) (* C_SINIT_VAL = "0" *) 
(* C_THRESH0_VALUE = "1100000000000000000000" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "24" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* ORIG_REF_NAME = "c_counter_binary_v12_0_13" *) (* downgradeipidentifiedwarnings = "yes" *) 
module bd_929b_c_counter_binary_0_0_c_counter_binary_v12_0_13
   (CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    UP,
    LOAD,
    L,
    THRESH0,
    Q);
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  input UP;
  input LOAD;
  input [23:0]L;
  output THRESH0;
  output [23:0]Q;

  wire CLK;
  wire [23:0]Q;
  wire SCLR;
  wire THRESH0;

  (* C_AINIT_VAL = "0" *) 
  (* C_CE_OVERRIDES_SYNC = "0" *) 
  (* C_FB_LATENCY = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_WIDTH = "24" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* c_count_by = "1" *) 
  (* c_count_mode = "0" *) 
  (* c_count_to = "1100000000000000000000" *) 
  (* c_has_load = "0" *) 
  (* c_has_thresh0 = "1" *) 
  (* c_latency = "1" *) 
  (* c_load_low = "0" *) 
  (* c_restrict_count = "1" *) 
  (* c_thresh0_value = "1100000000000000000000" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  bd_929b_c_counter_binary_0_0_c_counter_binary_v12_0_13_viv i_synth
       (.CE(1'b0),
        .CLK(CLK),
        .L({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .LOAD(1'b0),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0),
        .THRESH0(THRESH0),
        .UP(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2019.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
hkYW+OZm6k9gF5yAUfXGm/n8kfXYD6tjFQYha968Ws0SqrM/NNAjCrrtMG8kIqTbkipnmceefxNr
sB0PtSpUrw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
NEPpD4CxNBVJLV3hg1agn83QnqiCz3YuR89MlVuNyQGERKVJ+uGolFDqHFzBKLQArFTiHBWivkzK
A2DQ42XdOxp30NKOgHjrjgmF+fZMjDs24rn3Ue1INLHwTS5RT84Kih7Jx/7R0dl03/COJq+33l9u
7l+ArdY7mLwqqI9iIjU=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
cfBwEwc95LpKuxDGqpON2gGac620iHNKrm/QNXYg3/OFA5ZQNdpdhRz4vCTQRVbOg7b1nIox6GR8
TD/cf0JW38RU0NuY+TR6CkFT19NCdy67gR6JTDdXifhr/zTKjOL5gvp0XjT9PSLwwPyDirNX4TMa
9y9X5pf4gEnt0dikHNgySZO+Qpr30MP7n6oAjuxowlf45cfmPqZthYPnIjBSCdQGBPfSF+kZ2F1N
XCDEja5xE4CQshPPodH5njadc6kj7/qp9C4PfKcyNtDug+qsws9UK25Z2IFc8vk6/15HlIkQHkXv
Wq0iHaPLidqh3035FinHyPD/FDnfGGa5Oa2qcg==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
QmjD3HAHcP+h0RsjR0iH8h2N6drNxei50nfQN9RC8HobMEaARq/6rKjZEhHXMSCStQeCMhyVKRmN
HM7ZrqMf3W0s/8U4QMqp3M1VuYXVjEe2PCIpvtRcMY3JngdSWOydG2dH6dDA16ehxinMKgIr0TjA
PXA+lfyX6yTs1FWrne/6ufrl6ZAPpNG7EDKQ2aHqSm8DEXT1BJYMblBfAjAajwaJmPEu1aDlQeNo
onryTiFJkKP92pcZLCCufZL8ZAJ5uMvZZxiZRsiLd0BnCfOe3rl9AON+q53U+iK11EvAkpIBT+Cc
VYb5NqVAVaqXbQrqo3+YHEW5ft3fM9kZnlFDew==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_02", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
FkHW107swc8fPv4xOTlQJU6PWERObturlywl6rsGCswc/v367bmQ1Maze/8QdmUPjEYwhAcHKVMu
7U4o3CvYhmrDpYiUQdQQ0B7gAbMZbJ8MFY5jRxn7KYDk+Bi9Ov8092IdW1a51FPWEVPmF4Kn6z4E
DSqpQDL58qieEUnrU2Ltb4GLJc3NrWTLvnbvRtHUUuQWTMZTQ7WqX4iH2dZ/EICpbRjlAF50iMAS
YHuuFTRKXcIFQlKYRyeQV4nyaA5JGbb3RC3N/Q2IZjdSXqQ9EOpmdhttpxReCnsdJiD/pPCtf7ZN
d/TheLy1Va2FZR+p4MozZorVui5/FtcCwKy6aw==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
NgZm/7Jvy0UZQRVxBVxeZ/odxMd59IlnRFHjM+6Bof6o6u4Qy4u9MOoQ3Sr2paPuGq+B+5EhdcD8
a5WGiurBrPW0qF+L2CoUJsDqz0WonRehZECQynibSUlmctvvMyr790pwb+C78gtW47p8uALYdUCJ
NhcDkV8fE3jFdDEYmfQ=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
TxQNdd2WOThZCBZEYNgXxai4jK9AqWD/GRadYnarEfzmLUfcNDUoG7DxVWHCdTVuW8i2qZpouT1H
FUHt76rzZk8vI2tFLfUbKyTaRqik1aYwOCp1ZdqbgqQEDhBRWJjGxcJuxZbSQ2z8IUgiJ0eT148+
nf9UmzvYS1jrIsN/a7K4EjyRNMk0V917y85rxdk7itlisaUw4Cm72z9slByFtALj6/077uPjcK9U
mbWm7PbXk8PT44eQeaJl990wlWvD5/8BZS6AHqjg8520Xs+jftSeB6aNqTiYxfp21FJqmexwo7cG
G3BH/DRHhP7ZIsXHqSaJJFo20Nx9VgpLuF5t2g==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
XLrgmisfueT6HUh6LjwhchXvgNWhIGSDzBLmLITqzedDafDITnhCTNWT2ldzv39XkZZx+GHAPfUW
cn5yHGsQh2K4QkVXAkqFH4s3rRTofr/UgYxoYwbLe3J36Z/LMgm63SjiFItWu9yOpRqF3CZjXQi0
wligTyPsbK/qsj32e0KHc4ZUeptqOfbnVcwFfyDMNw8L4v4CtkEoH7kBtRYV9UJS+hJPNYOcZYBf
h1Eww+HNPvQfOqU51sLeuiPXgDsh6mJUpv/7DKX2ddtUfn9603FBapwz12m8erh8GG66RTCehIjF
nFJWVAOw8oOVdstNvLeEf8FlamT+0yQgrVo9pg==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
TnlNTqZX38wwJHhWy8xqTZvdTsusx6w9VfPWRyjKudmaNsQN81n6dYIy2B8XoCBx/y0swTxhZP9F
oRJ/bSoefFBn2F8NN4O8yOxrElNWCh6Mz067uvUaUBk8pNUGOcvivXKQR/qyhb6Zip2xpcg0nS2e
tbKUQGmausrvLU+FUm7WDT7itLvPCINw7xkKzrAPBpAHguy3K2vfE1bgFmCJ0k/dx5SHhFwyA8cR
BJ1bpR2RC4Nk2oKzlHH94E9xmAtYLKbDMI9uO/Y5PUd5TeR84Ia4dqa6rp+W7Ymb0EVIbR2Hn8P9
gCKPf/29czofnVHtzXqBn2dylOj/FLeH5wgXdA==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 12624)
`pragma protect data_block
/pCAXRUQfxeSt54q+QyAJPk5iiYxYWu//+9l85jBifkA+/liEOY5ykiGjlobCTed9QI3/IzH8i8A
UNjkwfMy96vqPiehEUL9XwuU76IHd4OkYtIHQ3AhAcyTIeoqWxHD3ejtD3uj9ODkfoJ2Vp+pCRaz
Uqof1JEKKQUj5qgJKmaBQ+oB69ukkVEvUdEJZKp0zKJzn3el8p2L6Eb/7G8FPwK0SLzM2tIQZDUz
GQSRkM2uOlKl4xJREHD7u3C/R6+fc86DBCZbKknFFhx4Y+JuNzRnk96Evme2FS6KaSrClDYmR5ST
GCVdmGuTmSy4vB4Xy2s1hziNJP2VncUNPAbEFKHaTivg84acIJyLxUnoI67zy2qvVq/ZjbjnZvV4
GlxH7Gu2+jrJwfd6tqQD25A7URxy6iGavKF9RT0yDprBz3BQaWRB9es6iimegaYPIUVHI128YgMv
8JubRSi6OoHbZQLzR6VfeXN+Eepg4J/LtgDXMbb4JfqQQtIGXAyJFw7t2stPYPb6oMHPcpSenTiJ
t7uExbM8J6xC9n055LgmYFLu8O0Eu3D0S77JrWfas/H8knkTAtFr6cBGz72Z0wZyVq0zEZcFOtZW
0Tof7/HKUgSN9CzjQyyRx5eFPQqArMqOdBR1TyITuiXbsYVBwHcuTFJDRTgCZfkIQIAEf/pcbyQh
P/nC8nUrbAtLvqZXKRT36Q5r3Pgo//vjZESus8XaTiBfkEaupqDyz+jOoaHtZEgn7d2d/EfXLIZ/
cK9ukZuWSPFfDcRy1dqlbNO3QCxNdONWIYB7ieaSRFUujR5M2Ey4BE68ifow+PsYXajmHhgY8Tze
4c4gBRbCYnHIx3oPMZK06qZUUxYNrvA9OuE9yJ7C2kG92zXpRYtAml7MaUJ1CYAl7hD4fjIg16sS
MJLRTJ8lKbw0wg5K54dYShKOKVZmCl1rALFHT3Vzo5Xe1TucNoAIvNScoucz7ruoOiuKurmf/DS6
j/OahfWw8PO2FDgDjSIPy7PZyVTUG11GAztms22WGcv2HyfIXa/A8aInYZ7jAdB+J4nFd9e0XJwk
fOvWVfsWwR0rvvBGCaWdHnDD59FVnds2k9YZcw+ltbxJabLiMm1dwTAj24b94RC4eGk0/+kYoKmU
W4hJ0wiIAeJk9ZzRY+qWjcPbO8Yvt6f4wWhOfpaPhLZ2PSXu2S27UeTewvAdLdhEazIvq/s8uBnZ
F64BzzX6hGV4c7XtzZY/ZqRGUytRZ8V0dAxrITL0IoCecVNcxxLNYJswbzKB7/QHcsdyI09DHqXs
Jc3skEQe5iOrY814GGRTbDK0yx1sveoTVoWAuED/o7UOt5QT0DWD4MGhEbU37+8ArXPtndqOVYyy
xwk9mYfywGP1sR/CCYidE75Sf9vvOsYyh8NSikN5sCPsTHW2EWfuX+bj6gAQSf5SMYNgONaC99r0
gEjfXFKaONAd77IjSFWod6X0nsS+XbLAgfnrrBOO7ruQoIEn4V1GHn6qDspn3X3BQOAU6UyJGa/6
jRFUs7cY/OrMdDpwHHxP6sb0yFXJnmw4jxStR3eNZ//9HvuySiAJO7v8ecR0eWsrFi9g6uZgTqwZ
wihXD5Ie73mhTC5xWIqKZONfdzBVsmd9FfgOfnzT6ykBtoUcJVxQpn4oyBhuR67bFoM2mkrMHWPJ
W+6wl43gb5e3ITudFgB5gM4+X+U9gTVzW/iETK56lFcWpkMQVBjgtOEXWJ8VeBpzPUDNTk5EwMuF
hnSHBu2Eof4ENyI2b/XnbAfzkDCw3GXdraHatVDt1MazXFI76nR+fg4ooLAuncKSS+aoCJVfLhaZ
bActqso+OQdDDqxIlGA7/p/nUYr20QdsV+ShypFwKCD+JDXtX5DEBVf7B6yZcV1LG0F1PEZHrl49
8/fjEgxYtYG8Ec+QdxuHBgbw2D1zXYKHJrNCRGq/jANGJ2dtA6JSVxlDIvdGwd5Nswydrquyktya
xi6LIEbpd1G3TqnGbKjhx9aXXouh7iw5Xmsg3FduQA39lyJ28x2EZwdJ5cYcab5ABVDYiotsJSer
VfYE7o11GcqivJ0KWuCc9fvJqKiw7g+IXl6EB2rm+WzH6Vp/roFXxvd4yUOi0OmpLYVPBNLWg4it
idZzRB9jxUZNIMmRKPV+FVm+BJRXjDYFowfLG4XPXUN8ZUDgkm1vt4MlVXnJe1X89HPQnX4NSyOZ
ynctj3V78xOsBLhYS6hNdlJ1uMs3bKDBL0d14NrXH/rC5VglhMNc6KPQGL47DTZ+cVBKvuhKjImR
uhzGJ2+/bVwG/WIqiH3nVaHlBb4aL8YXpdw3TOok9wOROA6JbzIdMCBhqMvVylBloO2Yr91dD1AS
Jfv8t71NUFRywBKvAF3VlJ3M4k5ZJImgvp61bDvxHRprNLKuzrvqKUEtxpZw80WVdiwr0NkXSU2g
boz6UL7or5PWjb7oFoykBMjjkUmmBXpQGHkyFQaFKLu/UkA2iYVVmdp4gc2diHoTiORixt5o2Gkz
C4qI0LT2sFmvi+DQ6MGTsWEyzPybGUfXKwE/p999QxqgwpX35WCVOZUruLQ1tOdJq1um/1mT4hoB
uFVdOEvikf22DMkXYJeHf9Gs44tjMWuUhsm6kIo0r8o439eRWKQ3vcHG7ZuUi9HR588nmZQVBuJ1
jm0xI5KD7Z6VTeF4PtXpFuSYAoM07zESaMKbd6pc0nsirV8AA9VforlGms0LXOuXsorwBfhhk0fu
Sn7kk8k1HmkFk81w18vSabo3vwsK6ffcV5luWbRvA0Ss42SMoukNIQ7k6Rn/wYlVrjkA1p6MIFZN
S41cwePeiEB3KDYMzIWPsWPfhlZZU48nEU6CbfdrXe0bmwX2UnKiO24L84dyNMiFW7iZllS6pNvg
8N0ms/MXZNBDhiNM/pSARdvxLdZbhOqS86WDP4uZL94Zw6H9AM2AKDuCYG2eRLbvp4ccZT+R4W3X
loQWV6fs4HMNPn6oVnxY8Ul2GmcXknvawYXbP+7uje4PL93ogZy0lsyN1NOD4qE8dapcJn80X1E6
14grUDi2eokQVgv8PDIKR8v4gGbAhZazJjdAulvKJR3QqpMuRZOuIjW4ptTK2U1E6DifLWKxPjhy
QUEajkTVmYlpWAz63fiaJ30NHxRqUiEXDd0k0zdGv21DCnYTioB4YPbwA60G4YczMqdzc9cChglb
iKhap/NteBQGKZuuJpABE68gxPBm5zSTz6TVj6M0dC5+CLR88bT3VVfhHkZ6tXy7kTrUyEe/azxS
GoMiz66ky9mGGeH8M9bwmfncWdyCkpNheYKChseJE0UpPMwQSIfavoaKblCFUxycgcX+MzniZzpH
e4VlIgNGsdLkuLTyULqx/Sz/K262smm6801h7vrzvvu0u+LrxZXtGbce+X4KuYoN+FAhpekuyNkS
X+HIfBnGWLkrBrPZ1JwEOQRYdExHxfqFrVjygN3pcsmxQhXue5Mt897iqsgJ4Vpuxv7e5bCZIIIN
tEBe44WZDqLAvNcgfDjEffW50h60vf/w2K3rc0eE1wpFptV6KKVP1TV296YnwpbtH6KHqMtSiZzL
v1mvTqey+UR6IlZvHa+xKR16hmXNKqGHVVgEpHb/vStwJ7eN9CARDRFsuYfBQiycKwXBoY5fqK/6
2F0KK1VpNQaceAJSOKowqVWh0aO2WHYyVclP+igepYvRLInRV+SNbHMylA4m8/B85Cm9YrhbB3DX
Q90c1rN/BtlaDyHJJX6QCAxMkq+iKn4vN9PjkGg9yrVRVT1Y95woMoWs5840c2amP7DfYhwkgILh
w6mmFY+808RkPekrFJrjah2H0ZgFDGkKiHr3zM6eGRd401KHN46/IRLNmsnb5fuvJMD2/nzVhNeW
MM9SxfihVYWZmf5TOIGYSXNbHDQrXWLmLxAbapHnHbfAs5pdyruLoMeYPr8Wfbr94y04/Z034wsU
JpwQKS1/yFrcgW6D4pD6iDZWno8SC9/XvwnYc3X3jJTOEBkqpcybLpmGklZQxkg7pTZrIzB90Ew0
EpkB4QMXnDwa8KRDM37luGfmNRD7EWYTeD13xGvh67vJG0oRdwJyMfai/vq27ZF7Iz9hHBTHWs1U
ptpuwJyUxsXYNDXYUqyHS2IdciiYn7tZV9A1b5T5fJM9mTsBRoN8ZqUYtsFjsHyvD9wJSJMgoyWN
478nitLTBWN2qkEIkCuCtQiLUc9VwJ9iylcR++6XiB+WwRMUU5GLErx+8mrzY/R41WqbNCTX116z
QdIV0zcikA6s5ayOAfB9NwlXtU5LJalLykaDe7vYSzjODk2d08p/Sp4FsfbwCWFC3fXrDCy45wPf
MLI2hIpmsa7cVGfWZBgc2IJwTVcMN+LjToqK+6faaoaPIH7PH4LZyCoHciXjhkORE7He1+g3ZCLN
EE40zhU7V6Dl0eJlqdWweog1ZlHc5BosxdVoUIXP7UdFR+7+VlGMk8YovR7xdE6n9Ciar4jVqje+
QXgcdkQmcEeLZQpL0jAg2k6HXOKHLV+ubgMGQ/VqGZ0KPkEc1Grhogl99+m+bdCtV7pgY4L0qsF+
U9ABGMoSBwlhuyK8rUHKy9ci78M4I52qrbbCjOpTu6SnehHl5OtDdOpEcp3JGtiiST55drRxI3BP
hLUcE531EGt1MNM4f0L+fzgQyIWOIBJyDef0AnSjTG3iTbIrc3rs8XTDdE3xwmfAz9w09hOszpmf
wQZydcnIxwriwbvZhlLjLT67K8U/OqyGKLkyX0qMky6Z6IJnRIySy+kJhvTlP/uRfsCm0aLmQtuY
Ca0CBwwlEa8Wbh87iXrwTtLeKWiUmdEVzk6XlWsyPvIv0PGEkHQsVZ3S2ecgjnhyrcZthQiLvEDE
P6g57JWzjimTIeMUD/ukeThoOqFmbEcqo3LWDOK2Bq6gWSvjnw4Qgu67hMr0xE2+6sKefoHxlDQX
SciYmz2owd0Ze0QZtgXkxJKFhGTIiTZaXUY2GW7+Rn3eqRJYnOAXsocmKgmiKIw8KY8a0DzbRFII
95xAw1QnzrOUQ07EfBlSTCoiClNUySyxJfLEzKOMXi3t6C/jyeUvkBqDalzQIkrJ8V3J71yW4UNK
TC2H2etHSbCDo3iARIa6rqlCJV9fTHiG/ZiwEZNgRSheIr5QXrgex86p3hcgwF+lum05j02VuhE4
alKiYungoisimAVYxDMGEP3DKS7184O/5Mt5d7utBgLbGK7AgaPmDU+3lDDBNuOu7qn6Ki5MXVeI
dCmD2nPqGJRVM6+vIPNlRIJw6qiVvR9607Hh+NZFJAd83FitZYhImvFL5DMWfgSEiisZ35Q6V+fW
rHgJnwXvUMYAdJzNoYpl04y9nMsSimCpLs5SNWNMlIH9f86kWku1MsZYGJIMRdANd1M4lqgVAPuF
9IVAqNEz6BKEHxxLX2198DvG1GGtAFAqfsStiDlEo7MkpCVG/dJbVnwAa6VL1bvQU3qAGDIl2TMJ
T5WRkxKlEEAURNmm4/rwetFn2oRWh1gcw2RqqJTZiC1qBzh58f4ZCc2uRGU98EEqMtmXEm4GEAZw
rQLmi9fbaIokGyoEWr5/mBVeK/5jvJE3nGfBthdrh+MwPr/T8T53elCK0rwHdJP4qqklKIr2qTHZ
Cw0CoSaVvQybaKBVn9tyhW+3h8/o94VJYWj8kze82TTJ91M9myFln/Fo6cieFOvX83LRIeAalJv1
pf8yoMrY4Gz2Q9a+XaD52mPeJ2VdnKWg7CqhTWXfPLmxYIpUbW+EtfOM8nUd3myQsO6w/ftMypbH
/9sY9zr/a+BsELnXnp4SWSTYkGKhIedHPv6BBH3DS+D+9s1vjuvoeVAOTdJeeiTzU3GEznLJw1F4
b58MQn04fsLEVBleUx2gMTsAB4pIULVKlFLaElGFSPhHdbGzgmfY7N0+JkY6TWEnPzhh0VuSpoJS
UMotPaEVJHuv+3p35yqJp/7u2rcmMT9RZ7rXgh2l7Sy3x7dmvnu1ym6r3WPDd20yHb8B+eyC8WCz
m7srtCTuTpaOU1nNKo5Vx9YV1/jBaZeO6afNTLdX4Zu/anUzNaVz+cexy6Vlkq/yzb9hyUZ2BrAv
Zst109AQ4FDn5atCLG9dfloJ5XqRU8l2VV2q9GGv4Szs08yLCwra7DpMjiNRAOADS61F7lpOhu0H
rJiJpxrJRoobKrINE3rgeJKEz2rl5QpnLf/x8AZNSZZN+BXBpb9Wrh77SgNFlA+FJJwBkintlHA3
7DUzCvVHV7FHLMv4ShBLXQnDxOU4b+ugbkN6z6EwEG9qMLEnf1hV4HPACChiBcvMSy9r6FIa4IZK
sZRPB2rSC2vjdZORHoCDPtlAO/6iefak/hrJj89lSkqChi4UztEg2Kh3f1YqeqnEpYgrfCK3Ksmw
fb7LzdIPdt+QkSNXZFdj2A8vrPpIWQh8GizKPhQxWp1TrpWZNbQfky6PoD07XAxRygohr+YP4IwH
xtGAd4xZxHO4Q19SOYZqvkRuzAY4GXkgb67sTKClCb6pvcLC+eBYLkIKhRnbTrfMtg/DuB8DJMlR
qVfbRv1PdrTyeE1ChBDQjMV3ZUmYUnCiAdzlZNpTm2cxxFjHLkCc8qqs1CFMC19d1GGyEngWivHr
gI8/1HKzBF44Glu1Hrj7+0O25YCUWlvZrPGBIagbHqAHPPUCsF6tXRYgR3ZJ7rrrytWyaptwu83y
anFGmxow/y7coeg/OIdZXBA6BgoONTq0yRjFVk6JqX6jBoyHqHM9e2zjEFAwGPQ+/ghBV6QyvcWs
qcrreuNfrwsY+jX2tCHrQS9nR4jDJPexcguwZbr4fZQh8SI/c49/mvzRp/qanwdzw3uTqNBBMRO8
vHYEZo6ogHfiW+2e8CaWT8gbUWDfjEFCIy/2qZIXoTA0LLApoVRZlv0IltNeTo54uABj259WaXdS
v3GVDE2nKMYnEnqTUNRApPAEmQoPj0pWHTtJtqXjbQJDhyTtAho10D0j8sMhayYtuHNKOOzYOab8
0tulV+Vt/aC2Fw9ddDDCBsWtVcYfLwHv0JWpzjyToE9yxgGXk1nDSWjh38EqQQc/amQorYrs9Z1P
GDgL0CN137lXl3fHH7aB0CcPReC1+N3ogl48fVIJULK0ywQxyaAxBN9R0IZLImseYh5f4VUuW4MO
EUjabsum1Fm0wvL5dzWC/GjuYbXQMphG+aCdx/9qdcYTBoCaxgWt/VVULI6K41BK3s/4FLK0RqJA
hPlE5ZEapbXwtayoN6kdFcIhdo8brLZ76HapzEkMC7J6pUiAE2jBi3Bn/J9G7O1hRIblfPbCgnPO
ibZFYqT/HC0XW3+MjM+Fdjq2Awnw26Vrvv9d+f7EBUB8b1B2unkeWD0ygt9Gtf8AQa4Td1fDRyVK
9dz8cxA4RngWltj7nA9lFM2zDjLRX/Y2NwvyzcGrGB4zM88QjxAGj1/MzpOvBJm0x3p5YiV0p1fE
VPDc4gxiWqWeY0RP/gUrSM+xQTbfFsV6KSpbGt7hOXGeUL956Ey+Qw2R0+cp9J4J4V+dhWfYYz6k
LLLum8yvyC/v8yqZNrm5jCwHGEyeSIINI2bfThfx5Df+c/wF1gagdfHVcNxWD8tiJNo3+NZxus4F
B2TNNFsMFTCMTKxbNCihU8iD6AdlOPPK2ZfzahrH3H7wUi9sEQ3EZniNoqRCQ2Z5nTgvvdLaiTjs
14aQFI+GovM3YjXLB3mn4qXLI9jDBlU9Ckeo0+SLOgkxB0egvsOF4vdPOmPJTHb0NudhpUcrSYXy
gko/6qsv/crOpNFf4TVqOfD7nYujhX+oB4XDs1aH1p2MVximLeiJ1CMLExLrBJrFopM1fakkJ2cl
i4DYFYr9bkYQJF+fq78BUM0fmquOCvtKt9re+7EStX7SbAgkqZtpb6ILv39gBeXqaQQ5QYnoAiK6
wujoGkg11Hz6f9DG6aSTfCajMoljUKwEFgWfFFksvnBQDRooTWobMPnjXWc/Rnlovu50OQTvmkHQ
aezWVvZVTmk8u2mzRTSaWrIG7kf6zSYnABZOhXSwnBrv115BjebFtANU1tn4XYRSevdDb4a4Ldph
FyGz1UuOJQ7NTWzAkNSU9hnr6g2OM/r+H/ns+HRn2dWZPwHd9WfdVHMt4YgJALt+0iquA3MLlj2E
Ut1PO9cA+9yre6rJyJr+qOeLFjbL627rZaSf6TKt1a95cdqReCCNB+7DqN/8xVI1q5g0Das+JPg+
A1DXPDEmELf06IAMVkVc+IfUjkTT5S3KkZNbDoUwKqAqCQC23e/DPMx8t+cwtSV4rm7mnlD4CjEg
43mTOfdI/lyvvhHE4CSWiCAjakVPVYYxnIyyMyHJ1JVUADZAf+yUOFuU+O4KtvCyM4Br842MK46o
eHVZUvqtbXCK2UAvwy9H3a5LnyQypMtPcGLa1UBzNIqkd17Ig0EFBgugyPpFsA7nNIVinrE3cEkj
nRevELvEV3FulFTbYdALtLC5eKnyHWYW9CPNCh+KSD1euPXYIkPrzVQalRFtuD5ljH9Zqf7pE+1+
GhaPqL0k+XeVqObPkAL+wKtMSiCmWDcx+rjAjWtgEKy+qvUMG4N8tn9hDO3Wlplt9Us/Veck0H2E
0hURvHVVrZtQxalzygyzXRd/6XYjoPX0uEr5Va+7Jafqqav/hctTwL1UhXx9ySGw9pTBaDuzTt87
Drrj1TPxffY71I1OFJvW2dngGatqE2eLPA5oNtYBEtqoOUQYY4sKx3/WeYVqTNLItsj2dnFiPnVe
9vaypc3nZUNiw7Quim5RZ4vHnj7pZrS2TySGrxH452qgukNeKxSXQy1KDuyoFR5ODzw0tGj+7UxJ
1vPB4G3nw1p4ullmOO1HW2otWXILlQCyk2c/JAwm4D3IVbz8YyI54R+C5Ozx0Fgh1KtdX3N024Jh
NT5IWxA/DRjOM98bosbLZnU5Q2KscrhAm64AnWIemm5o3EqZN7ziWyzfLz3UlhKqc86C0IuAmGct
sIX9xGQkTQONs2v+2f3pkjP8WOS9BEwPhbxGT4n3CbbrhIFT9SwY368s6Zg4r73d4ug/O9Oj6sZq
z73epndGO4XRdQV4QVEuZRa6I3kScN7XOgOy0cWvFq9TlmMEiicD1niUhhKkWlXQdOcrl+pUFUnA
ddiJsm3NeaSKv5ZWG7hI81iDZ22F8i0AXnHAu3paQPN3KYmCbtCAZgId/E/vc2kpPbQKNacO6RuJ
XR97wJbYkAJ0LRF/ZW/Q/vqlTThOCvi5ohnZlifEXlI+zIelL4aeRZSPHZIuNX543BLvLL4WAd0j
VwupKJJW9X/cXZDhzo2THfYAL2DLdo4TVnhZBxcbkfoyh2HADtegAXx2mDhM2ZLtlQL6a5vszNp2
cYA6oBIOniQKNtOlrGnvdkdRRNx6w1sbqHXP6F9FIIIPNLB4Pq+oN61k583ZXOUxKEy6xgMxo8G7
CE8h58/do7cIg1AEAAnoev77tXMEGQEJyyRVYwaVOtn4EXwGTM9LwNs3DjR7IjIe/bnazk7fc8Dk
YpAGgB8sIL3XchnFbSAelRitGqKzKe+5Ea+vF2FiHskYslZ9s2YWNWlEe0um31YMqRic10SVKaT3
qvIV4LfLiX4RVLS/LJAwNeRuydbPjzHcZPp8YuNtU9EYz5DfmcQ0xMpTLv+JKIbCNyTfIYuQNUqq
W5ieHrB12HBPlhEaael7RDWpNq4EMJis/aRrxWbNqUGKdbrRYhSXP3XuO2P7t6q4buGtnDpmDYhZ
v9h9NH5Y01v439bPXJ9eCR15oCsXdC3RA+Nnpgh1C6pHFm1KOy/cztVoY4gxMhJupwJBG7khVk8v
jzUI0wFzrCQySd0ogMz7C8s8tfQBk2gIRU/U+MMRQcvR4MdlpVeqsTAfyIddfau6/u2Du3zX23sO
klSB8w46+aZYf+DMagwrFN2HXmNv7uSIvyvVOBNMXCCfHpyH8/IXWFkASaqVWEtgmTNTzfslY332
GUH+juvs3tJgLwoRmpZyFJ+LpwvvjZdR3EvqFEagIGWmIbCkJ6wMoJLrQo/KLMRDGc0T2SLvAbfS
OJ6Y+MEYmzDCh++yIYiXTpdfw4Ue90vUrCy2gd9lRH1cAvcBkmyLv9s312MqRabSuMztI+DLi7/L
WRUhwPX3fDDPsHTIFo8PNIWYUjBgGk4dMhf+ltu2PtPHipFBGafg768N3/zRPihIQwIzhsOGY8bE
5cOvS/miwK3oDeJIpjdTYMzoE7ZgPGlr1Mc6I7/LFvZxOU2THW6f6t3AFf/iQh7Dbjo5GvUlN6LV
/8+WzBlGIuRwgE8Vm6hyt7NNJ9l+w78U6cN84EljLhMsz7xHgQg4eDZM8FOwzi3vmLGIzXI0/zNF
c+B5m+jGUU8uBNjhjtFNaT9ljG5gdmMa94COIy+z4DlKt/+ZY6UAZRsxw6tXHEPgBNbzbyqgfMKt
xrwqEO8hwoap/pXicLDH5dc0bT0jtTEuDqoeF28Ioc5phbUbAe4Mvl6djC7Bpyp3zmDtCl5PpnsC
7wbBh0S9Lp4DmM4/opURqkc2JWdgPOrEJnK9Vbv7X+DuPwiOQc4644xBN2Ikvj7epZqIFNZCbO1z
X9kXm4aj/qWIDnpEEBwq1ApB2GViWihgvxjtTabfHFdggahWaHoRghcbYMj67BXjEMCLTzN2+lsw
Gfl9M6rYBq+0rz63CTfsD1wfZ4rIKaKPbp1SMA76xh8zQ0FzvYgANAzSjTmA++wwx7rWFVg2+sQL
TY9vIBUUtbREcYFQQOAQ2pSwosLo+hWQGZk/LT4Vv+ImCezn8V6IkuWupKiDyUDw+iTJoL4n5EoY
Q0NSU1QjihplW+5tHVRi4b1COhsacnW3mwVIKrpY8oOM9TpaMHiY3PFYHwuUMWq1tEOjNNG16xFn
XRqSeo76bFhQDf/afT7zTqhLPjHOjjqe+a3KUI+N8pMOxVK8jKrP/Ab6a2nBkBiyBUgj6bHjxSSR
Y+Wgjki50DAFXEM4EangKYVSVu8NHGita+flRoteSehg2y2NGEswxffx3lYotWZP8TRD+MiNvTlb
38lMBA9FXdsOlRZklU88HenGZAzIuPYVsFSZDR7KuqV1CIHEODX552mumZi0rcQuuRomVs0wx4vc
JH1upQZVmObB5RK/fsC9TitxlIOc0p7KPHqTwyAGeCRR8tBEPbH9lKxvCaMgfWEy9OLgdtuiNb+M
LJQIQaytURASZTukL6mAHQ6DsdjUunFxR/a3gKumO4NPLHnGZ6wbW2Wt56RKnAcF8fu4OtRdglYF
lNH54afqzjyWVu5dc0Tj7C8ybRtW8e0wt4PGrZEARGdy1n9B9HapzhkqMVLzvxf+oT9J07qZ6eJ0
ndOTY1xdSh0jr27qOr8VA3NcDC5XugNMah/pRsYmOZgef6l1jqktT2T3WZic9+oQnPrcejG3RoIn
4t8einHaEj7jWAAc4+jMlWx1NL3jrBebVxKyVZlM8kbHW1dp3vWXrf+szBGf8umWR/z5TlaqOLGM
ch/w4y83dXFNtDILP7wdtp9/hfAE6Yn+csEg7Ztk8/2DhC/cIVwcLT5V9X7TvY04Dah6tRkFyBsb
nDk8GupnIadS9RkzX9ntQWSntE72a3Mjzptr2F47a5LvSXasxScbjdddG4c5BAwwI0dZq5pvzVPg
FKPPyzl+cH0D3OQgzMomm/vSCDwfH9FT4wlnSUwYn9L4cZ9M25g3tUnEwvvmH17LiX39TS5WqJRg
ZHQ0vvoVRSbwfFncvOY21eqPkH0moX4zeNnGn8Q/7Lc8OXaPVcEUKhfeENV6Xo9C11eGkiO8LvR1
IAPc5+TP7a0JjMDTSZY0b7xdbXcZZhOgp/IJuDHoWtIjijGUNc8nGH/Sax0ob+GyaIBA4N6hiXC3
q8rt1YUh4V6os1loeop/4UX5zQVpAMBJvK0ssFHyDuTLgv9ElSMrhgeY0B7/v67FTAMKbcMJNhHh
9doGpkjJLR5oLP5jCwYrCbrtFGRzKcvxPIxC8fJRhrWgveBkSbOFUJHDPu216dCCKAMBFnMSOvmi
9dBj/vIMwgdrN/eSWkLl5TZsnaOs7vUxrCyPweSX2arbLVMTVEUHc0khzNSi5sMQNRozzCJfFDWZ
8BfXpGIbKt/hRkXvMV4SxOoNxZdL5evhR2OOP6puBrLEW4KXgJWOuAR1NePdEiA8htsWjyvb0kJR
0MWax/ZR/Mnf4o7UE8VK3HwNU9wr6lM6r/V54RmBeybpF8g/9EIsZH2oJvMGUi/j0/1tnJ32/IhT
tu7HR1+ajHvbirHEs3X0jqjVkILwKpfORiYWPAtQttL9S42CRVTPg5/v+VIVCX0V2sb8fBhv4WUk
TppoDacjjkHOOTkKi9RX8ftd5RYS4grs7y+7+V4qrlebcbjNkSW7UXpB4pWkzrnaBV/xPU+K/HGb
s4WAFM9JNybEQ64D+E6jVHkxvCgIM8W2uKF7KSiHAOeqDjO9Jhekijs81AcSFHYPJ6aSAswQy1AZ
U6q5/XnfpEV3kN8oTFtrPU5I1t/uHY9o5oOwpYiFYVDS6Quwi6/KPamqoa7NUDvCghXOmM2zaCiX
eYcY//MV9l5fCBo3v2bfezalEr9To/Ap2JRnlVGNk7Pi14Oav6JyU+wYV2JE5nFNKxvl3skwM2Oy
gQ30oWQjFcAak0lKDozPtKLhGGjf8legGDhCVNtQEAE1BWkewH7/gY2QPe6xeCJWxHs9UNDBad5S
xdSf9XONOisK/0h2sksViOW2tZizWy72P3jmJGDnl075sVC6aeQWEtXhXHj4qyasIblMABhX90M+
j4AujpeWat1DoNRyYL1rNtn6tywuMCYlArKCkmT3occ+bYTpS+5xwDMwNOmluyWVvOCrLtPTE/Ur
vZnBHrwmYFIgu6C/mL1vVNKQDmPF0bklC4DJtW9D/hcK1s8qrsx0wMpYoTXzeeX7j8LXi9kRnW2N
Am54aiWIFMy5hC/FjKaVKH4ICLtND2mmF/u81qIlt+tJ5NUg+SyNB4XwXhUk5h/X0xoGGWUmDPOA
uWjk3/8ImcTF6D73mGkfIgGa7LxxvxWVOYhlfgOCpWUYXzIfEouXGj+G+41tSdiAPsEyW6sw/YWx
FO1fQfFwHWCuYLUEmOT7fmxmxry0E/0L+nWAZNkKzMg9o+829jcJRc2E1TlerbWoLCv116LpP4kL
j56aY+EjR/2ryURbubPzieKGVVBxsRXdKQX3poj3XUojVsLp6UHoJWBV2gjqb2dbepgIxKVpp7Ec
WsSOolHQC2YPOXzYvSAdJdU5XL9HJwTkf71I/lO1K2AXGPtsA0sdN4Z8F7PD6CaD67hB0rwkxvwT
9jk1Bl5qdvI0efyG3si8pfS7tBxYyc3EyRP8pJsSVJpAcM12Pyx4d/lPB9LlO8qNJ6M1UUmwBkum
neToKO2TIDosaqiglFH2OpL27VZyy5yubkjrJu34chOLTWejw6eagtyCZxeKt/qJSRwRCCq2D3N7
6iEVpvUCvRkGHDyvNoPdobnL+41wRkcwtNE1bRBt07wfYGksYnaVCiUJUHPfLmnSEDXv3KTq0iSE
u1BFtvRo2UxsrCS4Y+OpZNWKHx1j57u00kGSaJFJ3iUllGuzxQbKTclL7eBU8XIV1mskwNkPZN8G
XtQ7STQEFrKVocDeqzti2MqGNQEKma52c0K5Z+eB5bZq0ibBEXVMJfK98MCv3zEqUhvtvzfMHW/M
m4XGIBXbWHoahFMmQVch7rYIa9q0b2iMFUKe2RaKLc6jvSrqOl1iGdTqfq5TxHODUppH1pIsszk+
YOpVzWLoRTb4w77o6OECOgUXik4opqKBB0XQ4zSuGD7bB2sdDSkM/P53yx/QKYPDQuO6ELye5xxd
6N7Bd1W3xAJmjNONkSBnZ/aDeLYWYtCAm73lybWztT9PuEy5BB5nceZgb0rI4bANmTUwvKi86fXd
8T0UyYLdT4J52HhSBHR+CVVzVM0+BYBNLT3q0sQvkD8syVKEScVIZveN9rq2EVfzyLF5exynbxdg
wkU/oy6h/w2PilFlzy2seMjXN8y3qwlS/mnyjSgKwwpdI+826pfUq6UBQl7IPAp/V80dBqdUOfX6
qLULZvjdg2Y8nLjje4tK7PjKdfL8q94QUzZnND8b/GIqrYVSSnmTMRLHrOTaBHDdiznMf77wEG2x
ZozlJsI+iqgzMIiJVto1SZhZcbhormiZXunaexC2PXPy3lZaN4e3PtHbMS30yHEj61bDlZxyb5or
zNiqtkKEfOycm50tPjj25AD8BdPaytzLVO3bpaG5PY0DpcstYDQM2JxbyoJtYzSgsG9F1JytwS9a
vTkGz+/AgbrKr8OQmsiid/b2xUpZs/Xp0o6jC4ujI3fhMjreubwz/LVOYpM1tSveVBM2jIWnNTCx
gmG7WVShxau3h/JasGX/oqDbDFXCXlhGciV2kTLgXguYdSVQ85gNabg3UHrCnKCQiiJ20tvIMJCI
IpuYT62gfuFoe8PGKWKivcH0Fpm20QztGNynCJprYCbDtYY70Z7i05NmNHDa+qUITmuCcAkeNf+y
zfMiK14WQ3976X/Do836+Mk9u/wGsnlY0B/DbZok2RaAIpo29zqJ0ilBGYg9pXpwoDY/JgeKIjB9
5zebq+cP/zIU+jnpL3UIp7Lxh1CEk3KwIR8nPHY7p594EmDQ1aHINqAcfD1zK1s+A4N9A99J38Qx
AhSwzSrNSTysUkTBGBL/CWUDC0BxGXRjUpMSFyjRD0ABpLZpzbfxi4EhNuuqIhE7GyGGdrHw2SLI
+18z4OMJksdoklWrMao5uJFoMdlJ8O9JDNJldo/py+SYbrypC4n9JE73Z1B9n/Q//Js6ig1T8qIr
wt/23a00OIHbWPhbtwRsyZn3VyRd7NFpaoGjgZZrDhoKsFNx5yNFIGNk9RSNEIVmaCa3A9vqsTCU
U9O93Ew2wQO2jTmMfm33wkj0CWuYM9BNmm928jyiJLgJVOnX4HMOnn5lpa9LoIyXfngNkI/h3J5y
XS+h52jVZ0trnRW8h0Gr/Pp6QuA7A3I25c2HqB+EOhZz7HRMRFPF0U2ZY6cwLSmNd8OSSIQGJ0mJ
VLdmYKso+JtN1QWw6ZAu5dWksa57NcdJYgc2FG661L2+O3nP/NaFrHfkvAgJzhDZ18fDtt76F1pI
Zr4rZyXc6WrV0IJ7U/JYYC661lyoecf2Nxm3e0OMOFeqxIUHW7zD1K2+10F+NUiunWMvfzcugdjH
y/krA01ayyk3mhra0EPwcFT+ZEde8cOoANHV8/tADLDpHZhJqtb7uo0jhLsPexyjWk572fBn4EFd
UartQ2ApYZ7MSRY1Ki5ofsPzRkhu55XZLZ5ZKos1v4RRFAVsfzi1JkkjLL0ec5vRav05rEp+OkjO
nX77cfOIQfZ9K495cFPk4JsOPxRiR+r5SDeOu+1CauBPmL8BUSrQr1bde2SbsK28FKI2aIAI8Vzm
J7hxU3ZyBaSBZs5TQDI0NPK1rJXV1cAmSbuzrSfmR/vHrN12/UztA3Iq9r3L7UDSr25Wgqyq3jAM
rqpHsczW70IJYzXc5FLquNhNU3yFS/C2D7PilOm7BWTRemksS0Kj39/qfXCBZ5rkfDe4q+TwFMF2
JotCSqDk+pSuiD1wbtoHJCJaeAC65IF1dswhyupp0NCpSZ9l9oRtfg/7Wf52nzLhyGsOk3KDH26h
6MbVL4VUBWeMDrNTgzI1l0Ba6wOxbXCeYK9WmhrJwHGUIFxoXbFjmk+I5MEWxBvleNAsNk7xgfcT
pL0R5mDF2A8nu9rY+DwxErToJ+7b7yRIAKrr/7p7M17iAG6fTvFPTs0qBYVUNB+G6V+O3UR0BoXE
oX2Z/+dUFXCh3bC4SDEUQfLAiJ9gdCENft+eDPFrdJ/RIw5gW5pMrRJVPSzkCsbXsYTMs3GnWPRm
A3jD1jBjTyZX7tb1UZ9XsfzExzyAdqD4GDEagV9+h5odtIGT6YnE26BOplmKkLOYllQMp9CXka5d
fW0V64roVmQREqNtYG0Rb04PaI2oJ9PGxlegox19+nU+hAZyvSkUyrpvCnkdfceVc0ARqm7KflKt
LnF4fHc17dF9Zy+0OTUOhANkjOUk1j9vXaLD9dmwVRRzaHZ0eqsKhv3edZDPsTAsIAyqp1l1frHE
Wp3Kgi9GSfRMyPTLi7T/+YkdYBOhSU8CnKXZCtdgp1Sg6HLbmSOFRDqIKfVNoCuGXjY7kIgaobCZ
8OELvDMLWswuTIR6yTVyIR9fByNPLxRmz8aKUOvbFxK3t/5dLuTptqoBZko8KcZDALq2ksmRxV41
0gVgNneGrlhry+pxIb3+7A5lMTqeN9oVPogIhpEv+MhxrdxaMnsjfJPlP1ppTEmCYYABfkqehVOo
tCn4mlia+buMKIdzYSqL9NMQuhn5EFfV0aFm+h/yntlIothNMG6eud8Totdlc2OBrHypbSag2xTG
7R6qjCubs8nKFznKZE1ItGp+nNCryWzAGmcZPjSeNS6iM0ZhXnbjCHNZnnYL5xdj5DZL2X9B1HBo
lxzX51b9xnVAaO02yphLVQzQbUUUWLGHoSoK4WFO8Ay2Itwo/UsTz5RdDMngGgSGIUPdFpOD4gq6
TRm7dKyqAKxkKh5eTvMDFFKeQYmLfPJeqHBMQz757CyzS7I7IwdmPIamg4nYX0Wc/bF018/M2pDv
n5m5Kt4DTWPlwWODWYI3xtrE0MnjioCkjmIpoB/buHDEsqc3o8veNx1gB+5Jwp67nJlL5DvfZvfW
ArsLSukgg0pSAECgONUZ5YjUcUdoaVacxtLnrvJzUV0MjhF0jDBHHFt6PUL9ZuORj4OxWp7dPkP/
DgQ/vxLEjhBFxt4MpzSatbmzxRrXUpzAwdhq
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
