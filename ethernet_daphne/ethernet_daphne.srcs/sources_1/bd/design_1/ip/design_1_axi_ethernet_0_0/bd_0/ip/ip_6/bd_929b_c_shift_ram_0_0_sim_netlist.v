// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Tue Jul  6 23:25:18 2021
// Host        : DESKTOP-QTM1A6I running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               e:/Proyectos_vhdl_vivado/ethernet_2/ethernet_2.srcs/sources_1/bd/design_1/ip/design_1_axi_ethernet_0_0/bd_0/ip/ip_6/bd_929b_c_shift_ram_0_0_sim_netlist.v
// Design      : bd_929b_c_shift_ram_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a200tfbg676-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "bd_929b_c_shift_ram_0_0,c_shift_ram_v12_0_13,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_13,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module bd_929b_c_shift_ram_0_0
   (D,
    CLK,
    CE,
    SCLR,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [0:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 200000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:clockenable:1.0 ce_intf CE" *) (* x_interface_parameter = "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_LOW" *) input CE;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}" *) output [0:0]Q;

  wire CE;
  wire CLK;
  wire [0:0]D;
  wire [0:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "0" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "1" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  bd_929b_c_shift_ram_0_0_c_shift_ram_v12_0_13 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(CE),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "0" *) (* C_DEFAULT_DATA = "0" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "1" *) (* C_HAS_SCLR = "1" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "0" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "1" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* ORIG_REF_NAME = "c_shift_ram_v12_0_13" *) (* downgradeipidentifiedwarnings = "yes" *) 
module bd_929b_c_shift_ram_0_0_c_shift_ram_v12_0_13
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [0:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [0:0]Q;

  wire CE;
  wire CLK;
  wire [0:0]D;
  wire [0:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "0" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "1" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  bd_929b_c_shift_ram_0_0_c_shift_ram_v12_0_13_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(CE),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2019.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
klQ5h9hgPT4p1YBkLUYG8lguTKRhX0z2xODTfvC0iZ4ANS2ufXQ4XxirL31R7wyoNNR1hd7rMjwq
bhFP3bRYRg==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
m0Wn8/fGtoK5/3nNxNBBvNhfylkjw8jAoTERkIzypaD+FUTSQmf52TpAbVY1LPDo/g6d8tzIrR8n
vZ2G5ckAbcwDRGFF5wpZ/kzyANCTceWo7S+WbzLbzhWkG5fkJbJk/tafbSvqruxodmgtrUI2IZDv
n7Mrf4GRIdC1iTgv+oM=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
mC2bSkADVOO0t7I1BTHPnOuMcPIdeypycvBkfsUtXFD/WSov2Pt6qF5xMTcs6woq+465yEDvey00
mpX9JC1ypEuHaZI+dtH7s+A4vYLXy34kFLryTKJtqycAy6XqNtTegvy4giaDFwDn7LgsamHSf/4D
cVGL06Ul6gm+ZOMvzok6Y9TBltlEkZOZ4GncKspHPcn3EJar4GdDxKcPll1RqSFbA2m7bWz6jPV3
uOD6IWnIflyobdLIsuB8hse3kjXBG5sl6X8AP4armpo9TNNnRIRG2FoI6g16Sj6zldS9XF54ge/w
jnsP0vce7kfzvP1LjtFrWdW0raW61dIddZVz5A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
LC8srWyP8+H+elFwWxNw8UhEF94Tx3vV4OlwBcwFszd9xuvwYRipLEwY4ar7HEoCwJI7beW6k6I1
ntJSXGEnPMgIDe7PadSQIz6gi9ErRHWRhWD6bEDNd/ZjFQuOakXk78jzxOTKuOpQ+ExjdHImyPzK
mDR0BnkVc/QiSO7zpOf3yjx9Vzb0Fba6OgXg0iLPz1MUr0ahYIc0MAkw4pRBo6IH5ilqVbkgNQcT
G4z/0OSzsBxipE9o1elAez7CnehtEQ66g6i3JpIIwqwnpZNQ9otSnS8LN/spRcQLOUuBq9ynGkPL
sF7EUF1gLkTFFMGgnZgawWi0V0jhiHQkJuLKPQ==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_02", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
ijV0yStg7uRIl3uzK8/hlbIaWGHa9aPC5Eu/o1vErrwtArYsGFt3RCyG/S90FB6jkuLgqwPR8ZlQ
P9t/F2FWmEkwwjGbdrRKFfpbkjh5HVn0vvLKCP3SiVHXCOWxxb5z8BV+yCNdpgdnsHFecK1M8ydQ
C530kRu3UD1LcnZcWJi41LcJAc5rvlw/SP1gbl+I1qsRNEHsb+MK5vyjgwBZAqKyqi7/UK1VEPdq
myeWeCRrU0GqEq5y/PHBMknv1SqNe0d5qzG1rmAtC4df+iivMCc9xuHsCA7iqoe+ZKnMmnA/8F6+
nY+gx8AQNplCeFxWppKH952fIYplHtE2rKQyVQ==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
saHlQAxOuJkLItpv5N3fJoJP3EdR34QNYHtUBFx7fcQy8HAUaZ/9Tlt2kfpn9r/pk1MmJ7uf4z73
LyaG/PWw7v3yH/KIVSLeIxiaSHoVfdWH1/RDH6k3DTDNbWSJDOjGSvMTThUKe899F8IXTD5jKxCh
frGB3Io0W4klQV7ADEw=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
RFiy0rrSiVrZMC725S3XkUSDuMS7Z+xog1sOwADnIBmdCChtYLUQVO9oPjXIujfg3bBFTH3qzII3
Y63fFMSZnnzk+ups3RPMBqEPPdMQSwlpDzsvhCOwYHe/rsOPZvqd7lL6QOKoA3mS9TZIP+mOaTKd
vzQiVyfS0rs+QHdJcw49jD5y7Dc3clQHD9xZMAMUHa5v8hzX4IOcnkSUOLpsZptR/WM7rcMnTzCL
x6m2UC+xSrwrb5vEz9cePMHx9NJyO2DBG0HTEDzGQQkrOCJJjvBxtB3r/E7/3eWGHvMwphSJnfCd
PfvYXz88X+ZVCW9dTNfc75jI7ogVMSunmtUuMw==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
FDaeFfa+mHSgy4EmthTAzq+P5Ms5zWRbihxS2Y1JVZ/NLtU7BkMBZmCk06uj/QQaqMJ9iBQhbLA+
dog6u59EliY9WuMLHTbhSItWfQUQAypwmUPGiBdwO0BJjzM/WjDjaf6Qqk7m8OKEzpjQ5BHpZxTq
P5mq3RQ362iF8VjTqGhvf6hEb9fq9NHqWbNa6h7HBloyPZCIHX3fvWSHa9eI9HIPWWfg4s4/GC7x
ThbNfvPT0akUeV4oUQHcA8OZI9FZrmaydDRaI2V8fNisjml0ZuQiPlJJ5ypZdVSSCFEAH9r84poX
EJhkLbLxDNHdULJyrJSa4d6B0osrXeUpxrd5pw==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
I2jR04a09SJUj/JYi0EPUOrb9ghya2hn4uFTBs2P/KrWpEC7rwVeQAOH16B836hkdA30XnYfTBzh
oKoQtPCt00KzkNKmbl6iX6j+6thESjcW3psojefmyFvc+1RXZGLGsHJcU8bf8yaxsnHq2KtbDOmv
mN+c69UyaczVHCDtHj1mhxRDBGa3xalGkTGbtB58F+K6LkPI9dIsK1lWyaCHYwsOHYmOZhiv1wrl
vfxJeKJ/35A719km0fBbJikGO53tHRPZukDTbisVXPM8qAZ/GPxsxEpTw+1CYvoxQ4NPE2sKKiab
UpEJejh2CZ223EZDueQZ9LTFIVYGMzRTqqgUeA==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 4640)
`pragma protect data_block
bEgwG2V/seGWDVN9NzUJ/zIiAywQruM//i+ix2BT3pP8PVMi9old1kcSOOiYCZ1XfP+mLsFXwFJ+
5CrmCMOaCFTcfcfR2qxR6dMHOx/ySP1EKi89sbpUiECZQOM0BUc2gXJxsYqzyCZFhouzQrU/N7Lu
OR3hMX2uU4+VBcm740rnO+adDyrUaKm0K6wR62PPgH4qP3FvIHESk3rHSgdReNqO2GSTDSmazP7u
gtBNUgwEXVtBbie+HK3KCfiVx66x8Edg5cmYJdt5SRdfbTmQtBcyc4Oxa99BrjH4U02h+vcbRaU+
XJ6FyUtA0F4Y5cVzVjwvHftYLY93V4Zs745bWY0ZP/Dq9GCio+alB5y/5sJ0WzWr7eUI6oM24OjV
M5rpOO2b3LxHEMtfEzzBanS3IHl0MA6dCqBvEnOBVStTJhN+L+r00lxv8el41ef/3UZIEyqyqDJc
H1TTCckrlnk4pA5/+Ui3DPsTyNbY0A73CfKpPzswQQkRy2+LzoBbc0Ck1LF8zKuPBCrcygssyEgE
5CnqVG5vNd0avi7rE9bgPStSSfjisX4kdaIbaeJdxQMEJc6TivMx/lPTgifAY2tnC9qFZRZ224DT
Sg3G+go+hITDMq5oW6an8qoanMsIFTxBPhZcKYwgIP17CQ6NUbhVrn4uGpRgfkc5+Nidy2aemnIB
Ewf5tlcpBBgOHH9ajgEVGteViM+B53HjFxXlWE6TsrEpEMYzNTHrPZ7TNq+j53LTcDz9DuxsnEtV
MRBvWb6LxjC1RqEeOHjB4yMsuKAEN0LfFfwNtEeMtgR3dlVSGUHN0PNGtdHuheAhkB1tkYbys7ui
kE4wwv0NipZ+UA5zMoh1McQ6Smii6ZnRkY13wPDSbLf83e8+EUSPP/T2hSaXeTPdsBBd2wwmHKT0
8yapDT2XLw3X2QQzIhbdvs9k9Vz96lFMxtLK7x1QLazSFHcVdMz8wE1Inyh+HJf0/b3RRiwH7TKT
0doVCKluVssz/k5M9wN9hsYS692tkyQARApjRBtxgz1oVwxHwEPq8BvKN4qjTNS3nAEN/7rmxaa2
Py0mriTiOArONYGpWeZNDecuvLhgNL6I/Js6G0P8Q/joaa81P3NyrFXdt7GItJ/AVWGL8y7W2MWO
x2F1nUHVYs/XxRiJLrv0bnxEuIGh124cS27Xl0A8uR7PSWooPTFQqeXxbrFJfWmk7HT6n90Wk3tF
fLbHmSP5CxpDHLZp+d1vhBoHqTBMMIPwYjch23YiD5FmiUT/wwbhcDpbMgHBXcZKT9kS83X0oRh/
eRTCLZ4eVCq1lF4h4M7uvP9lZRJLCCjVpsTIxB8yuVmeFxgwkeUTbI3pB2ZWDzU2DZvXg4cootTW
TN+ZjefnfUj3e5dcK6WCc5Xy6Mgk8FRNTWy3G/w6d37U0z/fbXfmM79S162axccFGngCxyElqcZZ
GsxlqBksgwTKraO9/FNMzXNVzFl6mcVYUfs88dTXXvw/YJgdxiRTfjYaCB4tUpwlBMXNj1AzD65U
ppsP6UTYopxoJEvdZHs6tSp3Srg2fmPDOGp70MDjW+jo2qqZ14FrUhRtI5Fl1g0DkDVgweWmWqap
ngLsgF9Px1fGSVUYyPTLAE/v4k3/BtDXT/sCf8JwIxkEzy4DnP5DoVnWWLFpXssowjBD6d6jBD0u
0PobG08PbdTFvWhMEP7XXjTV5FPPJyGaXIrgfIMzPy7PA1zC0h0okbkHQJqVt4//ixCkdoKDDYrH
qbUglsKgjU7zfzjgf38gGIh8NrBmG+NkiJ9EdNVVTYS8sM74H2vZwbcb2QpNSekcYFy8CKw7C645
zSvvOEgN97It2MsVHztQralnMGN3bnav/dgBoyY3CW2zY3BK2H9k9KuXSHdV6BKk3V/sLDtOBmPA
20ycQyPZM6+eyLoSgDxoQbwoWORus+w3bvT9PCCJmi7oR0Rh6HYhaMZ75dxsmA/+K3expRIN9Iw0
voKhYuf0O9ur8NpFxIjjp8rexNXiKMxqcDScs3bLILXNRFbsX2GtdazxsJz32LjJMwh75A7SfQnj
Ytre+s7YkUor2zrzCUdOKJ18cY9iFDSToyrZItnNAao81dgkMoZ0r0a+HDPUDP1hw/scmy08hgf4
f0RynN9+bXZVCDaWD241vUl+aqIAFmZVymHy1S/TN4KFkKbP1WYgq+RyGtRCXSyMqcoCTi2tSiCJ
7zwFC+NGvgPSgCzNSkuz4eF0iWlwrcBPdEOJWAqcsQ3YMMk42gxYUVr7VXpNaBu9w0lf8NEjAguv
VSQMbchEBwnQdCNalouzXzNYXylYTlJrYTc6//DXEQISlOjJCR+DbQeBuFH0qsFMAyBCG+M6Los9
uH4ZEIfhRLZQWsT7I7XpzQQMxVNVGqFOt5WU8exeZh4dPomukgATEmJ/KhBgZGPsyFE5mTGjPIg/
MbFTAAp8rSPF8er2RMcOZfosjuxCki5QcCCRNZjDE+UNRlu3eUdcpdWTbXZmJxcT9L1DWL6ZRawx
SOCbPzIEFXhqK7iPXdY88yJdlPpBjDmNyOl67sEcNeWgQuDWQsDLjXf1YSWp7qcWEbTUyPnTC0RO
aNDkLe6GCUVszuvm9gJRBLBM2bj4BdKJb6oGlqtbRVki7bI3WlI9fJDb326n1cjzJl8aCC5Kp5+I
D5VdKIte7n0ycxw1cnbK+8R07eWMuOQk7M3+zXy1W7r8ovuqRCV6Rpaklxr9ALxkvjlhhvtMWx7l
7jwS/rXNrOMQKOP5HNJp5PWUodV9fCzrVYfOGNwBND3ZkEAZdetjqEfSTsrxV0I+Ttx2sGQgAzrK
SFAaX7JJn9oFD8B10d5UyCZCa47WW6TzJJJf5qNp5RTPpQbhWf8r3ekYPEije3uCCDC9+NjKZQ1/
GUSeNDPBXTtSkLU48V1cRozYHFcNmKQ0vOJ/rqPnPVU+RQzMPgI3D5Zw2yPEQveNKcs1nTL86qCR
kYdI6KHOZwVLxqWS01Vl6dx2R+yWUxI8Gi81xDcWgpJ2B9cTTIYVh0oMOsgnFJXgJee+R6TikY4d
vbB2q7g/JnP4me7d9HJuUUzrgMJX9RLt3blz0++JPGK3D3pqa58MhWU3xhIqllbQ2GkKVsqaf32v
OlkayRB+NH5sfXfHq3CDxoDQOjOwaorwjnqiuV0Qt1E74pTiYpCPV6gCiLMdBUKnSS5V7p3uCSRs
yPWFQy1EvGqz8/kIi48U+EAbJt3IwLR/9w8b/mWrDuN4OZ1l/FmLPLLc98H+TNo0ppgpPO6V1J92
KDo7UeXm2zaQFbh5fxSo24mMUA8nTXlIn+0C6IbQYDc2v45MswJHIJzKIiQXmQKd52NBlABcNdKG
CmXlzMbFmqA32jcnZVOzgYZn4QJ81SoYPgnb2Je4cIYDqXkiM5qfmKHUyTCYoVs5hEKlc8Ka+0hB
WaZ+sISkmRc0NG+9PdiAYMWKv0csHKhaVgoiUZcJ966hHeJ2wkCEHgwfQ3syfgNCqgqTGTIaoPOQ
OQVbIjnl+0dtyzy67aWc0kRVAK1YCoa6WmOBrleIG2MgQ4FvSFbh0KpKO1kGPLVzoJK3dMivIguh
nFI2P9d/UM8E0OB24By5c1+j5nrQfaXkBeb0jYCLW/3gXqrc38NJYkpgnrzX1V/Qh0tRDHZFxQBu
1iKo6AiknnG12Ust1BL6X7pEnoDP5SP/wYaFlk5OAoJVry5KR62+upbokGVLik50PzvAkdaC2xqh
fsOQ8wnGTxLhTuHj9Vm+0764tz7eQ7CAL7rc80TWMKYH5mHIpJVpKKqokPdhJIsW3QwXXa1/Vvb0
uEpOaoazFnknsEz2dveQGZWqq6UBjfYOAteJ8fOsdlKS3VZ+V+I9BAazgBsfyZ8DcrQrLDhHokx/
cEk42I4+jLwS2GGxMIAOO4mLP13KuT4Zxn/PTfgDjcoV9Lc6DqZq0PhXJq1/9GEx7/8Xayi9IYux
5dlxkc9DZZrmwg+A/Zjil4hHno2LaVr3JGcsSTVOKIRdAfyIJgmApRQuexBjJAQ9rWxO7/pcnUwN
uNMvqs/8hxkK765mLP633TGeVQoz3rSb0FIsbfRyZYZmW9VQV54vpjmzzAtwloum9Z2/1A3p2Ud1
oqEFYOQjRgrpKx7xj8eMRYncs5WZnOJot2FZYGHDb/PIKihcRWvP6sN5abMM4cWWQx4S+eene5tA
mNqPmsPLybLZu1ovIOpezeThEvC7jXPb0FnCRlmztB3xwTDWvMxozZtMRHDQA7Ybr0eZBjTWOULh
Z7beA+eyPuX+TNkGC8UDfNTFiF7ouiTrZ5NGfmkXHqxtIoepuowyhconP+mYr2MwUiyHjMB6O8aw
Ew6WgEKxm5mn7MKPOaayjB+82W2ugRZHnOKC8zTpRvD1F5fkpxTGg9fduZ7NAxKiiytsg/h2+VRy
A7mnXwmu2kVKQGgvEfjxiKEXmTZS/D9POcWtAQwhpx8NIAd9ETohhOo80cHjYeooxR2Tr6egrCkb
qJwV2UWjKY8IwK1kudCoD6dOLwH24GKKKvIs2qMmoSVTPIytlS223xAZJf20bOORxlJFV6uQP33q
PPRBzKS+l4JXO3CQIocKaI7PXKb38oTkFfDrcia8yVV6YuCHHNBkKWcEDjSccrnlpgxhBTl6z8gR
6bcUkltwcWERodXtTkHpt2qgsB6Bp1Jsr6hKj0CxxbysTDoPLND+k0XZglyEJkldb4SqH2/nBdFY
VCN3grtISYgfKJEW2vOAGv1SdMhOA9mebiF1ahDPYKHqGLTKIt4Mslava54TgwMChZu3NrFYHqvf
LeNm28p8uhNHlX3Yxz1e4FNIFRvvkZ00Sp9dY1g5T0jd76+XBxqCkvtGh+xc+Q2ZaQs23WpUxvfs
nhb0SGFLMGZV1cBMycx7tGtuhwk/XAVL6Y/HFXCB2dMGZoIS7VtVJ/6d7NZifZMJzeYbksfxsYdR
zMHkClyAGPH+nCy3YVXspplGvlWihI+vq3FwWGsYWE1X7JBajFBhkpL3oOUqn56p+FLYud2WNlQl
oPKpSLzFex/dKUOgkov7lUi/NsQDqyNMvHzOu6G+oPTk5xzRQyUZ7kgTwq5tiIFdjaeSiyJRiM5t
pL2qhtCf38bcKoS3QnYx1jumLCfCXht7Kdu3RPXjo41SFER+0sZa4qhIZWPjYL3DYlCGmJyMIax4
x5BYtXQkj4cfnt8J0/W11UGrqeZPS9mkv5YNjIJ0+7d7w9XUhZeVAR/xE8YJx1NV+invXIyp5evw
o0G9fP16z64o1jUbb3H+bOHZZAE9B1kivGS3mqKjTncHxom+mQsb2sqyo7Nfiurzmfaor3y/C5xa
cjXqfO941N8oC+1N6XofCcchyulruoG5cm2GP6BaBJIgpHqE/U1FWOqPUKAA9SQVoZ2tKGWk8lP7
1cnvyu5ZSu0DLA7wQVOybneeqHRObyU/5js0YLyxg3kCznGf1yAvuKNSo+VzUPyC6YZL62e740km
5SuP+YrL9bgsats/x6JuHOILIK9LR4G2TbAxQRBkgMx/GF4Gm6RH3Z7HjS3fhMXQm411+agoERY7
DhNPzEWBpKxO01WclTe9oT6knTZg4UWfIFozmb+d8UihLg0KCKfJxDB1uUlje8uCLqX0ngXGJtBw
cOqsPQC99sGly8JPjcisIAYZmSYGVyH+YEW6BBCdV7IJilP7SJIBVLZso9mQ7QMHit6g6jtQBCs9
VvTEBjGJ9OuIp3amaQdJt8a4vGOCZ4XU84bVnst2C2dFuNllwfoGFtNPNhZ5aRg0vxJo5P6dJbSu
n1uKYNYrHIAGcstji26J2xDYomO6nuML0QwIZ2Kj6nmHSy3ktp0i/PxkkS4bnNCYWru5DkgNuqsx
lMe+lLa6HeSI7re4cYu+ueayXS+ZU9pWCppE7LsOBx3YUsWYbksG+TIiYsh7NCAWlJMJNDf0+HpW
CYkCuc9lRy6h4qzwVPdOLBTaX8I7l7s0c4pzo0eTY02eGw4nXyQkDCKFJrF2z3sjuo4OnCNMeFRs
TyCKIzIw3eW3iAHcRzlcYo6aBliZRQLJtfWoyS7M3cdlfBDTxEjVRt11lwN14BoEdN9NbFr0eyoV
5/9z5jIPEaiF0TzlHEoaE4Cf0hnc/5K7IEOPomotv7cxFOF/jdHm9UHQbAj5zBWrswXSiLHvF5wq
iwoLR+fjJg/GP881SzNLhrT8nXG1nR4=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
