// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Tue Jul  6 23:04:27 2021
// Host        : DESKTOP-QTM1A6I running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               e:/Proyectos_vhdl_vivado/ethernet_2/ethernet_2.srcs/sources_1/bd/design_1/ip/design_1_myip_ethernet_0_2/design_1_myip_ethernet_0_2_stub.v
// Design      : design_1_myip_ethernet_0_2
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7a200tfbg676-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "myip_ethernet_v1_0,Vivado 2019.1" *)
module design_1_myip_ethernet_0_2(CLK_read, RST, FIFO_RD_EN, FIFO_Empty, 
  FIFO_dato_Read, m00_axi_awaddr, m00_axi_awprot, m00_axi_awvalid, m00_axi_awready, 
  m00_axi_wdata, m00_axi_wstrb, m00_axi_wvalid, m00_axi_wready, m00_axi_bresp, 
  m00_axi_bvalid, m00_axi_bready, m00_axi_araddr, m00_axi_arprot, m00_axi_arvalid, 
  m00_axi_arready, m00_axi_rdata, m00_axi_rresp, m00_axi_rvalid, m00_axi_rready, 
  m00_axi_aclk, m00_axi_aresetn, m00_axi_init_axi_txn, m00_axi_error, m00_axi_txn_done)
/* synthesis syn_black_box black_box_pad_pin="CLK_read,RST,FIFO_RD_EN,FIFO_Empty,FIFO_dato_Read[31:0],m00_axi_awaddr[31:0],m00_axi_awprot[2:0],m00_axi_awvalid,m00_axi_awready,m00_axi_wdata[31:0],m00_axi_wstrb[3:0],m00_axi_wvalid,m00_axi_wready,m00_axi_bresp[1:0],m00_axi_bvalid,m00_axi_bready,m00_axi_araddr[31:0],m00_axi_arprot[2:0],m00_axi_arvalid,m00_axi_arready,m00_axi_rdata[31:0],m00_axi_rresp[1:0],m00_axi_rvalid,m00_axi_rready,m00_axi_aclk,m00_axi_aresetn,m00_axi_init_axi_txn,m00_axi_error,m00_axi_txn_done" */;
  input CLK_read;
  input RST;
  output FIFO_RD_EN;
  input FIFO_Empty;
  input [31:0]FIFO_dato_Read;
  output [31:0]m00_axi_awaddr;
  output [2:0]m00_axi_awprot;
  output m00_axi_awvalid;
  input m00_axi_awready;
  output [31:0]m00_axi_wdata;
  output [3:0]m00_axi_wstrb;
  output m00_axi_wvalid;
  input m00_axi_wready;
  input [1:0]m00_axi_bresp;
  input m00_axi_bvalid;
  output m00_axi_bready;
  output [31:0]m00_axi_araddr;
  output [2:0]m00_axi_arprot;
  output m00_axi_arvalid;
  input m00_axi_arready;
  input [31:0]m00_axi_rdata;
  input [1:0]m00_axi_rresp;
  input m00_axi_rvalid;
  output m00_axi_rready;
  input m00_axi_aclk;
  input m00_axi_aresetn;
  input m00_axi_init_axi_txn;
  output m00_axi_error;
  output m00_axi_txn_done;
endmodule
