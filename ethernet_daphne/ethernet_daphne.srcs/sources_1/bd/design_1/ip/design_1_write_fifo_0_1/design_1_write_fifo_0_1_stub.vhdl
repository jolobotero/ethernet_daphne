-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Tue Jul  6 23:34:40 2021
-- Host        : DESKTOP-QTM1A6I running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub
--               e:/Proyectos_vhdl_vivado/ethernet_2/ethernet_2.srcs/sources_1/bd/design_1/ip/design_1_write_fifo_0_1/design_1_write_fifo_0_1_stub.vhdl
-- Design      : design_1_write_fifo_0_1
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7a200tfbg676-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity design_1_write_fifo_0_1 is
  Port ( 
    clk_Write : in STD_LOGIC;
    RST : in STD_LOGIC;
    FIFO_FULL : in STD_LOGIC;
    FIFO_WR_EN : out STD_LOGIC;
    FIFO_DATO_WR : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );

end design_1_write_fifo_0_1;

architecture stub of design_1_write_fifo_0_1 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "clk_Write,RST,FIFO_FULL,FIFO_WR_EN,FIFO_DATO_WR[31:0]";
attribute x_core_info : string;
attribute x_core_info of stub : architecture is "write_fifo,Vivado 2019.1";
begin
end;
