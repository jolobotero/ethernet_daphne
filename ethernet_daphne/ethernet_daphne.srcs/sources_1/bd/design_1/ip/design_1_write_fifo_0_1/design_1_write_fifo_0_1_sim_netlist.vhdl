-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Tue Jul  6 23:34:40 2021
-- Host        : DESKTOP-QTM1A6I running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim
--               e:/Proyectos_vhdl_vivado/ethernet_2/ethernet_2.srcs/sources_1/bd/design_1/ip/design_1_write_fifo_0_1/design_1_write_fifo_0_1_sim_netlist.vhdl
-- Design      : design_1_write_fifo_0_1
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a200tfbg676-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_write_fifo_0_1_write_fifo is
  port (
    FIFO_WR_EN : out STD_LOGIC;
    FIFO_DATO_WR : out STD_LOGIC_VECTOR ( 31 downto 0 );
    RST : in STD_LOGIC;
    FIFO_FULL : in STD_LOGIC;
    clk_Write : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_write_fifo_0_1_write_fifo : entity is "write_fifo";
end design_1_write_fifo_0_1_write_fifo;

architecture STRUCTURE of design_1_write_fifo_0_1_write_fifo is
  signal \FIFO_DATO_WR[31]_i_1_n_0\ : STD_LOGIC;
  signal FIFO_WR_EN_i_1_n_0 : STD_LOGIC;
  signal \count_fifo[0]_i_2_n_0\ : STD_LOGIC;
  signal count_fifo_reg : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \count_fifo_reg[0]_i_1_n_0\ : STD_LOGIC;
  signal \count_fifo_reg[0]_i_1_n_1\ : STD_LOGIC;
  signal \count_fifo_reg[0]_i_1_n_2\ : STD_LOGIC;
  signal \count_fifo_reg[0]_i_1_n_3\ : STD_LOGIC;
  signal \count_fifo_reg[0]_i_1_n_4\ : STD_LOGIC;
  signal \count_fifo_reg[0]_i_1_n_5\ : STD_LOGIC;
  signal \count_fifo_reg[0]_i_1_n_6\ : STD_LOGIC;
  signal \count_fifo_reg[0]_i_1_n_7\ : STD_LOGIC;
  signal \count_fifo_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \count_fifo_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \count_fifo_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \count_fifo_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \count_fifo_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \count_fifo_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \count_fifo_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \count_fifo_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \count_fifo_reg[16]_i_1_n_0\ : STD_LOGIC;
  signal \count_fifo_reg[16]_i_1_n_1\ : STD_LOGIC;
  signal \count_fifo_reg[16]_i_1_n_2\ : STD_LOGIC;
  signal \count_fifo_reg[16]_i_1_n_3\ : STD_LOGIC;
  signal \count_fifo_reg[16]_i_1_n_4\ : STD_LOGIC;
  signal \count_fifo_reg[16]_i_1_n_5\ : STD_LOGIC;
  signal \count_fifo_reg[16]_i_1_n_6\ : STD_LOGIC;
  signal \count_fifo_reg[16]_i_1_n_7\ : STD_LOGIC;
  signal \count_fifo_reg[20]_i_1_n_0\ : STD_LOGIC;
  signal \count_fifo_reg[20]_i_1_n_1\ : STD_LOGIC;
  signal \count_fifo_reg[20]_i_1_n_2\ : STD_LOGIC;
  signal \count_fifo_reg[20]_i_1_n_3\ : STD_LOGIC;
  signal \count_fifo_reg[20]_i_1_n_4\ : STD_LOGIC;
  signal \count_fifo_reg[20]_i_1_n_5\ : STD_LOGIC;
  signal \count_fifo_reg[20]_i_1_n_6\ : STD_LOGIC;
  signal \count_fifo_reg[20]_i_1_n_7\ : STD_LOGIC;
  signal \count_fifo_reg[24]_i_1_n_0\ : STD_LOGIC;
  signal \count_fifo_reg[24]_i_1_n_1\ : STD_LOGIC;
  signal \count_fifo_reg[24]_i_1_n_2\ : STD_LOGIC;
  signal \count_fifo_reg[24]_i_1_n_3\ : STD_LOGIC;
  signal \count_fifo_reg[24]_i_1_n_4\ : STD_LOGIC;
  signal \count_fifo_reg[24]_i_1_n_5\ : STD_LOGIC;
  signal \count_fifo_reg[24]_i_1_n_6\ : STD_LOGIC;
  signal \count_fifo_reg[24]_i_1_n_7\ : STD_LOGIC;
  signal \count_fifo_reg[28]_i_1_n_1\ : STD_LOGIC;
  signal \count_fifo_reg[28]_i_1_n_2\ : STD_LOGIC;
  signal \count_fifo_reg[28]_i_1_n_3\ : STD_LOGIC;
  signal \count_fifo_reg[28]_i_1_n_4\ : STD_LOGIC;
  signal \count_fifo_reg[28]_i_1_n_5\ : STD_LOGIC;
  signal \count_fifo_reg[28]_i_1_n_6\ : STD_LOGIC;
  signal \count_fifo_reg[28]_i_1_n_7\ : STD_LOGIC;
  signal \count_fifo_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \count_fifo_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \count_fifo_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \count_fifo_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \count_fifo_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \count_fifo_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \count_fifo_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \count_fifo_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \count_fifo_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \count_fifo_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \count_fifo_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \count_fifo_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \count_fifo_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \count_fifo_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \count_fifo_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \count_fifo_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal \NLW_count_fifo_reg[28]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
begin
\FIFO_DATO_WR[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => RST,
      I1 => FIFO_FULL,
      O => \FIFO_DATO_WR[31]_i_1_n_0\
    );
\FIFO_DATO_WR_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk_Write,
      CE => \FIFO_DATO_WR[31]_i_1_n_0\,
      D => count_fifo_reg(0),
      Q => FIFO_DATO_WR(0),
      R => '0'
    );
\FIFO_DATO_WR_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => clk_Write,
      CE => \FIFO_DATO_WR[31]_i_1_n_0\,
      D => count_fifo_reg(10),
      Q => FIFO_DATO_WR(10),
      R => '0'
    );
\FIFO_DATO_WR_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => clk_Write,
      CE => \FIFO_DATO_WR[31]_i_1_n_0\,
      D => count_fifo_reg(11),
      Q => FIFO_DATO_WR(11),
      R => '0'
    );
\FIFO_DATO_WR_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => clk_Write,
      CE => \FIFO_DATO_WR[31]_i_1_n_0\,
      D => count_fifo_reg(12),
      Q => FIFO_DATO_WR(12),
      R => '0'
    );
\FIFO_DATO_WR_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clk_Write,
      CE => \FIFO_DATO_WR[31]_i_1_n_0\,
      D => count_fifo_reg(13),
      Q => FIFO_DATO_WR(13),
      R => '0'
    );
\FIFO_DATO_WR_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clk_Write,
      CE => \FIFO_DATO_WR[31]_i_1_n_0\,
      D => count_fifo_reg(14),
      Q => FIFO_DATO_WR(14),
      R => '0'
    );
\FIFO_DATO_WR_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clk_Write,
      CE => \FIFO_DATO_WR[31]_i_1_n_0\,
      D => count_fifo_reg(15),
      Q => FIFO_DATO_WR(15),
      R => '0'
    );
\FIFO_DATO_WR_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => clk_Write,
      CE => \FIFO_DATO_WR[31]_i_1_n_0\,
      D => count_fifo_reg(16),
      Q => FIFO_DATO_WR(16),
      R => '0'
    );
\FIFO_DATO_WR_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => clk_Write,
      CE => \FIFO_DATO_WR[31]_i_1_n_0\,
      D => count_fifo_reg(17),
      Q => FIFO_DATO_WR(17),
      R => '0'
    );
\FIFO_DATO_WR_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => clk_Write,
      CE => \FIFO_DATO_WR[31]_i_1_n_0\,
      D => count_fifo_reg(18),
      Q => FIFO_DATO_WR(18),
      R => '0'
    );
\FIFO_DATO_WR_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => clk_Write,
      CE => \FIFO_DATO_WR[31]_i_1_n_0\,
      D => count_fifo_reg(19),
      Q => FIFO_DATO_WR(19),
      R => '0'
    );
\FIFO_DATO_WR_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk_Write,
      CE => \FIFO_DATO_WR[31]_i_1_n_0\,
      D => count_fifo_reg(1),
      Q => FIFO_DATO_WR(1),
      R => '0'
    );
\FIFO_DATO_WR_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => clk_Write,
      CE => \FIFO_DATO_WR[31]_i_1_n_0\,
      D => count_fifo_reg(20),
      Q => FIFO_DATO_WR(20),
      R => '0'
    );
\FIFO_DATO_WR_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => clk_Write,
      CE => \FIFO_DATO_WR[31]_i_1_n_0\,
      D => count_fifo_reg(21),
      Q => FIFO_DATO_WR(21),
      R => '0'
    );
\FIFO_DATO_WR_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => clk_Write,
      CE => \FIFO_DATO_WR[31]_i_1_n_0\,
      D => count_fifo_reg(22),
      Q => FIFO_DATO_WR(22),
      R => '0'
    );
\FIFO_DATO_WR_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => clk_Write,
      CE => \FIFO_DATO_WR[31]_i_1_n_0\,
      D => count_fifo_reg(23),
      Q => FIFO_DATO_WR(23),
      R => '0'
    );
\FIFO_DATO_WR_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => clk_Write,
      CE => \FIFO_DATO_WR[31]_i_1_n_0\,
      D => count_fifo_reg(24),
      Q => FIFO_DATO_WR(24),
      R => '0'
    );
\FIFO_DATO_WR_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => clk_Write,
      CE => \FIFO_DATO_WR[31]_i_1_n_0\,
      D => count_fifo_reg(25),
      Q => FIFO_DATO_WR(25),
      R => '0'
    );
\FIFO_DATO_WR_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => clk_Write,
      CE => \FIFO_DATO_WR[31]_i_1_n_0\,
      D => count_fifo_reg(26),
      Q => FIFO_DATO_WR(26),
      R => '0'
    );
\FIFO_DATO_WR_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => clk_Write,
      CE => \FIFO_DATO_WR[31]_i_1_n_0\,
      D => count_fifo_reg(27),
      Q => FIFO_DATO_WR(27),
      R => '0'
    );
\FIFO_DATO_WR_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => clk_Write,
      CE => \FIFO_DATO_WR[31]_i_1_n_0\,
      D => count_fifo_reg(28),
      Q => FIFO_DATO_WR(28),
      R => '0'
    );
\FIFO_DATO_WR_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => clk_Write,
      CE => \FIFO_DATO_WR[31]_i_1_n_0\,
      D => count_fifo_reg(29),
      Q => FIFO_DATO_WR(29),
      R => '0'
    );
\FIFO_DATO_WR_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk_Write,
      CE => \FIFO_DATO_WR[31]_i_1_n_0\,
      D => count_fifo_reg(2),
      Q => FIFO_DATO_WR(2),
      R => '0'
    );
\FIFO_DATO_WR_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => clk_Write,
      CE => \FIFO_DATO_WR[31]_i_1_n_0\,
      D => count_fifo_reg(30),
      Q => FIFO_DATO_WR(30),
      R => '0'
    );
\FIFO_DATO_WR_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => clk_Write,
      CE => \FIFO_DATO_WR[31]_i_1_n_0\,
      D => count_fifo_reg(31),
      Q => FIFO_DATO_WR(31),
      R => '0'
    );
\FIFO_DATO_WR_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk_Write,
      CE => \FIFO_DATO_WR[31]_i_1_n_0\,
      D => count_fifo_reg(3),
      Q => FIFO_DATO_WR(3),
      R => '0'
    );
\FIFO_DATO_WR_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk_Write,
      CE => \FIFO_DATO_WR[31]_i_1_n_0\,
      D => count_fifo_reg(4),
      Q => FIFO_DATO_WR(4),
      R => '0'
    );
\FIFO_DATO_WR_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk_Write,
      CE => \FIFO_DATO_WR[31]_i_1_n_0\,
      D => count_fifo_reg(5),
      Q => FIFO_DATO_WR(5),
      R => '0'
    );
\FIFO_DATO_WR_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk_Write,
      CE => \FIFO_DATO_WR[31]_i_1_n_0\,
      D => count_fifo_reg(6),
      Q => FIFO_DATO_WR(6),
      R => '0'
    );
\FIFO_DATO_WR_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk_Write,
      CE => \FIFO_DATO_WR[31]_i_1_n_0\,
      D => count_fifo_reg(7),
      Q => FIFO_DATO_WR(7),
      R => '0'
    );
\FIFO_DATO_WR_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clk_Write,
      CE => \FIFO_DATO_WR[31]_i_1_n_0\,
      D => count_fifo_reg(8),
      Q => FIFO_DATO_WR(8),
      R => '0'
    );
\FIFO_DATO_WR_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clk_Write,
      CE => \FIFO_DATO_WR[31]_i_1_n_0\,
      D => count_fifo_reg(9),
      Q => FIFO_DATO_WR(9),
      R => '0'
    );
FIFO_WR_EN_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FIFO_FULL,
      O => FIFO_WR_EN_i_1_n_0
    );
FIFO_WR_EN_reg: unisim.vcomponents.FDCE
     port map (
      C => clk_Write,
      CE => '1',
      CLR => RST,
      D => FIFO_WR_EN_i_1_n_0,
      Q => FIFO_WR_EN
    );
\count_fifo[0]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count_fifo_reg(0),
      O => \count_fifo[0]_i_2_n_0\
    );
\count_fifo_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => clk_Write,
      CE => FIFO_WR_EN_i_1_n_0,
      CLR => RST,
      D => \count_fifo_reg[0]_i_1_n_7\,
      Q => count_fifo_reg(0)
    );
\count_fifo_reg[0]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \count_fifo_reg[0]_i_1_n_0\,
      CO(2) => \count_fifo_reg[0]_i_1_n_1\,
      CO(1) => \count_fifo_reg[0]_i_1_n_2\,
      CO(0) => \count_fifo_reg[0]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \count_fifo_reg[0]_i_1_n_4\,
      O(2) => \count_fifo_reg[0]_i_1_n_5\,
      O(1) => \count_fifo_reg[0]_i_1_n_6\,
      O(0) => \count_fifo_reg[0]_i_1_n_7\,
      S(3 downto 1) => count_fifo_reg(3 downto 1),
      S(0) => \count_fifo[0]_i_2_n_0\
    );
\count_fifo_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => clk_Write,
      CE => FIFO_WR_EN_i_1_n_0,
      CLR => RST,
      D => \count_fifo_reg[8]_i_1_n_5\,
      Q => count_fifo_reg(10)
    );
\count_fifo_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => clk_Write,
      CE => FIFO_WR_EN_i_1_n_0,
      CLR => RST,
      D => \count_fifo_reg[8]_i_1_n_4\,
      Q => count_fifo_reg(11)
    );
\count_fifo_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => clk_Write,
      CE => FIFO_WR_EN_i_1_n_0,
      CLR => RST,
      D => \count_fifo_reg[12]_i_1_n_7\,
      Q => count_fifo_reg(12)
    );
\count_fifo_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_fifo_reg[8]_i_1_n_0\,
      CO(3) => \count_fifo_reg[12]_i_1_n_0\,
      CO(2) => \count_fifo_reg[12]_i_1_n_1\,
      CO(1) => \count_fifo_reg[12]_i_1_n_2\,
      CO(0) => \count_fifo_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \count_fifo_reg[12]_i_1_n_4\,
      O(2) => \count_fifo_reg[12]_i_1_n_5\,
      O(1) => \count_fifo_reg[12]_i_1_n_6\,
      O(0) => \count_fifo_reg[12]_i_1_n_7\,
      S(3 downto 0) => count_fifo_reg(15 downto 12)
    );
\count_fifo_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => clk_Write,
      CE => FIFO_WR_EN_i_1_n_0,
      CLR => RST,
      D => \count_fifo_reg[12]_i_1_n_6\,
      Q => count_fifo_reg(13)
    );
\count_fifo_reg[14]\: unisim.vcomponents.FDCE
     port map (
      C => clk_Write,
      CE => FIFO_WR_EN_i_1_n_0,
      CLR => RST,
      D => \count_fifo_reg[12]_i_1_n_5\,
      Q => count_fifo_reg(14)
    );
\count_fifo_reg[15]\: unisim.vcomponents.FDCE
     port map (
      C => clk_Write,
      CE => FIFO_WR_EN_i_1_n_0,
      CLR => RST,
      D => \count_fifo_reg[12]_i_1_n_4\,
      Q => count_fifo_reg(15)
    );
\count_fifo_reg[16]\: unisim.vcomponents.FDCE
     port map (
      C => clk_Write,
      CE => FIFO_WR_EN_i_1_n_0,
      CLR => RST,
      D => \count_fifo_reg[16]_i_1_n_7\,
      Q => count_fifo_reg(16)
    );
\count_fifo_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_fifo_reg[12]_i_1_n_0\,
      CO(3) => \count_fifo_reg[16]_i_1_n_0\,
      CO(2) => \count_fifo_reg[16]_i_1_n_1\,
      CO(1) => \count_fifo_reg[16]_i_1_n_2\,
      CO(0) => \count_fifo_reg[16]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \count_fifo_reg[16]_i_1_n_4\,
      O(2) => \count_fifo_reg[16]_i_1_n_5\,
      O(1) => \count_fifo_reg[16]_i_1_n_6\,
      O(0) => \count_fifo_reg[16]_i_1_n_7\,
      S(3 downto 0) => count_fifo_reg(19 downto 16)
    );
\count_fifo_reg[17]\: unisim.vcomponents.FDCE
     port map (
      C => clk_Write,
      CE => FIFO_WR_EN_i_1_n_0,
      CLR => RST,
      D => \count_fifo_reg[16]_i_1_n_6\,
      Q => count_fifo_reg(17)
    );
\count_fifo_reg[18]\: unisim.vcomponents.FDCE
     port map (
      C => clk_Write,
      CE => FIFO_WR_EN_i_1_n_0,
      CLR => RST,
      D => \count_fifo_reg[16]_i_1_n_5\,
      Q => count_fifo_reg(18)
    );
\count_fifo_reg[19]\: unisim.vcomponents.FDCE
     port map (
      C => clk_Write,
      CE => FIFO_WR_EN_i_1_n_0,
      CLR => RST,
      D => \count_fifo_reg[16]_i_1_n_4\,
      Q => count_fifo_reg(19)
    );
\count_fifo_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => clk_Write,
      CE => FIFO_WR_EN_i_1_n_0,
      CLR => RST,
      D => \count_fifo_reg[0]_i_1_n_6\,
      Q => count_fifo_reg(1)
    );
\count_fifo_reg[20]\: unisim.vcomponents.FDCE
     port map (
      C => clk_Write,
      CE => FIFO_WR_EN_i_1_n_0,
      CLR => RST,
      D => \count_fifo_reg[20]_i_1_n_7\,
      Q => count_fifo_reg(20)
    );
\count_fifo_reg[20]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_fifo_reg[16]_i_1_n_0\,
      CO(3) => \count_fifo_reg[20]_i_1_n_0\,
      CO(2) => \count_fifo_reg[20]_i_1_n_1\,
      CO(1) => \count_fifo_reg[20]_i_1_n_2\,
      CO(0) => \count_fifo_reg[20]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \count_fifo_reg[20]_i_1_n_4\,
      O(2) => \count_fifo_reg[20]_i_1_n_5\,
      O(1) => \count_fifo_reg[20]_i_1_n_6\,
      O(0) => \count_fifo_reg[20]_i_1_n_7\,
      S(3 downto 0) => count_fifo_reg(23 downto 20)
    );
\count_fifo_reg[21]\: unisim.vcomponents.FDCE
     port map (
      C => clk_Write,
      CE => FIFO_WR_EN_i_1_n_0,
      CLR => RST,
      D => \count_fifo_reg[20]_i_1_n_6\,
      Q => count_fifo_reg(21)
    );
\count_fifo_reg[22]\: unisim.vcomponents.FDCE
     port map (
      C => clk_Write,
      CE => FIFO_WR_EN_i_1_n_0,
      CLR => RST,
      D => \count_fifo_reg[20]_i_1_n_5\,
      Q => count_fifo_reg(22)
    );
\count_fifo_reg[23]\: unisim.vcomponents.FDCE
     port map (
      C => clk_Write,
      CE => FIFO_WR_EN_i_1_n_0,
      CLR => RST,
      D => \count_fifo_reg[20]_i_1_n_4\,
      Q => count_fifo_reg(23)
    );
\count_fifo_reg[24]\: unisim.vcomponents.FDCE
     port map (
      C => clk_Write,
      CE => FIFO_WR_EN_i_1_n_0,
      CLR => RST,
      D => \count_fifo_reg[24]_i_1_n_7\,
      Q => count_fifo_reg(24)
    );
\count_fifo_reg[24]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_fifo_reg[20]_i_1_n_0\,
      CO(3) => \count_fifo_reg[24]_i_1_n_0\,
      CO(2) => \count_fifo_reg[24]_i_1_n_1\,
      CO(1) => \count_fifo_reg[24]_i_1_n_2\,
      CO(0) => \count_fifo_reg[24]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \count_fifo_reg[24]_i_1_n_4\,
      O(2) => \count_fifo_reg[24]_i_1_n_5\,
      O(1) => \count_fifo_reg[24]_i_1_n_6\,
      O(0) => \count_fifo_reg[24]_i_1_n_7\,
      S(3 downto 0) => count_fifo_reg(27 downto 24)
    );
\count_fifo_reg[25]\: unisim.vcomponents.FDCE
     port map (
      C => clk_Write,
      CE => FIFO_WR_EN_i_1_n_0,
      CLR => RST,
      D => \count_fifo_reg[24]_i_1_n_6\,
      Q => count_fifo_reg(25)
    );
\count_fifo_reg[26]\: unisim.vcomponents.FDCE
     port map (
      C => clk_Write,
      CE => FIFO_WR_EN_i_1_n_0,
      CLR => RST,
      D => \count_fifo_reg[24]_i_1_n_5\,
      Q => count_fifo_reg(26)
    );
\count_fifo_reg[27]\: unisim.vcomponents.FDCE
     port map (
      C => clk_Write,
      CE => FIFO_WR_EN_i_1_n_0,
      CLR => RST,
      D => \count_fifo_reg[24]_i_1_n_4\,
      Q => count_fifo_reg(27)
    );
\count_fifo_reg[28]\: unisim.vcomponents.FDCE
     port map (
      C => clk_Write,
      CE => FIFO_WR_EN_i_1_n_0,
      CLR => RST,
      D => \count_fifo_reg[28]_i_1_n_7\,
      Q => count_fifo_reg(28)
    );
\count_fifo_reg[28]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_fifo_reg[24]_i_1_n_0\,
      CO(3) => \NLW_count_fifo_reg[28]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \count_fifo_reg[28]_i_1_n_1\,
      CO(1) => \count_fifo_reg[28]_i_1_n_2\,
      CO(0) => \count_fifo_reg[28]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \count_fifo_reg[28]_i_1_n_4\,
      O(2) => \count_fifo_reg[28]_i_1_n_5\,
      O(1) => \count_fifo_reg[28]_i_1_n_6\,
      O(0) => \count_fifo_reg[28]_i_1_n_7\,
      S(3 downto 0) => count_fifo_reg(31 downto 28)
    );
\count_fifo_reg[29]\: unisim.vcomponents.FDCE
     port map (
      C => clk_Write,
      CE => FIFO_WR_EN_i_1_n_0,
      CLR => RST,
      D => \count_fifo_reg[28]_i_1_n_6\,
      Q => count_fifo_reg(29)
    );
\count_fifo_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => clk_Write,
      CE => FIFO_WR_EN_i_1_n_0,
      CLR => RST,
      D => \count_fifo_reg[0]_i_1_n_5\,
      Q => count_fifo_reg(2)
    );
\count_fifo_reg[30]\: unisim.vcomponents.FDCE
     port map (
      C => clk_Write,
      CE => FIFO_WR_EN_i_1_n_0,
      CLR => RST,
      D => \count_fifo_reg[28]_i_1_n_5\,
      Q => count_fifo_reg(30)
    );
\count_fifo_reg[31]\: unisim.vcomponents.FDCE
     port map (
      C => clk_Write,
      CE => FIFO_WR_EN_i_1_n_0,
      CLR => RST,
      D => \count_fifo_reg[28]_i_1_n_4\,
      Q => count_fifo_reg(31)
    );
\count_fifo_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => clk_Write,
      CE => FIFO_WR_EN_i_1_n_0,
      CLR => RST,
      D => \count_fifo_reg[0]_i_1_n_4\,
      Q => count_fifo_reg(3)
    );
\count_fifo_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => clk_Write,
      CE => FIFO_WR_EN_i_1_n_0,
      CLR => RST,
      D => \count_fifo_reg[4]_i_1_n_7\,
      Q => count_fifo_reg(4)
    );
\count_fifo_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_fifo_reg[0]_i_1_n_0\,
      CO(3) => \count_fifo_reg[4]_i_1_n_0\,
      CO(2) => \count_fifo_reg[4]_i_1_n_1\,
      CO(1) => \count_fifo_reg[4]_i_1_n_2\,
      CO(0) => \count_fifo_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \count_fifo_reg[4]_i_1_n_4\,
      O(2) => \count_fifo_reg[4]_i_1_n_5\,
      O(1) => \count_fifo_reg[4]_i_1_n_6\,
      O(0) => \count_fifo_reg[4]_i_1_n_7\,
      S(3 downto 0) => count_fifo_reg(7 downto 4)
    );
\count_fifo_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => clk_Write,
      CE => FIFO_WR_EN_i_1_n_0,
      CLR => RST,
      D => \count_fifo_reg[4]_i_1_n_6\,
      Q => count_fifo_reg(5)
    );
\count_fifo_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => clk_Write,
      CE => FIFO_WR_EN_i_1_n_0,
      CLR => RST,
      D => \count_fifo_reg[4]_i_1_n_5\,
      Q => count_fifo_reg(6)
    );
\count_fifo_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => clk_Write,
      CE => FIFO_WR_EN_i_1_n_0,
      CLR => RST,
      D => \count_fifo_reg[4]_i_1_n_4\,
      Q => count_fifo_reg(7)
    );
\count_fifo_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => clk_Write,
      CE => FIFO_WR_EN_i_1_n_0,
      CLR => RST,
      D => \count_fifo_reg[8]_i_1_n_7\,
      Q => count_fifo_reg(8)
    );
\count_fifo_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_fifo_reg[4]_i_1_n_0\,
      CO(3) => \count_fifo_reg[8]_i_1_n_0\,
      CO(2) => \count_fifo_reg[8]_i_1_n_1\,
      CO(1) => \count_fifo_reg[8]_i_1_n_2\,
      CO(0) => \count_fifo_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \count_fifo_reg[8]_i_1_n_4\,
      O(2) => \count_fifo_reg[8]_i_1_n_5\,
      O(1) => \count_fifo_reg[8]_i_1_n_6\,
      O(0) => \count_fifo_reg[8]_i_1_n_7\,
      S(3 downto 0) => count_fifo_reg(11 downto 8)
    );
\count_fifo_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => clk_Write,
      CE => FIFO_WR_EN_i_1_n_0,
      CLR => RST,
      D => \count_fifo_reg[8]_i_1_n_6\,
      Q => count_fifo_reg(9)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_write_fifo_0_1 is
  port (
    clk_Write : in STD_LOGIC;
    RST : in STD_LOGIC;
    FIFO_FULL : in STD_LOGIC;
    FIFO_WR_EN : out STD_LOGIC;
    FIFO_DATO_WR : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_1_write_fifo_0_1 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_1_write_fifo_0_1 : entity is "design_1_write_fifo_0_1,write_fifo,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_1_write_fifo_0_1 : entity is "yes";
  attribute ip_definition_source : string;
  attribute ip_definition_source of design_1_write_fifo_0_1 : entity is "module_ref";
  attribute x_core_info : string;
  attribute x_core_info of design_1_write_fifo_0_1 : entity is "write_fifo,Vivado 2019.1";
end design_1_write_fifo_0_1;

architecture STRUCTURE of design_1_write_fifo_0_1 is
  attribute x_interface_info : string;
  attribute x_interface_info of RST : signal is "xilinx.com:signal:reset:1.0 RST RST";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of RST : signal is "XIL_INTERFACENAME RST, POLARITY ACTIVE_HIGH, INSERT_VIP 0";
begin
U0: entity work.design_1_write_fifo_0_1_write_fifo
     port map (
      FIFO_DATO_WR(31 downto 0) => FIFO_DATO_WR(31 downto 0),
      FIFO_FULL => FIFO_FULL,
      FIFO_WR_EN => FIFO_WR_EN,
      RST => RST,
      clk_Write => clk_Write
    );
end STRUCTURE;
