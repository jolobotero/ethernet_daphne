// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Tue Jul  6 23:34:40 2021
// Host        : DESKTOP-QTM1A6I running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               e:/Proyectos_vhdl_vivado/ethernet_2/ethernet_2.srcs/sources_1/bd/design_1/ip/design_1_write_fifo_0_1/design_1_write_fifo_0_1_sim_netlist.v
// Design      : design_1_write_fifo_0_1
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a200tfbg676-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_write_fifo_0_1,write_fifo,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* ip_definition_source = "module_ref" *) 
(* x_core_info = "write_fifo,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module design_1_write_fifo_0_1
   (clk_Write,
    RST,
    FIFO_FULL,
    FIFO_WR_EN,
    FIFO_DATO_WR);
  input clk_Write;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 RST RST" *) (* x_interface_parameter = "XIL_INTERFACENAME RST, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input RST;
  input FIFO_FULL;
  output FIFO_WR_EN;
  output [31:0]FIFO_DATO_WR;

  wire [31:0]FIFO_DATO_WR;
  wire FIFO_FULL;
  wire FIFO_WR_EN;
  wire RST;
  wire clk_Write;

  design_1_write_fifo_0_1_write_fifo U0
       (.FIFO_DATO_WR(FIFO_DATO_WR),
        .FIFO_FULL(FIFO_FULL),
        .FIFO_WR_EN(FIFO_WR_EN),
        .RST(RST),
        .clk_Write(clk_Write));
endmodule

(* ORIG_REF_NAME = "write_fifo" *) 
module design_1_write_fifo_0_1_write_fifo
   (FIFO_WR_EN,
    FIFO_DATO_WR,
    RST,
    FIFO_FULL,
    clk_Write);
  output FIFO_WR_EN;
  output [31:0]FIFO_DATO_WR;
  input RST;
  input FIFO_FULL;
  input clk_Write;

  wire [31:0]FIFO_DATO_WR;
  wire \FIFO_DATO_WR[31]_i_1_n_0 ;
  wire FIFO_FULL;
  wire FIFO_WR_EN;
  wire FIFO_WR_EN_i_1_n_0;
  wire RST;
  wire clk_Write;
  wire \count_fifo[0]_i_2_n_0 ;
  wire [31:0]count_fifo_reg;
  wire \count_fifo_reg[0]_i_1_n_0 ;
  wire \count_fifo_reg[0]_i_1_n_1 ;
  wire \count_fifo_reg[0]_i_1_n_2 ;
  wire \count_fifo_reg[0]_i_1_n_3 ;
  wire \count_fifo_reg[0]_i_1_n_4 ;
  wire \count_fifo_reg[0]_i_1_n_5 ;
  wire \count_fifo_reg[0]_i_1_n_6 ;
  wire \count_fifo_reg[0]_i_1_n_7 ;
  wire \count_fifo_reg[12]_i_1_n_0 ;
  wire \count_fifo_reg[12]_i_1_n_1 ;
  wire \count_fifo_reg[12]_i_1_n_2 ;
  wire \count_fifo_reg[12]_i_1_n_3 ;
  wire \count_fifo_reg[12]_i_1_n_4 ;
  wire \count_fifo_reg[12]_i_1_n_5 ;
  wire \count_fifo_reg[12]_i_1_n_6 ;
  wire \count_fifo_reg[12]_i_1_n_7 ;
  wire \count_fifo_reg[16]_i_1_n_0 ;
  wire \count_fifo_reg[16]_i_1_n_1 ;
  wire \count_fifo_reg[16]_i_1_n_2 ;
  wire \count_fifo_reg[16]_i_1_n_3 ;
  wire \count_fifo_reg[16]_i_1_n_4 ;
  wire \count_fifo_reg[16]_i_1_n_5 ;
  wire \count_fifo_reg[16]_i_1_n_6 ;
  wire \count_fifo_reg[16]_i_1_n_7 ;
  wire \count_fifo_reg[20]_i_1_n_0 ;
  wire \count_fifo_reg[20]_i_1_n_1 ;
  wire \count_fifo_reg[20]_i_1_n_2 ;
  wire \count_fifo_reg[20]_i_1_n_3 ;
  wire \count_fifo_reg[20]_i_1_n_4 ;
  wire \count_fifo_reg[20]_i_1_n_5 ;
  wire \count_fifo_reg[20]_i_1_n_6 ;
  wire \count_fifo_reg[20]_i_1_n_7 ;
  wire \count_fifo_reg[24]_i_1_n_0 ;
  wire \count_fifo_reg[24]_i_1_n_1 ;
  wire \count_fifo_reg[24]_i_1_n_2 ;
  wire \count_fifo_reg[24]_i_1_n_3 ;
  wire \count_fifo_reg[24]_i_1_n_4 ;
  wire \count_fifo_reg[24]_i_1_n_5 ;
  wire \count_fifo_reg[24]_i_1_n_6 ;
  wire \count_fifo_reg[24]_i_1_n_7 ;
  wire \count_fifo_reg[28]_i_1_n_1 ;
  wire \count_fifo_reg[28]_i_1_n_2 ;
  wire \count_fifo_reg[28]_i_1_n_3 ;
  wire \count_fifo_reg[28]_i_1_n_4 ;
  wire \count_fifo_reg[28]_i_1_n_5 ;
  wire \count_fifo_reg[28]_i_1_n_6 ;
  wire \count_fifo_reg[28]_i_1_n_7 ;
  wire \count_fifo_reg[4]_i_1_n_0 ;
  wire \count_fifo_reg[4]_i_1_n_1 ;
  wire \count_fifo_reg[4]_i_1_n_2 ;
  wire \count_fifo_reg[4]_i_1_n_3 ;
  wire \count_fifo_reg[4]_i_1_n_4 ;
  wire \count_fifo_reg[4]_i_1_n_5 ;
  wire \count_fifo_reg[4]_i_1_n_6 ;
  wire \count_fifo_reg[4]_i_1_n_7 ;
  wire \count_fifo_reg[8]_i_1_n_0 ;
  wire \count_fifo_reg[8]_i_1_n_1 ;
  wire \count_fifo_reg[8]_i_1_n_2 ;
  wire \count_fifo_reg[8]_i_1_n_3 ;
  wire \count_fifo_reg[8]_i_1_n_4 ;
  wire \count_fifo_reg[8]_i_1_n_5 ;
  wire \count_fifo_reg[8]_i_1_n_6 ;
  wire \count_fifo_reg[8]_i_1_n_7 ;
  wire [3:3]\NLW_count_fifo_reg[28]_i_1_CO_UNCONNECTED ;

  LUT2 #(
    .INIT(4'h1)) 
    \FIFO_DATO_WR[31]_i_1 
       (.I0(RST),
        .I1(FIFO_FULL),
        .O(\FIFO_DATO_WR[31]_i_1_n_0 ));
  FDRE \FIFO_DATO_WR_reg[0] 
       (.C(clk_Write),
        .CE(\FIFO_DATO_WR[31]_i_1_n_0 ),
        .D(count_fifo_reg[0]),
        .Q(FIFO_DATO_WR[0]),
        .R(1'b0));
  FDRE \FIFO_DATO_WR_reg[10] 
       (.C(clk_Write),
        .CE(\FIFO_DATO_WR[31]_i_1_n_0 ),
        .D(count_fifo_reg[10]),
        .Q(FIFO_DATO_WR[10]),
        .R(1'b0));
  FDRE \FIFO_DATO_WR_reg[11] 
       (.C(clk_Write),
        .CE(\FIFO_DATO_WR[31]_i_1_n_0 ),
        .D(count_fifo_reg[11]),
        .Q(FIFO_DATO_WR[11]),
        .R(1'b0));
  FDRE \FIFO_DATO_WR_reg[12] 
       (.C(clk_Write),
        .CE(\FIFO_DATO_WR[31]_i_1_n_0 ),
        .D(count_fifo_reg[12]),
        .Q(FIFO_DATO_WR[12]),
        .R(1'b0));
  FDRE \FIFO_DATO_WR_reg[13] 
       (.C(clk_Write),
        .CE(\FIFO_DATO_WR[31]_i_1_n_0 ),
        .D(count_fifo_reg[13]),
        .Q(FIFO_DATO_WR[13]),
        .R(1'b0));
  FDRE \FIFO_DATO_WR_reg[14] 
       (.C(clk_Write),
        .CE(\FIFO_DATO_WR[31]_i_1_n_0 ),
        .D(count_fifo_reg[14]),
        .Q(FIFO_DATO_WR[14]),
        .R(1'b0));
  FDRE \FIFO_DATO_WR_reg[15] 
       (.C(clk_Write),
        .CE(\FIFO_DATO_WR[31]_i_1_n_0 ),
        .D(count_fifo_reg[15]),
        .Q(FIFO_DATO_WR[15]),
        .R(1'b0));
  FDRE \FIFO_DATO_WR_reg[16] 
       (.C(clk_Write),
        .CE(\FIFO_DATO_WR[31]_i_1_n_0 ),
        .D(count_fifo_reg[16]),
        .Q(FIFO_DATO_WR[16]),
        .R(1'b0));
  FDRE \FIFO_DATO_WR_reg[17] 
       (.C(clk_Write),
        .CE(\FIFO_DATO_WR[31]_i_1_n_0 ),
        .D(count_fifo_reg[17]),
        .Q(FIFO_DATO_WR[17]),
        .R(1'b0));
  FDRE \FIFO_DATO_WR_reg[18] 
       (.C(clk_Write),
        .CE(\FIFO_DATO_WR[31]_i_1_n_0 ),
        .D(count_fifo_reg[18]),
        .Q(FIFO_DATO_WR[18]),
        .R(1'b0));
  FDRE \FIFO_DATO_WR_reg[19] 
       (.C(clk_Write),
        .CE(\FIFO_DATO_WR[31]_i_1_n_0 ),
        .D(count_fifo_reg[19]),
        .Q(FIFO_DATO_WR[19]),
        .R(1'b0));
  FDRE \FIFO_DATO_WR_reg[1] 
       (.C(clk_Write),
        .CE(\FIFO_DATO_WR[31]_i_1_n_0 ),
        .D(count_fifo_reg[1]),
        .Q(FIFO_DATO_WR[1]),
        .R(1'b0));
  FDRE \FIFO_DATO_WR_reg[20] 
       (.C(clk_Write),
        .CE(\FIFO_DATO_WR[31]_i_1_n_0 ),
        .D(count_fifo_reg[20]),
        .Q(FIFO_DATO_WR[20]),
        .R(1'b0));
  FDRE \FIFO_DATO_WR_reg[21] 
       (.C(clk_Write),
        .CE(\FIFO_DATO_WR[31]_i_1_n_0 ),
        .D(count_fifo_reg[21]),
        .Q(FIFO_DATO_WR[21]),
        .R(1'b0));
  FDRE \FIFO_DATO_WR_reg[22] 
       (.C(clk_Write),
        .CE(\FIFO_DATO_WR[31]_i_1_n_0 ),
        .D(count_fifo_reg[22]),
        .Q(FIFO_DATO_WR[22]),
        .R(1'b0));
  FDRE \FIFO_DATO_WR_reg[23] 
       (.C(clk_Write),
        .CE(\FIFO_DATO_WR[31]_i_1_n_0 ),
        .D(count_fifo_reg[23]),
        .Q(FIFO_DATO_WR[23]),
        .R(1'b0));
  FDRE \FIFO_DATO_WR_reg[24] 
       (.C(clk_Write),
        .CE(\FIFO_DATO_WR[31]_i_1_n_0 ),
        .D(count_fifo_reg[24]),
        .Q(FIFO_DATO_WR[24]),
        .R(1'b0));
  FDRE \FIFO_DATO_WR_reg[25] 
       (.C(clk_Write),
        .CE(\FIFO_DATO_WR[31]_i_1_n_0 ),
        .D(count_fifo_reg[25]),
        .Q(FIFO_DATO_WR[25]),
        .R(1'b0));
  FDRE \FIFO_DATO_WR_reg[26] 
       (.C(clk_Write),
        .CE(\FIFO_DATO_WR[31]_i_1_n_0 ),
        .D(count_fifo_reg[26]),
        .Q(FIFO_DATO_WR[26]),
        .R(1'b0));
  FDRE \FIFO_DATO_WR_reg[27] 
       (.C(clk_Write),
        .CE(\FIFO_DATO_WR[31]_i_1_n_0 ),
        .D(count_fifo_reg[27]),
        .Q(FIFO_DATO_WR[27]),
        .R(1'b0));
  FDRE \FIFO_DATO_WR_reg[28] 
       (.C(clk_Write),
        .CE(\FIFO_DATO_WR[31]_i_1_n_0 ),
        .D(count_fifo_reg[28]),
        .Q(FIFO_DATO_WR[28]),
        .R(1'b0));
  FDRE \FIFO_DATO_WR_reg[29] 
       (.C(clk_Write),
        .CE(\FIFO_DATO_WR[31]_i_1_n_0 ),
        .D(count_fifo_reg[29]),
        .Q(FIFO_DATO_WR[29]),
        .R(1'b0));
  FDRE \FIFO_DATO_WR_reg[2] 
       (.C(clk_Write),
        .CE(\FIFO_DATO_WR[31]_i_1_n_0 ),
        .D(count_fifo_reg[2]),
        .Q(FIFO_DATO_WR[2]),
        .R(1'b0));
  FDRE \FIFO_DATO_WR_reg[30] 
       (.C(clk_Write),
        .CE(\FIFO_DATO_WR[31]_i_1_n_0 ),
        .D(count_fifo_reg[30]),
        .Q(FIFO_DATO_WR[30]),
        .R(1'b0));
  FDRE \FIFO_DATO_WR_reg[31] 
       (.C(clk_Write),
        .CE(\FIFO_DATO_WR[31]_i_1_n_0 ),
        .D(count_fifo_reg[31]),
        .Q(FIFO_DATO_WR[31]),
        .R(1'b0));
  FDRE \FIFO_DATO_WR_reg[3] 
       (.C(clk_Write),
        .CE(\FIFO_DATO_WR[31]_i_1_n_0 ),
        .D(count_fifo_reg[3]),
        .Q(FIFO_DATO_WR[3]),
        .R(1'b0));
  FDRE \FIFO_DATO_WR_reg[4] 
       (.C(clk_Write),
        .CE(\FIFO_DATO_WR[31]_i_1_n_0 ),
        .D(count_fifo_reg[4]),
        .Q(FIFO_DATO_WR[4]),
        .R(1'b0));
  FDRE \FIFO_DATO_WR_reg[5] 
       (.C(clk_Write),
        .CE(\FIFO_DATO_WR[31]_i_1_n_0 ),
        .D(count_fifo_reg[5]),
        .Q(FIFO_DATO_WR[5]),
        .R(1'b0));
  FDRE \FIFO_DATO_WR_reg[6] 
       (.C(clk_Write),
        .CE(\FIFO_DATO_WR[31]_i_1_n_0 ),
        .D(count_fifo_reg[6]),
        .Q(FIFO_DATO_WR[6]),
        .R(1'b0));
  FDRE \FIFO_DATO_WR_reg[7] 
       (.C(clk_Write),
        .CE(\FIFO_DATO_WR[31]_i_1_n_0 ),
        .D(count_fifo_reg[7]),
        .Q(FIFO_DATO_WR[7]),
        .R(1'b0));
  FDRE \FIFO_DATO_WR_reg[8] 
       (.C(clk_Write),
        .CE(\FIFO_DATO_WR[31]_i_1_n_0 ),
        .D(count_fifo_reg[8]),
        .Q(FIFO_DATO_WR[8]),
        .R(1'b0));
  FDRE \FIFO_DATO_WR_reg[9] 
       (.C(clk_Write),
        .CE(\FIFO_DATO_WR[31]_i_1_n_0 ),
        .D(count_fifo_reg[9]),
        .Q(FIFO_DATO_WR[9]),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    FIFO_WR_EN_i_1
       (.I0(FIFO_FULL),
        .O(FIFO_WR_EN_i_1_n_0));
  FDCE FIFO_WR_EN_reg
       (.C(clk_Write),
        .CE(1'b1),
        .CLR(RST),
        .D(FIFO_WR_EN_i_1_n_0),
        .Q(FIFO_WR_EN));
  LUT1 #(
    .INIT(2'h1)) 
    \count_fifo[0]_i_2 
       (.I0(count_fifo_reg[0]),
        .O(\count_fifo[0]_i_2_n_0 ));
  FDCE \count_fifo_reg[0] 
       (.C(clk_Write),
        .CE(FIFO_WR_EN_i_1_n_0),
        .CLR(RST),
        .D(\count_fifo_reg[0]_i_1_n_7 ),
        .Q(count_fifo_reg[0]));
  CARRY4 \count_fifo_reg[0]_i_1 
       (.CI(1'b0),
        .CO({\count_fifo_reg[0]_i_1_n_0 ,\count_fifo_reg[0]_i_1_n_1 ,\count_fifo_reg[0]_i_1_n_2 ,\count_fifo_reg[0]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\count_fifo_reg[0]_i_1_n_4 ,\count_fifo_reg[0]_i_1_n_5 ,\count_fifo_reg[0]_i_1_n_6 ,\count_fifo_reg[0]_i_1_n_7 }),
        .S({count_fifo_reg[3:1],\count_fifo[0]_i_2_n_0 }));
  FDCE \count_fifo_reg[10] 
       (.C(clk_Write),
        .CE(FIFO_WR_EN_i_1_n_0),
        .CLR(RST),
        .D(\count_fifo_reg[8]_i_1_n_5 ),
        .Q(count_fifo_reg[10]));
  FDCE \count_fifo_reg[11] 
       (.C(clk_Write),
        .CE(FIFO_WR_EN_i_1_n_0),
        .CLR(RST),
        .D(\count_fifo_reg[8]_i_1_n_4 ),
        .Q(count_fifo_reg[11]));
  FDCE \count_fifo_reg[12] 
       (.C(clk_Write),
        .CE(FIFO_WR_EN_i_1_n_0),
        .CLR(RST),
        .D(\count_fifo_reg[12]_i_1_n_7 ),
        .Q(count_fifo_reg[12]));
  CARRY4 \count_fifo_reg[12]_i_1 
       (.CI(\count_fifo_reg[8]_i_1_n_0 ),
        .CO({\count_fifo_reg[12]_i_1_n_0 ,\count_fifo_reg[12]_i_1_n_1 ,\count_fifo_reg[12]_i_1_n_2 ,\count_fifo_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_fifo_reg[12]_i_1_n_4 ,\count_fifo_reg[12]_i_1_n_5 ,\count_fifo_reg[12]_i_1_n_6 ,\count_fifo_reg[12]_i_1_n_7 }),
        .S(count_fifo_reg[15:12]));
  FDCE \count_fifo_reg[13] 
       (.C(clk_Write),
        .CE(FIFO_WR_EN_i_1_n_0),
        .CLR(RST),
        .D(\count_fifo_reg[12]_i_1_n_6 ),
        .Q(count_fifo_reg[13]));
  FDCE \count_fifo_reg[14] 
       (.C(clk_Write),
        .CE(FIFO_WR_EN_i_1_n_0),
        .CLR(RST),
        .D(\count_fifo_reg[12]_i_1_n_5 ),
        .Q(count_fifo_reg[14]));
  FDCE \count_fifo_reg[15] 
       (.C(clk_Write),
        .CE(FIFO_WR_EN_i_1_n_0),
        .CLR(RST),
        .D(\count_fifo_reg[12]_i_1_n_4 ),
        .Q(count_fifo_reg[15]));
  FDCE \count_fifo_reg[16] 
       (.C(clk_Write),
        .CE(FIFO_WR_EN_i_1_n_0),
        .CLR(RST),
        .D(\count_fifo_reg[16]_i_1_n_7 ),
        .Q(count_fifo_reg[16]));
  CARRY4 \count_fifo_reg[16]_i_1 
       (.CI(\count_fifo_reg[12]_i_1_n_0 ),
        .CO({\count_fifo_reg[16]_i_1_n_0 ,\count_fifo_reg[16]_i_1_n_1 ,\count_fifo_reg[16]_i_1_n_2 ,\count_fifo_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_fifo_reg[16]_i_1_n_4 ,\count_fifo_reg[16]_i_1_n_5 ,\count_fifo_reg[16]_i_1_n_6 ,\count_fifo_reg[16]_i_1_n_7 }),
        .S(count_fifo_reg[19:16]));
  FDCE \count_fifo_reg[17] 
       (.C(clk_Write),
        .CE(FIFO_WR_EN_i_1_n_0),
        .CLR(RST),
        .D(\count_fifo_reg[16]_i_1_n_6 ),
        .Q(count_fifo_reg[17]));
  FDCE \count_fifo_reg[18] 
       (.C(clk_Write),
        .CE(FIFO_WR_EN_i_1_n_0),
        .CLR(RST),
        .D(\count_fifo_reg[16]_i_1_n_5 ),
        .Q(count_fifo_reg[18]));
  FDCE \count_fifo_reg[19] 
       (.C(clk_Write),
        .CE(FIFO_WR_EN_i_1_n_0),
        .CLR(RST),
        .D(\count_fifo_reg[16]_i_1_n_4 ),
        .Q(count_fifo_reg[19]));
  FDCE \count_fifo_reg[1] 
       (.C(clk_Write),
        .CE(FIFO_WR_EN_i_1_n_0),
        .CLR(RST),
        .D(\count_fifo_reg[0]_i_1_n_6 ),
        .Q(count_fifo_reg[1]));
  FDCE \count_fifo_reg[20] 
       (.C(clk_Write),
        .CE(FIFO_WR_EN_i_1_n_0),
        .CLR(RST),
        .D(\count_fifo_reg[20]_i_1_n_7 ),
        .Q(count_fifo_reg[20]));
  CARRY4 \count_fifo_reg[20]_i_1 
       (.CI(\count_fifo_reg[16]_i_1_n_0 ),
        .CO({\count_fifo_reg[20]_i_1_n_0 ,\count_fifo_reg[20]_i_1_n_1 ,\count_fifo_reg[20]_i_1_n_2 ,\count_fifo_reg[20]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_fifo_reg[20]_i_1_n_4 ,\count_fifo_reg[20]_i_1_n_5 ,\count_fifo_reg[20]_i_1_n_6 ,\count_fifo_reg[20]_i_1_n_7 }),
        .S(count_fifo_reg[23:20]));
  FDCE \count_fifo_reg[21] 
       (.C(clk_Write),
        .CE(FIFO_WR_EN_i_1_n_0),
        .CLR(RST),
        .D(\count_fifo_reg[20]_i_1_n_6 ),
        .Q(count_fifo_reg[21]));
  FDCE \count_fifo_reg[22] 
       (.C(clk_Write),
        .CE(FIFO_WR_EN_i_1_n_0),
        .CLR(RST),
        .D(\count_fifo_reg[20]_i_1_n_5 ),
        .Q(count_fifo_reg[22]));
  FDCE \count_fifo_reg[23] 
       (.C(clk_Write),
        .CE(FIFO_WR_EN_i_1_n_0),
        .CLR(RST),
        .D(\count_fifo_reg[20]_i_1_n_4 ),
        .Q(count_fifo_reg[23]));
  FDCE \count_fifo_reg[24] 
       (.C(clk_Write),
        .CE(FIFO_WR_EN_i_1_n_0),
        .CLR(RST),
        .D(\count_fifo_reg[24]_i_1_n_7 ),
        .Q(count_fifo_reg[24]));
  CARRY4 \count_fifo_reg[24]_i_1 
       (.CI(\count_fifo_reg[20]_i_1_n_0 ),
        .CO({\count_fifo_reg[24]_i_1_n_0 ,\count_fifo_reg[24]_i_1_n_1 ,\count_fifo_reg[24]_i_1_n_2 ,\count_fifo_reg[24]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_fifo_reg[24]_i_1_n_4 ,\count_fifo_reg[24]_i_1_n_5 ,\count_fifo_reg[24]_i_1_n_6 ,\count_fifo_reg[24]_i_1_n_7 }),
        .S(count_fifo_reg[27:24]));
  FDCE \count_fifo_reg[25] 
       (.C(clk_Write),
        .CE(FIFO_WR_EN_i_1_n_0),
        .CLR(RST),
        .D(\count_fifo_reg[24]_i_1_n_6 ),
        .Q(count_fifo_reg[25]));
  FDCE \count_fifo_reg[26] 
       (.C(clk_Write),
        .CE(FIFO_WR_EN_i_1_n_0),
        .CLR(RST),
        .D(\count_fifo_reg[24]_i_1_n_5 ),
        .Q(count_fifo_reg[26]));
  FDCE \count_fifo_reg[27] 
       (.C(clk_Write),
        .CE(FIFO_WR_EN_i_1_n_0),
        .CLR(RST),
        .D(\count_fifo_reg[24]_i_1_n_4 ),
        .Q(count_fifo_reg[27]));
  FDCE \count_fifo_reg[28] 
       (.C(clk_Write),
        .CE(FIFO_WR_EN_i_1_n_0),
        .CLR(RST),
        .D(\count_fifo_reg[28]_i_1_n_7 ),
        .Q(count_fifo_reg[28]));
  CARRY4 \count_fifo_reg[28]_i_1 
       (.CI(\count_fifo_reg[24]_i_1_n_0 ),
        .CO({\NLW_count_fifo_reg[28]_i_1_CO_UNCONNECTED [3],\count_fifo_reg[28]_i_1_n_1 ,\count_fifo_reg[28]_i_1_n_2 ,\count_fifo_reg[28]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_fifo_reg[28]_i_1_n_4 ,\count_fifo_reg[28]_i_1_n_5 ,\count_fifo_reg[28]_i_1_n_6 ,\count_fifo_reg[28]_i_1_n_7 }),
        .S(count_fifo_reg[31:28]));
  FDCE \count_fifo_reg[29] 
       (.C(clk_Write),
        .CE(FIFO_WR_EN_i_1_n_0),
        .CLR(RST),
        .D(\count_fifo_reg[28]_i_1_n_6 ),
        .Q(count_fifo_reg[29]));
  FDCE \count_fifo_reg[2] 
       (.C(clk_Write),
        .CE(FIFO_WR_EN_i_1_n_0),
        .CLR(RST),
        .D(\count_fifo_reg[0]_i_1_n_5 ),
        .Q(count_fifo_reg[2]));
  FDCE \count_fifo_reg[30] 
       (.C(clk_Write),
        .CE(FIFO_WR_EN_i_1_n_0),
        .CLR(RST),
        .D(\count_fifo_reg[28]_i_1_n_5 ),
        .Q(count_fifo_reg[30]));
  FDCE \count_fifo_reg[31] 
       (.C(clk_Write),
        .CE(FIFO_WR_EN_i_1_n_0),
        .CLR(RST),
        .D(\count_fifo_reg[28]_i_1_n_4 ),
        .Q(count_fifo_reg[31]));
  FDCE \count_fifo_reg[3] 
       (.C(clk_Write),
        .CE(FIFO_WR_EN_i_1_n_0),
        .CLR(RST),
        .D(\count_fifo_reg[0]_i_1_n_4 ),
        .Q(count_fifo_reg[3]));
  FDCE \count_fifo_reg[4] 
       (.C(clk_Write),
        .CE(FIFO_WR_EN_i_1_n_0),
        .CLR(RST),
        .D(\count_fifo_reg[4]_i_1_n_7 ),
        .Q(count_fifo_reg[4]));
  CARRY4 \count_fifo_reg[4]_i_1 
       (.CI(\count_fifo_reg[0]_i_1_n_0 ),
        .CO({\count_fifo_reg[4]_i_1_n_0 ,\count_fifo_reg[4]_i_1_n_1 ,\count_fifo_reg[4]_i_1_n_2 ,\count_fifo_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_fifo_reg[4]_i_1_n_4 ,\count_fifo_reg[4]_i_1_n_5 ,\count_fifo_reg[4]_i_1_n_6 ,\count_fifo_reg[4]_i_1_n_7 }),
        .S(count_fifo_reg[7:4]));
  FDCE \count_fifo_reg[5] 
       (.C(clk_Write),
        .CE(FIFO_WR_EN_i_1_n_0),
        .CLR(RST),
        .D(\count_fifo_reg[4]_i_1_n_6 ),
        .Q(count_fifo_reg[5]));
  FDCE \count_fifo_reg[6] 
       (.C(clk_Write),
        .CE(FIFO_WR_EN_i_1_n_0),
        .CLR(RST),
        .D(\count_fifo_reg[4]_i_1_n_5 ),
        .Q(count_fifo_reg[6]));
  FDCE \count_fifo_reg[7] 
       (.C(clk_Write),
        .CE(FIFO_WR_EN_i_1_n_0),
        .CLR(RST),
        .D(\count_fifo_reg[4]_i_1_n_4 ),
        .Q(count_fifo_reg[7]));
  FDCE \count_fifo_reg[8] 
       (.C(clk_Write),
        .CE(FIFO_WR_EN_i_1_n_0),
        .CLR(RST),
        .D(\count_fifo_reg[8]_i_1_n_7 ),
        .Q(count_fifo_reg[8]));
  CARRY4 \count_fifo_reg[8]_i_1 
       (.CI(\count_fifo_reg[4]_i_1_n_0 ),
        .CO({\count_fifo_reg[8]_i_1_n_0 ,\count_fifo_reg[8]_i_1_n_1 ,\count_fifo_reg[8]_i_1_n_2 ,\count_fifo_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_fifo_reg[8]_i_1_n_4 ,\count_fifo_reg[8]_i_1_n_5 ,\count_fifo_reg[8]_i_1_n_6 ,\count_fifo_reg[8]_i_1_n_7 }),
        .S(count_fifo_reg[11:8]));
  FDCE \count_fifo_reg[9] 
       (.C(clk_Write),
        .CE(FIFO_WR_EN_i_1_n_0),
        .CLR(RST),
        .D(\count_fifo_reg[8]_i_1_n_6 ),
        .Q(count_fifo_reg[9]));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
