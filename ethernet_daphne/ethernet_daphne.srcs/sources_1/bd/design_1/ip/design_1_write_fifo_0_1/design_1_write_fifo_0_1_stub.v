// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Tue Jul  6 23:34:40 2021
// Host        : DESKTOP-QTM1A6I running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               e:/Proyectos_vhdl_vivado/ethernet_2/ethernet_2.srcs/sources_1/bd/design_1/ip/design_1_write_fifo_0_1/design_1_write_fifo_0_1_stub.v
// Design      : design_1_write_fifo_0_1
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7a200tfbg676-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "write_fifo,Vivado 2019.1" *)
module design_1_write_fifo_0_1(clk_Write, RST, FIFO_FULL, FIFO_WR_EN, 
  FIFO_DATO_WR)
/* synthesis syn_black_box black_box_pad_pin="clk_Write,RST,FIFO_FULL,FIFO_WR_EN,FIFO_DATO_WR[31:0]" */;
  input clk_Write;
  input RST;
  input FIFO_FULL;
  output FIFO_WR_EN;
  output [31:0]FIFO_DATO_WR;
endmodule
