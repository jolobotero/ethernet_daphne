--Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
--Date        : Tue Jul  6 22:47:35 2021
--Host        : DESKTOP-QTM1A6I running 64-bit major release  (build 9200)
--Command     : generate_target design_1.bd
--Design      : design_1
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity m00_couplers_imp_11GI61F is
  port (
    M_ACLK : in STD_LOGIC;
    M_ARESETN : in STD_LOGIC;
    M_AXI_araddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
    M_AXI_arready : in STD_LOGIC_VECTOR ( 0 to 0 );
    M_AXI_arvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    M_AXI_awaddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
    M_AXI_awready : in STD_LOGIC_VECTOR ( 0 to 0 );
    M_AXI_awvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    M_AXI_bready : out STD_LOGIC_VECTOR ( 0 to 0 );
    M_AXI_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    M_AXI_bvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    M_AXI_rdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    M_AXI_rready : out STD_LOGIC_VECTOR ( 0 to 0 );
    M_AXI_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    M_AXI_rvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    M_AXI_wdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    M_AXI_wready : in STD_LOGIC_VECTOR ( 0 to 0 );
    M_AXI_wstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M_AXI_wvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    S_ACLK : in STD_LOGIC;
    S_ARESETN : in STD_LOGIC;
    S_AXI_araddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_arready : out STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_arvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_awaddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_awready : out STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_awvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_bready : in STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_bvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_rready : in STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_rvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_wready : out STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_wvalid : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end m00_couplers_imp_11GI61F;

architecture STRUCTURE of m00_couplers_imp_11GI61F is
  signal m00_couplers_to_m00_couplers_ARADDR : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal m00_couplers_to_m00_couplers_ARREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m00_couplers_to_m00_couplers_ARVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m00_couplers_to_m00_couplers_AWADDR : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal m00_couplers_to_m00_couplers_AWREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m00_couplers_to_m00_couplers_AWVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m00_couplers_to_m00_couplers_BREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m00_couplers_to_m00_couplers_BRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal m00_couplers_to_m00_couplers_BVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m00_couplers_to_m00_couplers_RDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal m00_couplers_to_m00_couplers_RREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m00_couplers_to_m00_couplers_RRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal m00_couplers_to_m00_couplers_RVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m00_couplers_to_m00_couplers_WDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal m00_couplers_to_m00_couplers_WREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m00_couplers_to_m00_couplers_WSTRB : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal m00_couplers_to_m00_couplers_WVALID : STD_LOGIC_VECTOR ( 0 to 0 );
begin
  M_AXI_araddr(31 downto 0) <= m00_couplers_to_m00_couplers_ARADDR(31 downto 0);
  M_AXI_arvalid(0) <= m00_couplers_to_m00_couplers_ARVALID(0);
  M_AXI_awaddr(31 downto 0) <= m00_couplers_to_m00_couplers_AWADDR(31 downto 0);
  M_AXI_awvalid(0) <= m00_couplers_to_m00_couplers_AWVALID(0);
  M_AXI_bready(0) <= m00_couplers_to_m00_couplers_BREADY(0);
  M_AXI_rready(0) <= m00_couplers_to_m00_couplers_RREADY(0);
  M_AXI_wdata(31 downto 0) <= m00_couplers_to_m00_couplers_WDATA(31 downto 0);
  M_AXI_wstrb(3 downto 0) <= m00_couplers_to_m00_couplers_WSTRB(3 downto 0);
  M_AXI_wvalid(0) <= m00_couplers_to_m00_couplers_WVALID(0);
  S_AXI_arready(0) <= m00_couplers_to_m00_couplers_ARREADY(0);
  S_AXI_awready(0) <= m00_couplers_to_m00_couplers_AWREADY(0);
  S_AXI_bresp(1 downto 0) <= m00_couplers_to_m00_couplers_BRESP(1 downto 0);
  S_AXI_bvalid(0) <= m00_couplers_to_m00_couplers_BVALID(0);
  S_AXI_rdata(31 downto 0) <= m00_couplers_to_m00_couplers_RDATA(31 downto 0);
  S_AXI_rresp(1 downto 0) <= m00_couplers_to_m00_couplers_RRESP(1 downto 0);
  S_AXI_rvalid(0) <= m00_couplers_to_m00_couplers_RVALID(0);
  S_AXI_wready(0) <= m00_couplers_to_m00_couplers_WREADY(0);
  m00_couplers_to_m00_couplers_ARADDR(31 downto 0) <= S_AXI_araddr(31 downto 0);
  m00_couplers_to_m00_couplers_ARREADY(0) <= M_AXI_arready(0);
  m00_couplers_to_m00_couplers_ARVALID(0) <= S_AXI_arvalid(0);
  m00_couplers_to_m00_couplers_AWADDR(31 downto 0) <= S_AXI_awaddr(31 downto 0);
  m00_couplers_to_m00_couplers_AWREADY(0) <= M_AXI_awready(0);
  m00_couplers_to_m00_couplers_AWVALID(0) <= S_AXI_awvalid(0);
  m00_couplers_to_m00_couplers_BREADY(0) <= S_AXI_bready(0);
  m00_couplers_to_m00_couplers_BRESP(1 downto 0) <= M_AXI_bresp(1 downto 0);
  m00_couplers_to_m00_couplers_BVALID(0) <= M_AXI_bvalid(0);
  m00_couplers_to_m00_couplers_RDATA(31 downto 0) <= M_AXI_rdata(31 downto 0);
  m00_couplers_to_m00_couplers_RREADY(0) <= S_AXI_rready(0);
  m00_couplers_to_m00_couplers_RRESP(1 downto 0) <= M_AXI_rresp(1 downto 0);
  m00_couplers_to_m00_couplers_RVALID(0) <= M_AXI_rvalid(0);
  m00_couplers_to_m00_couplers_WDATA(31 downto 0) <= S_AXI_wdata(31 downto 0);
  m00_couplers_to_m00_couplers_WREADY(0) <= M_AXI_wready(0);
  m00_couplers_to_m00_couplers_WSTRB(3 downto 0) <= S_AXI_wstrb(3 downto 0);
  m00_couplers_to_m00_couplers_WVALID(0) <= S_AXI_wvalid(0);
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity m01_couplers_imp_SY0ZJM is
  port (
    M_ACLK : in STD_LOGIC;
    M_ARESETN : in STD_LOGIC;
    M_AXI_araddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
    M_AXI_arready : in STD_LOGIC_VECTOR ( 0 to 0 );
    M_AXI_arvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    M_AXI_awaddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
    M_AXI_awready : in STD_LOGIC_VECTOR ( 0 to 0 );
    M_AXI_awvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    M_AXI_bready : out STD_LOGIC_VECTOR ( 0 to 0 );
    M_AXI_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    M_AXI_bvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    M_AXI_rdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    M_AXI_rready : out STD_LOGIC_VECTOR ( 0 to 0 );
    M_AXI_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    M_AXI_rvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    M_AXI_wdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    M_AXI_wready : in STD_LOGIC_VECTOR ( 0 to 0 );
    M_AXI_wstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M_AXI_wvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    S_ACLK : in STD_LOGIC;
    S_ARESETN : in STD_LOGIC;
    S_AXI_araddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_arready : out STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_arvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_awaddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_awready : out STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_awvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_bready : in STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_bvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_rready : in STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_rvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_wready : out STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_wvalid : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end m01_couplers_imp_SY0ZJM;

architecture STRUCTURE of m01_couplers_imp_SY0ZJM is
  signal m01_couplers_to_m01_couplers_ARADDR : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal m01_couplers_to_m01_couplers_ARREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m01_couplers_to_m01_couplers_ARVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m01_couplers_to_m01_couplers_AWADDR : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal m01_couplers_to_m01_couplers_AWREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m01_couplers_to_m01_couplers_AWVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m01_couplers_to_m01_couplers_BREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m01_couplers_to_m01_couplers_BRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal m01_couplers_to_m01_couplers_BVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m01_couplers_to_m01_couplers_RDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal m01_couplers_to_m01_couplers_RREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m01_couplers_to_m01_couplers_RRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal m01_couplers_to_m01_couplers_RVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m01_couplers_to_m01_couplers_WDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal m01_couplers_to_m01_couplers_WREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m01_couplers_to_m01_couplers_WSTRB : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal m01_couplers_to_m01_couplers_WVALID : STD_LOGIC_VECTOR ( 0 to 0 );
begin
  M_AXI_araddr(31 downto 0) <= m01_couplers_to_m01_couplers_ARADDR(31 downto 0);
  M_AXI_arvalid(0) <= m01_couplers_to_m01_couplers_ARVALID(0);
  M_AXI_awaddr(31 downto 0) <= m01_couplers_to_m01_couplers_AWADDR(31 downto 0);
  M_AXI_awvalid(0) <= m01_couplers_to_m01_couplers_AWVALID(0);
  M_AXI_bready(0) <= m01_couplers_to_m01_couplers_BREADY(0);
  M_AXI_rready(0) <= m01_couplers_to_m01_couplers_RREADY(0);
  M_AXI_wdata(31 downto 0) <= m01_couplers_to_m01_couplers_WDATA(31 downto 0);
  M_AXI_wstrb(3 downto 0) <= m01_couplers_to_m01_couplers_WSTRB(3 downto 0);
  M_AXI_wvalid(0) <= m01_couplers_to_m01_couplers_WVALID(0);
  S_AXI_arready(0) <= m01_couplers_to_m01_couplers_ARREADY(0);
  S_AXI_awready(0) <= m01_couplers_to_m01_couplers_AWREADY(0);
  S_AXI_bresp(1 downto 0) <= m01_couplers_to_m01_couplers_BRESP(1 downto 0);
  S_AXI_bvalid(0) <= m01_couplers_to_m01_couplers_BVALID(0);
  S_AXI_rdata(31 downto 0) <= m01_couplers_to_m01_couplers_RDATA(31 downto 0);
  S_AXI_rresp(1 downto 0) <= m01_couplers_to_m01_couplers_RRESP(1 downto 0);
  S_AXI_rvalid(0) <= m01_couplers_to_m01_couplers_RVALID(0);
  S_AXI_wready(0) <= m01_couplers_to_m01_couplers_WREADY(0);
  m01_couplers_to_m01_couplers_ARADDR(31 downto 0) <= S_AXI_araddr(31 downto 0);
  m01_couplers_to_m01_couplers_ARREADY(0) <= M_AXI_arready(0);
  m01_couplers_to_m01_couplers_ARVALID(0) <= S_AXI_arvalid(0);
  m01_couplers_to_m01_couplers_AWADDR(31 downto 0) <= S_AXI_awaddr(31 downto 0);
  m01_couplers_to_m01_couplers_AWREADY(0) <= M_AXI_awready(0);
  m01_couplers_to_m01_couplers_AWVALID(0) <= S_AXI_awvalid(0);
  m01_couplers_to_m01_couplers_BREADY(0) <= S_AXI_bready(0);
  m01_couplers_to_m01_couplers_BRESP(1 downto 0) <= M_AXI_bresp(1 downto 0);
  m01_couplers_to_m01_couplers_BVALID(0) <= M_AXI_bvalid(0);
  m01_couplers_to_m01_couplers_RDATA(31 downto 0) <= M_AXI_rdata(31 downto 0);
  m01_couplers_to_m01_couplers_RREADY(0) <= S_AXI_rready(0);
  m01_couplers_to_m01_couplers_RRESP(1 downto 0) <= M_AXI_rresp(1 downto 0);
  m01_couplers_to_m01_couplers_RVALID(0) <= M_AXI_rvalid(0);
  m01_couplers_to_m01_couplers_WDATA(31 downto 0) <= S_AXI_wdata(31 downto 0);
  m01_couplers_to_m01_couplers_WREADY(0) <= M_AXI_wready(0);
  m01_couplers_to_m01_couplers_WSTRB(3 downto 0) <= S_AXI_wstrb(3 downto 0);
  m01_couplers_to_m01_couplers_WVALID(0) <= S_AXI_wvalid(0);
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity s00_couplers_imp_Z10DW1 is
  port (
    M_ACLK : in STD_LOGIC;
    M_ARESETN : in STD_LOGIC;
    M_AXI_araddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
    M_AXI_arprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    M_AXI_arready : in STD_LOGIC_VECTOR ( 0 to 0 );
    M_AXI_arvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    M_AXI_awaddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
    M_AXI_awprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    M_AXI_awready : in STD_LOGIC_VECTOR ( 0 to 0 );
    M_AXI_awvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    M_AXI_bready : out STD_LOGIC_VECTOR ( 0 to 0 );
    M_AXI_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    M_AXI_bvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    M_AXI_rdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    M_AXI_rready : out STD_LOGIC_VECTOR ( 0 to 0 );
    M_AXI_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    M_AXI_rvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    M_AXI_wdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    M_AXI_wready : in STD_LOGIC_VECTOR ( 0 to 0 );
    M_AXI_wstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M_AXI_wvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    S_ACLK : in STD_LOGIC;
    S_ARESETN : in STD_LOGIC;
    S_AXI_araddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_arready : out STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_arvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_awaddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_awready : out STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_awvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_bready : in STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_bvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_rready : in STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_rvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_wready : out STD_LOGIC_VECTOR ( 0 to 0 );
    S_AXI_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_wvalid : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end s00_couplers_imp_Z10DW1;

architecture STRUCTURE of s00_couplers_imp_Z10DW1 is
  signal s00_couplers_to_s00_couplers_ARADDR : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal s00_couplers_to_s00_couplers_ARPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal s00_couplers_to_s00_couplers_ARREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal s00_couplers_to_s00_couplers_ARVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal s00_couplers_to_s00_couplers_AWADDR : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal s00_couplers_to_s00_couplers_AWPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal s00_couplers_to_s00_couplers_AWREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal s00_couplers_to_s00_couplers_AWVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal s00_couplers_to_s00_couplers_BREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal s00_couplers_to_s00_couplers_BRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal s00_couplers_to_s00_couplers_BVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal s00_couplers_to_s00_couplers_RDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal s00_couplers_to_s00_couplers_RREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal s00_couplers_to_s00_couplers_RRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal s00_couplers_to_s00_couplers_RVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal s00_couplers_to_s00_couplers_WDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal s00_couplers_to_s00_couplers_WREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal s00_couplers_to_s00_couplers_WSTRB : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal s00_couplers_to_s00_couplers_WVALID : STD_LOGIC_VECTOR ( 0 to 0 );
begin
  M_AXI_araddr(31 downto 0) <= s00_couplers_to_s00_couplers_ARADDR(31 downto 0);
  M_AXI_arprot(2 downto 0) <= s00_couplers_to_s00_couplers_ARPROT(2 downto 0);
  M_AXI_arvalid(0) <= s00_couplers_to_s00_couplers_ARVALID(0);
  M_AXI_awaddr(31 downto 0) <= s00_couplers_to_s00_couplers_AWADDR(31 downto 0);
  M_AXI_awprot(2 downto 0) <= s00_couplers_to_s00_couplers_AWPROT(2 downto 0);
  M_AXI_awvalid(0) <= s00_couplers_to_s00_couplers_AWVALID(0);
  M_AXI_bready(0) <= s00_couplers_to_s00_couplers_BREADY(0);
  M_AXI_rready(0) <= s00_couplers_to_s00_couplers_RREADY(0);
  M_AXI_wdata(31 downto 0) <= s00_couplers_to_s00_couplers_WDATA(31 downto 0);
  M_AXI_wstrb(3 downto 0) <= s00_couplers_to_s00_couplers_WSTRB(3 downto 0);
  M_AXI_wvalid(0) <= s00_couplers_to_s00_couplers_WVALID(0);
  S_AXI_arready(0) <= s00_couplers_to_s00_couplers_ARREADY(0);
  S_AXI_awready(0) <= s00_couplers_to_s00_couplers_AWREADY(0);
  S_AXI_bresp(1 downto 0) <= s00_couplers_to_s00_couplers_BRESP(1 downto 0);
  S_AXI_bvalid(0) <= s00_couplers_to_s00_couplers_BVALID(0);
  S_AXI_rdata(31 downto 0) <= s00_couplers_to_s00_couplers_RDATA(31 downto 0);
  S_AXI_rresp(1 downto 0) <= s00_couplers_to_s00_couplers_RRESP(1 downto 0);
  S_AXI_rvalid(0) <= s00_couplers_to_s00_couplers_RVALID(0);
  S_AXI_wready(0) <= s00_couplers_to_s00_couplers_WREADY(0);
  s00_couplers_to_s00_couplers_ARADDR(31 downto 0) <= S_AXI_araddr(31 downto 0);
  s00_couplers_to_s00_couplers_ARPROT(2 downto 0) <= S_AXI_arprot(2 downto 0);
  s00_couplers_to_s00_couplers_ARREADY(0) <= M_AXI_arready(0);
  s00_couplers_to_s00_couplers_ARVALID(0) <= S_AXI_arvalid(0);
  s00_couplers_to_s00_couplers_AWADDR(31 downto 0) <= S_AXI_awaddr(31 downto 0);
  s00_couplers_to_s00_couplers_AWPROT(2 downto 0) <= S_AXI_awprot(2 downto 0);
  s00_couplers_to_s00_couplers_AWREADY(0) <= M_AXI_awready(0);
  s00_couplers_to_s00_couplers_AWVALID(0) <= S_AXI_awvalid(0);
  s00_couplers_to_s00_couplers_BREADY(0) <= S_AXI_bready(0);
  s00_couplers_to_s00_couplers_BRESP(1 downto 0) <= M_AXI_bresp(1 downto 0);
  s00_couplers_to_s00_couplers_BVALID(0) <= M_AXI_bvalid(0);
  s00_couplers_to_s00_couplers_RDATA(31 downto 0) <= M_AXI_rdata(31 downto 0);
  s00_couplers_to_s00_couplers_RREADY(0) <= S_AXI_rready(0);
  s00_couplers_to_s00_couplers_RRESP(1 downto 0) <= M_AXI_rresp(1 downto 0);
  s00_couplers_to_s00_couplers_RVALID(0) <= M_AXI_rvalid(0);
  s00_couplers_to_s00_couplers_WDATA(31 downto 0) <= S_AXI_wdata(31 downto 0);
  s00_couplers_to_s00_couplers_WREADY(0) <= M_AXI_wready(0);
  s00_couplers_to_s00_couplers_WSTRB(3 downto 0) <= S_AXI_wstrb(3 downto 0);
  s00_couplers_to_s00_couplers_WVALID(0) <= S_AXI_wvalid(0);
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_myip_ethernet_0_axi_periph_3 is
  port (
    ACLK : in STD_LOGIC;
    ARESETN : in STD_LOGIC;
    M00_ACLK : in STD_LOGIC;
    M00_ARESETN : in STD_LOGIC;
    M00_AXI_araddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
    M00_AXI_arready : in STD_LOGIC_VECTOR ( 0 to 0 );
    M00_AXI_arvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    M00_AXI_awaddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
    M00_AXI_awready : in STD_LOGIC_VECTOR ( 0 to 0 );
    M00_AXI_awvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    M00_AXI_bready : out STD_LOGIC_VECTOR ( 0 to 0 );
    M00_AXI_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    M00_AXI_bvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    M00_AXI_rdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    M00_AXI_rready : out STD_LOGIC_VECTOR ( 0 to 0 );
    M00_AXI_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    M00_AXI_rvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    M00_AXI_wdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    M00_AXI_wready : in STD_LOGIC_VECTOR ( 0 to 0 );
    M00_AXI_wstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M00_AXI_wvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    M01_ACLK : in STD_LOGIC;
    M01_ARESETN : in STD_LOGIC;
    M01_AXI_araddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
    M01_AXI_arready : in STD_LOGIC_VECTOR ( 0 to 0 );
    M01_AXI_arvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    M01_AXI_awaddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
    M01_AXI_awready : in STD_LOGIC_VECTOR ( 0 to 0 );
    M01_AXI_awvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    M01_AXI_bready : out STD_LOGIC_VECTOR ( 0 to 0 );
    M01_AXI_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    M01_AXI_bvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    M01_AXI_rdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    M01_AXI_rready : out STD_LOGIC_VECTOR ( 0 to 0 );
    M01_AXI_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    M01_AXI_rvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    M01_AXI_wdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    M01_AXI_wready : in STD_LOGIC_VECTOR ( 0 to 0 );
    M01_AXI_wstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M01_AXI_wvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    S00_ACLK : in STD_LOGIC;
    S00_ARESETN : in STD_LOGIC;
    S00_AXI_araddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S00_AXI_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S00_AXI_arready : out STD_LOGIC_VECTOR ( 0 to 0 );
    S00_AXI_arvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    S00_AXI_awaddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S00_AXI_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S00_AXI_awready : out STD_LOGIC_VECTOR ( 0 to 0 );
    S00_AXI_awvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    S00_AXI_bready : in STD_LOGIC_VECTOR ( 0 to 0 );
    S00_AXI_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S00_AXI_bvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    S00_AXI_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    S00_AXI_rready : in STD_LOGIC_VECTOR ( 0 to 0 );
    S00_AXI_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S00_AXI_rvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    S00_AXI_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S00_AXI_wready : out STD_LOGIC_VECTOR ( 0 to 0 );
    S00_AXI_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S00_AXI_wvalid : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end design_1_myip_ethernet_0_axi_periph_3;

architecture STRUCTURE of design_1_myip_ethernet_0_axi_periph_3 is
  component design_1_xbar_0 is
  port (
    aclk : in STD_LOGIC;
    aresetn : in STD_LOGIC;
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_awvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_awready : out STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_wvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_wready : out STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_bvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_bready : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_araddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_arvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_arready : out STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_rready : in STD_LOGIC_VECTOR ( 0 to 0 );
    m_axi_awaddr : out STD_LOGIC_VECTOR ( 63 downto 0 );
    m_axi_awprot : out STD_LOGIC_VECTOR ( 5 downto 0 );
    m_axi_awvalid : out STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_awready : in STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_wdata : out STD_LOGIC_VECTOR ( 63 downto 0 );
    m_axi_wstrb : out STD_LOGIC_VECTOR ( 7 downto 0 );
    m_axi_wvalid : out STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_wready : in STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_bresp : in STD_LOGIC_VECTOR ( 3 downto 0 );
    m_axi_bvalid : in STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_bready : out STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_araddr : out STD_LOGIC_VECTOR ( 63 downto 0 );
    m_axi_arprot : out STD_LOGIC_VECTOR ( 5 downto 0 );
    m_axi_arvalid : out STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_arready : in STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_rdata : in STD_LOGIC_VECTOR ( 63 downto 0 );
    m_axi_rresp : in STD_LOGIC_VECTOR ( 3 downto 0 );
    m_axi_rvalid : in STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_rready : out STD_LOGIC_VECTOR ( 1 downto 0 )
  );
  end component design_1_xbar_0;
  signal m00_couplers_to_myip_ethernet_0_axi_periph_ARADDR : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal m00_couplers_to_myip_ethernet_0_axi_periph_ARREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m00_couplers_to_myip_ethernet_0_axi_periph_ARVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m00_couplers_to_myip_ethernet_0_axi_periph_AWADDR : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal m00_couplers_to_myip_ethernet_0_axi_periph_AWREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m00_couplers_to_myip_ethernet_0_axi_periph_AWVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m00_couplers_to_myip_ethernet_0_axi_periph_BREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m00_couplers_to_myip_ethernet_0_axi_periph_BRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal m00_couplers_to_myip_ethernet_0_axi_periph_BVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m00_couplers_to_myip_ethernet_0_axi_periph_RDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal m00_couplers_to_myip_ethernet_0_axi_periph_RREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m00_couplers_to_myip_ethernet_0_axi_periph_RRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal m00_couplers_to_myip_ethernet_0_axi_periph_RVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m00_couplers_to_myip_ethernet_0_axi_periph_WDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal m00_couplers_to_myip_ethernet_0_axi_periph_WREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m00_couplers_to_myip_ethernet_0_axi_periph_WSTRB : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal m00_couplers_to_myip_ethernet_0_axi_periph_WVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m01_couplers_to_myip_ethernet_0_axi_periph_ARADDR : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal m01_couplers_to_myip_ethernet_0_axi_periph_ARREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m01_couplers_to_myip_ethernet_0_axi_periph_ARVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m01_couplers_to_myip_ethernet_0_axi_periph_AWADDR : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal m01_couplers_to_myip_ethernet_0_axi_periph_AWREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m01_couplers_to_myip_ethernet_0_axi_periph_AWVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m01_couplers_to_myip_ethernet_0_axi_periph_BREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m01_couplers_to_myip_ethernet_0_axi_periph_BRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal m01_couplers_to_myip_ethernet_0_axi_periph_BVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m01_couplers_to_myip_ethernet_0_axi_periph_RDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal m01_couplers_to_myip_ethernet_0_axi_periph_RREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m01_couplers_to_myip_ethernet_0_axi_periph_RRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal m01_couplers_to_myip_ethernet_0_axi_periph_RVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m01_couplers_to_myip_ethernet_0_axi_periph_WDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal m01_couplers_to_myip_ethernet_0_axi_periph_WREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal m01_couplers_to_myip_ethernet_0_axi_periph_WSTRB : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal m01_couplers_to_myip_ethernet_0_axi_periph_WVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal myip_ethernet_0_axi_periph_ACLK_net : STD_LOGIC;
  signal myip_ethernet_0_axi_periph_ARESETN_net : STD_LOGIC;
  signal myip_ethernet_0_axi_periph_to_s00_couplers_ARADDR : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal myip_ethernet_0_axi_periph_to_s00_couplers_ARPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal myip_ethernet_0_axi_periph_to_s00_couplers_ARREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal myip_ethernet_0_axi_periph_to_s00_couplers_ARVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal myip_ethernet_0_axi_periph_to_s00_couplers_AWADDR : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal myip_ethernet_0_axi_periph_to_s00_couplers_AWPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal myip_ethernet_0_axi_periph_to_s00_couplers_AWREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal myip_ethernet_0_axi_periph_to_s00_couplers_AWVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal myip_ethernet_0_axi_periph_to_s00_couplers_BREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal myip_ethernet_0_axi_periph_to_s00_couplers_BRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal myip_ethernet_0_axi_periph_to_s00_couplers_BVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal myip_ethernet_0_axi_periph_to_s00_couplers_RDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal myip_ethernet_0_axi_periph_to_s00_couplers_RREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal myip_ethernet_0_axi_periph_to_s00_couplers_RRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal myip_ethernet_0_axi_periph_to_s00_couplers_RVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal myip_ethernet_0_axi_periph_to_s00_couplers_WDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal myip_ethernet_0_axi_periph_to_s00_couplers_WREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal myip_ethernet_0_axi_periph_to_s00_couplers_WSTRB : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal myip_ethernet_0_axi_periph_to_s00_couplers_WVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal s00_couplers_to_xbar_ARADDR : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal s00_couplers_to_xbar_ARPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal s00_couplers_to_xbar_ARREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal s00_couplers_to_xbar_ARVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal s00_couplers_to_xbar_AWADDR : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal s00_couplers_to_xbar_AWPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal s00_couplers_to_xbar_AWREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal s00_couplers_to_xbar_AWVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal s00_couplers_to_xbar_BREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal s00_couplers_to_xbar_BRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal s00_couplers_to_xbar_BVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal s00_couplers_to_xbar_RDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal s00_couplers_to_xbar_RREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal s00_couplers_to_xbar_RRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal s00_couplers_to_xbar_RVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal s00_couplers_to_xbar_WDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal s00_couplers_to_xbar_WREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal s00_couplers_to_xbar_WSTRB : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal s00_couplers_to_xbar_WVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal xbar_to_m00_couplers_ARADDR : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal xbar_to_m00_couplers_ARREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal xbar_to_m00_couplers_ARVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal xbar_to_m00_couplers_AWADDR : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal xbar_to_m00_couplers_AWREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal xbar_to_m00_couplers_AWVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal xbar_to_m00_couplers_BREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal xbar_to_m00_couplers_BRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal xbar_to_m00_couplers_BVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal xbar_to_m00_couplers_RDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal xbar_to_m00_couplers_RREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal xbar_to_m00_couplers_RRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal xbar_to_m00_couplers_RVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal xbar_to_m00_couplers_WDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal xbar_to_m00_couplers_WREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal xbar_to_m00_couplers_WSTRB : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal xbar_to_m00_couplers_WVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal xbar_to_m01_couplers_ARADDR : STD_LOGIC_VECTOR ( 63 downto 32 );
  signal xbar_to_m01_couplers_ARREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal xbar_to_m01_couplers_ARVALID : STD_LOGIC_VECTOR ( 1 to 1 );
  signal xbar_to_m01_couplers_AWADDR : STD_LOGIC_VECTOR ( 63 downto 32 );
  signal xbar_to_m01_couplers_AWREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal xbar_to_m01_couplers_AWVALID : STD_LOGIC_VECTOR ( 1 to 1 );
  signal xbar_to_m01_couplers_BREADY : STD_LOGIC_VECTOR ( 1 to 1 );
  signal xbar_to_m01_couplers_BRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal xbar_to_m01_couplers_BVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal xbar_to_m01_couplers_RDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal xbar_to_m01_couplers_RREADY : STD_LOGIC_VECTOR ( 1 to 1 );
  signal xbar_to_m01_couplers_RRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal xbar_to_m01_couplers_RVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal xbar_to_m01_couplers_WDATA : STD_LOGIC_VECTOR ( 63 downto 32 );
  signal xbar_to_m01_couplers_WREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal xbar_to_m01_couplers_WSTRB : STD_LOGIC_VECTOR ( 7 downto 4 );
  signal xbar_to_m01_couplers_WVALID : STD_LOGIC_VECTOR ( 1 to 1 );
  signal NLW_xbar_m_axi_arprot_UNCONNECTED : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal NLW_xbar_m_axi_awprot_UNCONNECTED : STD_LOGIC_VECTOR ( 5 downto 0 );
begin
  M00_AXI_araddr(31 downto 0) <= m00_couplers_to_myip_ethernet_0_axi_periph_ARADDR(31 downto 0);
  M00_AXI_arvalid(0) <= m00_couplers_to_myip_ethernet_0_axi_periph_ARVALID(0);
  M00_AXI_awaddr(31 downto 0) <= m00_couplers_to_myip_ethernet_0_axi_periph_AWADDR(31 downto 0);
  M00_AXI_awvalid(0) <= m00_couplers_to_myip_ethernet_0_axi_periph_AWVALID(0);
  M00_AXI_bready(0) <= m00_couplers_to_myip_ethernet_0_axi_periph_BREADY(0);
  M00_AXI_rready(0) <= m00_couplers_to_myip_ethernet_0_axi_periph_RREADY(0);
  M00_AXI_wdata(31 downto 0) <= m00_couplers_to_myip_ethernet_0_axi_periph_WDATA(31 downto 0);
  M00_AXI_wstrb(3 downto 0) <= m00_couplers_to_myip_ethernet_0_axi_periph_WSTRB(3 downto 0);
  M00_AXI_wvalid(0) <= m00_couplers_to_myip_ethernet_0_axi_periph_WVALID(0);
  M01_AXI_araddr(31 downto 0) <= m01_couplers_to_myip_ethernet_0_axi_periph_ARADDR(31 downto 0);
  M01_AXI_arvalid(0) <= m01_couplers_to_myip_ethernet_0_axi_periph_ARVALID(0);
  M01_AXI_awaddr(31 downto 0) <= m01_couplers_to_myip_ethernet_0_axi_periph_AWADDR(31 downto 0);
  M01_AXI_awvalid(0) <= m01_couplers_to_myip_ethernet_0_axi_periph_AWVALID(0);
  M01_AXI_bready(0) <= m01_couplers_to_myip_ethernet_0_axi_periph_BREADY(0);
  M01_AXI_rready(0) <= m01_couplers_to_myip_ethernet_0_axi_periph_RREADY(0);
  M01_AXI_wdata(31 downto 0) <= m01_couplers_to_myip_ethernet_0_axi_periph_WDATA(31 downto 0);
  M01_AXI_wstrb(3 downto 0) <= m01_couplers_to_myip_ethernet_0_axi_periph_WSTRB(3 downto 0);
  M01_AXI_wvalid(0) <= m01_couplers_to_myip_ethernet_0_axi_periph_WVALID(0);
  S00_AXI_arready(0) <= myip_ethernet_0_axi_periph_to_s00_couplers_ARREADY(0);
  S00_AXI_awready(0) <= myip_ethernet_0_axi_periph_to_s00_couplers_AWREADY(0);
  S00_AXI_bresp(1 downto 0) <= myip_ethernet_0_axi_periph_to_s00_couplers_BRESP(1 downto 0);
  S00_AXI_bvalid(0) <= myip_ethernet_0_axi_periph_to_s00_couplers_BVALID(0);
  S00_AXI_rdata(31 downto 0) <= myip_ethernet_0_axi_periph_to_s00_couplers_RDATA(31 downto 0);
  S00_AXI_rresp(1 downto 0) <= myip_ethernet_0_axi_periph_to_s00_couplers_RRESP(1 downto 0);
  S00_AXI_rvalid(0) <= myip_ethernet_0_axi_periph_to_s00_couplers_RVALID(0);
  S00_AXI_wready(0) <= myip_ethernet_0_axi_periph_to_s00_couplers_WREADY(0);
  m00_couplers_to_myip_ethernet_0_axi_periph_ARREADY(0) <= M00_AXI_arready(0);
  m00_couplers_to_myip_ethernet_0_axi_periph_AWREADY(0) <= M00_AXI_awready(0);
  m00_couplers_to_myip_ethernet_0_axi_periph_BRESP(1 downto 0) <= M00_AXI_bresp(1 downto 0);
  m00_couplers_to_myip_ethernet_0_axi_periph_BVALID(0) <= M00_AXI_bvalid(0);
  m00_couplers_to_myip_ethernet_0_axi_periph_RDATA(31 downto 0) <= M00_AXI_rdata(31 downto 0);
  m00_couplers_to_myip_ethernet_0_axi_periph_RRESP(1 downto 0) <= M00_AXI_rresp(1 downto 0);
  m00_couplers_to_myip_ethernet_0_axi_periph_RVALID(0) <= M00_AXI_rvalid(0);
  m00_couplers_to_myip_ethernet_0_axi_periph_WREADY(0) <= M00_AXI_wready(0);
  m01_couplers_to_myip_ethernet_0_axi_periph_ARREADY(0) <= M01_AXI_arready(0);
  m01_couplers_to_myip_ethernet_0_axi_periph_AWREADY(0) <= M01_AXI_awready(0);
  m01_couplers_to_myip_ethernet_0_axi_periph_BRESP(1 downto 0) <= M01_AXI_bresp(1 downto 0);
  m01_couplers_to_myip_ethernet_0_axi_periph_BVALID(0) <= M01_AXI_bvalid(0);
  m01_couplers_to_myip_ethernet_0_axi_periph_RDATA(31 downto 0) <= M01_AXI_rdata(31 downto 0);
  m01_couplers_to_myip_ethernet_0_axi_periph_RRESP(1 downto 0) <= M01_AXI_rresp(1 downto 0);
  m01_couplers_to_myip_ethernet_0_axi_periph_RVALID(0) <= M01_AXI_rvalid(0);
  m01_couplers_to_myip_ethernet_0_axi_periph_WREADY(0) <= M01_AXI_wready(0);
  myip_ethernet_0_axi_periph_ACLK_net <= ACLK;
  myip_ethernet_0_axi_periph_ARESETN_net <= ARESETN;
  myip_ethernet_0_axi_periph_to_s00_couplers_ARADDR(31 downto 0) <= S00_AXI_araddr(31 downto 0);
  myip_ethernet_0_axi_periph_to_s00_couplers_ARPROT(2 downto 0) <= S00_AXI_arprot(2 downto 0);
  myip_ethernet_0_axi_periph_to_s00_couplers_ARVALID(0) <= S00_AXI_arvalid(0);
  myip_ethernet_0_axi_periph_to_s00_couplers_AWADDR(31 downto 0) <= S00_AXI_awaddr(31 downto 0);
  myip_ethernet_0_axi_periph_to_s00_couplers_AWPROT(2 downto 0) <= S00_AXI_awprot(2 downto 0);
  myip_ethernet_0_axi_periph_to_s00_couplers_AWVALID(0) <= S00_AXI_awvalid(0);
  myip_ethernet_0_axi_periph_to_s00_couplers_BREADY(0) <= S00_AXI_bready(0);
  myip_ethernet_0_axi_periph_to_s00_couplers_RREADY(0) <= S00_AXI_rready(0);
  myip_ethernet_0_axi_periph_to_s00_couplers_WDATA(31 downto 0) <= S00_AXI_wdata(31 downto 0);
  myip_ethernet_0_axi_periph_to_s00_couplers_WSTRB(3 downto 0) <= S00_AXI_wstrb(3 downto 0);
  myip_ethernet_0_axi_periph_to_s00_couplers_WVALID(0) <= S00_AXI_wvalid(0);
m00_couplers: entity work.m00_couplers_imp_11GI61F
     port map (
      M_ACLK => myip_ethernet_0_axi_periph_ACLK_net,
      M_ARESETN => myip_ethernet_0_axi_periph_ARESETN_net,
      M_AXI_araddr(31 downto 0) => m00_couplers_to_myip_ethernet_0_axi_periph_ARADDR(31 downto 0),
      M_AXI_arready(0) => m00_couplers_to_myip_ethernet_0_axi_periph_ARREADY(0),
      M_AXI_arvalid(0) => m00_couplers_to_myip_ethernet_0_axi_periph_ARVALID(0),
      M_AXI_awaddr(31 downto 0) => m00_couplers_to_myip_ethernet_0_axi_periph_AWADDR(31 downto 0),
      M_AXI_awready(0) => m00_couplers_to_myip_ethernet_0_axi_periph_AWREADY(0),
      M_AXI_awvalid(0) => m00_couplers_to_myip_ethernet_0_axi_periph_AWVALID(0),
      M_AXI_bready(0) => m00_couplers_to_myip_ethernet_0_axi_periph_BREADY(0),
      M_AXI_bresp(1 downto 0) => m00_couplers_to_myip_ethernet_0_axi_periph_BRESP(1 downto 0),
      M_AXI_bvalid(0) => m00_couplers_to_myip_ethernet_0_axi_periph_BVALID(0),
      M_AXI_rdata(31 downto 0) => m00_couplers_to_myip_ethernet_0_axi_periph_RDATA(31 downto 0),
      M_AXI_rready(0) => m00_couplers_to_myip_ethernet_0_axi_periph_RREADY(0),
      M_AXI_rresp(1 downto 0) => m00_couplers_to_myip_ethernet_0_axi_periph_RRESP(1 downto 0),
      M_AXI_rvalid(0) => m00_couplers_to_myip_ethernet_0_axi_periph_RVALID(0),
      M_AXI_wdata(31 downto 0) => m00_couplers_to_myip_ethernet_0_axi_periph_WDATA(31 downto 0),
      M_AXI_wready(0) => m00_couplers_to_myip_ethernet_0_axi_periph_WREADY(0),
      M_AXI_wstrb(3 downto 0) => m00_couplers_to_myip_ethernet_0_axi_periph_WSTRB(3 downto 0),
      M_AXI_wvalid(0) => m00_couplers_to_myip_ethernet_0_axi_periph_WVALID(0),
      S_ACLK => myip_ethernet_0_axi_periph_ACLK_net,
      S_ARESETN => myip_ethernet_0_axi_periph_ARESETN_net,
      S_AXI_araddr(31 downto 0) => xbar_to_m00_couplers_ARADDR(31 downto 0),
      S_AXI_arready(0) => xbar_to_m00_couplers_ARREADY(0),
      S_AXI_arvalid(0) => xbar_to_m00_couplers_ARVALID(0),
      S_AXI_awaddr(31 downto 0) => xbar_to_m00_couplers_AWADDR(31 downto 0),
      S_AXI_awready(0) => xbar_to_m00_couplers_AWREADY(0),
      S_AXI_awvalid(0) => xbar_to_m00_couplers_AWVALID(0),
      S_AXI_bready(0) => xbar_to_m00_couplers_BREADY(0),
      S_AXI_bresp(1 downto 0) => xbar_to_m00_couplers_BRESP(1 downto 0),
      S_AXI_bvalid(0) => xbar_to_m00_couplers_BVALID(0),
      S_AXI_rdata(31 downto 0) => xbar_to_m00_couplers_RDATA(31 downto 0),
      S_AXI_rready(0) => xbar_to_m00_couplers_RREADY(0),
      S_AXI_rresp(1 downto 0) => xbar_to_m00_couplers_RRESP(1 downto 0),
      S_AXI_rvalid(0) => xbar_to_m00_couplers_RVALID(0),
      S_AXI_wdata(31 downto 0) => xbar_to_m00_couplers_WDATA(31 downto 0),
      S_AXI_wready(0) => xbar_to_m00_couplers_WREADY(0),
      S_AXI_wstrb(3 downto 0) => xbar_to_m00_couplers_WSTRB(3 downto 0),
      S_AXI_wvalid(0) => xbar_to_m00_couplers_WVALID(0)
    );
m01_couplers: entity work.m01_couplers_imp_SY0ZJM
     port map (
      M_ACLK => myip_ethernet_0_axi_periph_ACLK_net,
      M_ARESETN => myip_ethernet_0_axi_periph_ARESETN_net,
      M_AXI_araddr(31 downto 0) => m01_couplers_to_myip_ethernet_0_axi_periph_ARADDR(31 downto 0),
      M_AXI_arready(0) => m01_couplers_to_myip_ethernet_0_axi_periph_ARREADY(0),
      M_AXI_arvalid(0) => m01_couplers_to_myip_ethernet_0_axi_periph_ARVALID(0),
      M_AXI_awaddr(31 downto 0) => m01_couplers_to_myip_ethernet_0_axi_periph_AWADDR(31 downto 0),
      M_AXI_awready(0) => m01_couplers_to_myip_ethernet_0_axi_periph_AWREADY(0),
      M_AXI_awvalid(0) => m01_couplers_to_myip_ethernet_0_axi_periph_AWVALID(0),
      M_AXI_bready(0) => m01_couplers_to_myip_ethernet_0_axi_periph_BREADY(0),
      M_AXI_bresp(1 downto 0) => m01_couplers_to_myip_ethernet_0_axi_periph_BRESP(1 downto 0),
      M_AXI_bvalid(0) => m01_couplers_to_myip_ethernet_0_axi_periph_BVALID(0),
      M_AXI_rdata(31 downto 0) => m01_couplers_to_myip_ethernet_0_axi_periph_RDATA(31 downto 0),
      M_AXI_rready(0) => m01_couplers_to_myip_ethernet_0_axi_periph_RREADY(0),
      M_AXI_rresp(1 downto 0) => m01_couplers_to_myip_ethernet_0_axi_periph_RRESP(1 downto 0),
      M_AXI_rvalid(0) => m01_couplers_to_myip_ethernet_0_axi_periph_RVALID(0),
      M_AXI_wdata(31 downto 0) => m01_couplers_to_myip_ethernet_0_axi_periph_WDATA(31 downto 0),
      M_AXI_wready(0) => m01_couplers_to_myip_ethernet_0_axi_periph_WREADY(0),
      M_AXI_wstrb(3 downto 0) => m01_couplers_to_myip_ethernet_0_axi_periph_WSTRB(3 downto 0),
      M_AXI_wvalid(0) => m01_couplers_to_myip_ethernet_0_axi_periph_WVALID(0),
      S_ACLK => myip_ethernet_0_axi_periph_ACLK_net,
      S_ARESETN => myip_ethernet_0_axi_periph_ARESETN_net,
      S_AXI_araddr(31 downto 0) => xbar_to_m01_couplers_ARADDR(63 downto 32),
      S_AXI_arready(0) => xbar_to_m01_couplers_ARREADY(0),
      S_AXI_arvalid(0) => xbar_to_m01_couplers_ARVALID(1),
      S_AXI_awaddr(31 downto 0) => xbar_to_m01_couplers_AWADDR(63 downto 32),
      S_AXI_awready(0) => xbar_to_m01_couplers_AWREADY(0),
      S_AXI_awvalid(0) => xbar_to_m01_couplers_AWVALID(1),
      S_AXI_bready(0) => xbar_to_m01_couplers_BREADY(1),
      S_AXI_bresp(1 downto 0) => xbar_to_m01_couplers_BRESP(1 downto 0),
      S_AXI_bvalid(0) => xbar_to_m01_couplers_BVALID(0),
      S_AXI_rdata(31 downto 0) => xbar_to_m01_couplers_RDATA(31 downto 0),
      S_AXI_rready(0) => xbar_to_m01_couplers_RREADY(1),
      S_AXI_rresp(1 downto 0) => xbar_to_m01_couplers_RRESP(1 downto 0),
      S_AXI_rvalid(0) => xbar_to_m01_couplers_RVALID(0),
      S_AXI_wdata(31 downto 0) => xbar_to_m01_couplers_WDATA(63 downto 32),
      S_AXI_wready(0) => xbar_to_m01_couplers_WREADY(0),
      S_AXI_wstrb(3 downto 0) => xbar_to_m01_couplers_WSTRB(7 downto 4),
      S_AXI_wvalid(0) => xbar_to_m01_couplers_WVALID(1)
    );
s00_couplers: entity work.s00_couplers_imp_Z10DW1
     port map (
      M_ACLK => myip_ethernet_0_axi_periph_ACLK_net,
      M_ARESETN => myip_ethernet_0_axi_periph_ARESETN_net,
      M_AXI_araddr(31 downto 0) => s00_couplers_to_xbar_ARADDR(31 downto 0),
      M_AXI_arprot(2 downto 0) => s00_couplers_to_xbar_ARPROT(2 downto 0),
      M_AXI_arready(0) => s00_couplers_to_xbar_ARREADY(0),
      M_AXI_arvalid(0) => s00_couplers_to_xbar_ARVALID(0),
      M_AXI_awaddr(31 downto 0) => s00_couplers_to_xbar_AWADDR(31 downto 0),
      M_AXI_awprot(2 downto 0) => s00_couplers_to_xbar_AWPROT(2 downto 0),
      M_AXI_awready(0) => s00_couplers_to_xbar_AWREADY(0),
      M_AXI_awvalid(0) => s00_couplers_to_xbar_AWVALID(0),
      M_AXI_bready(0) => s00_couplers_to_xbar_BREADY(0),
      M_AXI_bresp(1 downto 0) => s00_couplers_to_xbar_BRESP(1 downto 0),
      M_AXI_bvalid(0) => s00_couplers_to_xbar_BVALID(0),
      M_AXI_rdata(31 downto 0) => s00_couplers_to_xbar_RDATA(31 downto 0),
      M_AXI_rready(0) => s00_couplers_to_xbar_RREADY(0),
      M_AXI_rresp(1 downto 0) => s00_couplers_to_xbar_RRESP(1 downto 0),
      M_AXI_rvalid(0) => s00_couplers_to_xbar_RVALID(0),
      M_AXI_wdata(31 downto 0) => s00_couplers_to_xbar_WDATA(31 downto 0),
      M_AXI_wready(0) => s00_couplers_to_xbar_WREADY(0),
      M_AXI_wstrb(3 downto 0) => s00_couplers_to_xbar_WSTRB(3 downto 0),
      M_AXI_wvalid(0) => s00_couplers_to_xbar_WVALID(0),
      S_ACLK => myip_ethernet_0_axi_periph_ACLK_net,
      S_ARESETN => myip_ethernet_0_axi_periph_ARESETN_net,
      S_AXI_araddr(31 downto 0) => myip_ethernet_0_axi_periph_to_s00_couplers_ARADDR(31 downto 0),
      S_AXI_arprot(2 downto 0) => myip_ethernet_0_axi_periph_to_s00_couplers_ARPROT(2 downto 0),
      S_AXI_arready(0) => myip_ethernet_0_axi_periph_to_s00_couplers_ARREADY(0),
      S_AXI_arvalid(0) => myip_ethernet_0_axi_periph_to_s00_couplers_ARVALID(0),
      S_AXI_awaddr(31 downto 0) => myip_ethernet_0_axi_periph_to_s00_couplers_AWADDR(31 downto 0),
      S_AXI_awprot(2 downto 0) => myip_ethernet_0_axi_periph_to_s00_couplers_AWPROT(2 downto 0),
      S_AXI_awready(0) => myip_ethernet_0_axi_periph_to_s00_couplers_AWREADY(0),
      S_AXI_awvalid(0) => myip_ethernet_0_axi_periph_to_s00_couplers_AWVALID(0),
      S_AXI_bready(0) => myip_ethernet_0_axi_periph_to_s00_couplers_BREADY(0),
      S_AXI_bresp(1 downto 0) => myip_ethernet_0_axi_periph_to_s00_couplers_BRESP(1 downto 0),
      S_AXI_bvalid(0) => myip_ethernet_0_axi_periph_to_s00_couplers_BVALID(0),
      S_AXI_rdata(31 downto 0) => myip_ethernet_0_axi_periph_to_s00_couplers_RDATA(31 downto 0),
      S_AXI_rready(0) => myip_ethernet_0_axi_periph_to_s00_couplers_RREADY(0),
      S_AXI_rresp(1 downto 0) => myip_ethernet_0_axi_periph_to_s00_couplers_RRESP(1 downto 0),
      S_AXI_rvalid(0) => myip_ethernet_0_axi_periph_to_s00_couplers_RVALID(0),
      S_AXI_wdata(31 downto 0) => myip_ethernet_0_axi_periph_to_s00_couplers_WDATA(31 downto 0),
      S_AXI_wready(0) => myip_ethernet_0_axi_periph_to_s00_couplers_WREADY(0),
      S_AXI_wstrb(3 downto 0) => myip_ethernet_0_axi_periph_to_s00_couplers_WSTRB(3 downto 0),
      S_AXI_wvalid(0) => myip_ethernet_0_axi_periph_to_s00_couplers_WVALID(0)
    );
xbar: component design_1_xbar_0
     port map (
      aclk => myip_ethernet_0_axi_periph_ACLK_net,
      aresetn => myip_ethernet_0_axi_periph_ARESETN_net,
      m_axi_araddr(63 downto 32) => xbar_to_m01_couplers_ARADDR(63 downto 32),
      m_axi_araddr(31 downto 0) => xbar_to_m00_couplers_ARADDR(31 downto 0),
      m_axi_arprot(5 downto 0) => NLW_xbar_m_axi_arprot_UNCONNECTED(5 downto 0),
      m_axi_arready(1) => xbar_to_m01_couplers_ARREADY(0),
      m_axi_arready(0) => xbar_to_m00_couplers_ARREADY(0),
      m_axi_arvalid(1) => xbar_to_m01_couplers_ARVALID(1),
      m_axi_arvalid(0) => xbar_to_m00_couplers_ARVALID(0),
      m_axi_awaddr(63 downto 32) => xbar_to_m01_couplers_AWADDR(63 downto 32),
      m_axi_awaddr(31 downto 0) => xbar_to_m00_couplers_AWADDR(31 downto 0),
      m_axi_awprot(5 downto 0) => NLW_xbar_m_axi_awprot_UNCONNECTED(5 downto 0),
      m_axi_awready(1) => xbar_to_m01_couplers_AWREADY(0),
      m_axi_awready(0) => xbar_to_m00_couplers_AWREADY(0),
      m_axi_awvalid(1) => xbar_to_m01_couplers_AWVALID(1),
      m_axi_awvalid(0) => xbar_to_m00_couplers_AWVALID(0),
      m_axi_bready(1) => xbar_to_m01_couplers_BREADY(1),
      m_axi_bready(0) => xbar_to_m00_couplers_BREADY(0),
      m_axi_bresp(3 downto 2) => xbar_to_m01_couplers_BRESP(1 downto 0),
      m_axi_bresp(1 downto 0) => xbar_to_m00_couplers_BRESP(1 downto 0),
      m_axi_bvalid(1) => xbar_to_m01_couplers_BVALID(0),
      m_axi_bvalid(0) => xbar_to_m00_couplers_BVALID(0),
      m_axi_rdata(63 downto 32) => xbar_to_m01_couplers_RDATA(31 downto 0),
      m_axi_rdata(31 downto 0) => xbar_to_m00_couplers_RDATA(31 downto 0),
      m_axi_rready(1) => xbar_to_m01_couplers_RREADY(1),
      m_axi_rready(0) => xbar_to_m00_couplers_RREADY(0),
      m_axi_rresp(3 downto 2) => xbar_to_m01_couplers_RRESP(1 downto 0),
      m_axi_rresp(1 downto 0) => xbar_to_m00_couplers_RRESP(1 downto 0),
      m_axi_rvalid(1) => xbar_to_m01_couplers_RVALID(0),
      m_axi_rvalid(0) => xbar_to_m00_couplers_RVALID(0),
      m_axi_wdata(63 downto 32) => xbar_to_m01_couplers_WDATA(63 downto 32),
      m_axi_wdata(31 downto 0) => xbar_to_m00_couplers_WDATA(31 downto 0),
      m_axi_wready(1) => xbar_to_m01_couplers_WREADY(0),
      m_axi_wready(0) => xbar_to_m00_couplers_WREADY(0),
      m_axi_wstrb(7 downto 4) => xbar_to_m01_couplers_WSTRB(7 downto 4),
      m_axi_wstrb(3 downto 0) => xbar_to_m00_couplers_WSTRB(3 downto 0),
      m_axi_wvalid(1) => xbar_to_m01_couplers_WVALID(1),
      m_axi_wvalid(0) => xbar_to_m00_couplers_WVALID(0),
      s_axi_araddr(31 downto 0) => s00_couplers_to_xbar_ARADDR(31 downto 0),
      s_axi_arprot(2 downto 0) => s00_couplers_to_xbar_ARPROT(2 downto 0),
      s_axi_arready(0) => s00_couplers_to_xbar_ARREADY(0),
      s_axi_arvalid(0) => s00_couplers_to_xbar_ARVALID(0),
      s_axi_awaddr(31 downto 0) => s00_couplers_to_xbar_AWADDR(31 downto 0),
      s_axi_awprot(2 downto 0) => s00_couplers_to_xbar_AWPROT(2 downto 0),
      s_axi_awready(0) => s00_couplers_to_xbar_AWREADY(0),
      s_axi_awvalid(0) => s00_couplers_to_xbar_AWVALID(0),
      s_axi_bready(0) => s00_couplers_to_xbar_BREADY(0),
      s_axi_bresp(1 downto 0) => s00_couplers_to_xbar_BRESP(1 downto 0),
      s_axi_bvalid(0) => s00_couplers_to_xbar_BVALID(0),
      s_axi_rdata(31 downto 0) => s00_couplers_to_xbar_RDATA(31 downto 0),
      s_axi_rready(0) => s00_couplers_to_xbar_RREADY(0),
      s_axi_rresp(1 downto 0) => s00_couplers_to_xbar_RRESP(1 downto 0),
      s_axi_rvalid(0) => s00_couplers_to_xbar_RVALID(0),
      s_axi_wdata(31 downto 0) => s00_couplers_to_xbar_WDATA(31 downto 0),
      s_axi_wready(0) => s00_couplers_to_xbar_WREADY(0),
      s_axi_wstrb(3 downto 0) => s00_couplers_to_xbar_WSTRB(3 downto 0),
      s_axi_wvalid(0) => s00_couplers_to_xbar_WVALID(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1 is
  port (
    clk_100MHz : in STD_LOGIC;
    diff_clock_rtl_1_clk_n : in STD_LOGIC;
    diff_clock_rtl_1_clk_p : in STD_LOGIC;
    mdio_rtl_0_mdc : out STD_LOGIC;
    mdio_rtl_0_mdio_i : in STD_LOGIC;
    mdio_rtl_0_mdio_o : out STD_LOGIC;
    mdio_rtl_0_mdio_t : out STD_LOGIC;
    reset_rtl_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    rst_0 : in STD_LOGIC;
    sgmii_rtl_0_rxn : in STD_LOGIC;
    sgmii_rtl_0_rxp : in STD_LOGIC;
    sgmii_rtl_0_txn : out STD_LOGIC;
    sgmii_rtl_0_txp : out STD_LOGIC
  );
  attribute CORE_GENERATION_INFO : string;
  attribute CORE_GENERATION_INFO of design_1 : entity is "design_1,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLibrary=BlockDiagram,x_ipName=design_1,x_ipVersion=1.00.a,x_ipLanguage=VHDL,numBlks=13,numReposBlks=9,numNonXlnxBlks=0,numHierBlks=4,maxHierDepth=0,numSysgenBlks=0,numHlsBlks=0,numHdlrefBlks=2,numPkgbdBlks=0,bdsource=USER,da_aeth_cnt=1,da_axi4_cnt=6,da_axi4_s2mm_cnt=1,da_board_cnt=39,da_clkrst_cnt=12,da_mb_cnt=1,synth_mode=OOC_per_IP}";
  attribute HW_HANDOFF : string;
  attribute HW_HANDOFF of design_1 : entity is "design_1.hwdef";
end design_1;

architecture STRUCTURE of design_1 is
  component design_1_axi_ethernet_0_0 is
  port (
    s_axi_lite_resetn : in STD_LOGIC;
    s_axi_lite_clk : in STD_LOGIC;
    mac_irq : out STD_LOGIC;
    axis_clk : in STD_LOGIC;
    axi_txd_arstn : in STD_LOGIC;
    axi_txc_arstn : in STD_LOGIC;
    axi_rxd_arstn : in STD_LOGIC;
    axi_rxs_arstn : in STD_LOGIC;
    interrupt : out STD_LOGIC;
    signal_detect : in STD_LOGIC;
    mmcm_locked_out : out STD_LOGIC;
    rxuserclk_out : out STD_LOGIC;
    rxuserclk2_out : out STD_LOGIC;
    userclk_out : out STD_LOGIC;
    userclk2_out : out STD_LOGIC;
    pma_reset_out : out STD_LOGIC;
    gt0_pll0outclk_out : out STD_LOGIC;
    gt0_pll0outrefclk_out : out STD_LOGIC;
    gt0_pll1outclk_out : out STD_LOGIC;
    gt0_pll1outrefclk_out : out STD_LOGIC;
    gt0_pll0lock_out : out STD_LOGIC;
    gt0_pll0refclklost_out : out STD_LOGIC;
    phy_rst_n : out STD_LOGIC_VECTOR ( 0 to 0 );
    ref_clk : in STD_LOGIC;
    gtref_clk_out : out STD_LOGIC;
    gtref_clk_buf_out : out STD_LOGIC;
    s_axi_araddr : in STD_LOGIC_VECTOR ( 17 downto 0 );
    s_axi_arready : out STD_LOGIC;
    s_axi_arvalid : in STD_LOGIC;
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 17 downto 0 );
    s_axi_awready : out STD_LOGIC;
    s_axi_awvalid : in STD_LOGIC;
    s_axi_bready : in STD_LOGIC;
    s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_bvalid : out STD_LOGIC;
    s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rready : in STD_LOGIC;
    s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rvalid : out STD_LOGIC;
    s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_wready : out STD_LOGIC;
    s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_wvalid : in STD_LOGIC;
    s_axis_txc_tdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axis_txc_tkeep : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axis_txc_tlast : in STD_LOGIC;
    s_axis_txc_tready : out STD_LOGIC;
    s_axis_txc_tvalid : in STD_LOGIC;
    s_axis_txd_tdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axis_txd_tkeep : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axis_txd_tlast : in STD_LOGIC;
    s_axis_txd_tready : out STD_LOGIC;
    s_axis_txd_tvalid : in STD_LOGIC;
    m_axis_rxd_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axis_rxd_tkeep : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_axis_rxd_tlast : out STD_LOGIC;
    m_axis_rxd_tready : in STD_LOGIC;
    m_axis_rxd_tvalid : out STD_LOGIC;
    m_axis_rxs_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axis_rxs_tkeep : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_axis_rxs_tlast : out STD_LOGIC;
    m_axis_rxs_tready : in STD_LOGIC;
    m_axis_rxs_tvalid : out STD_LOGIC;
    sgmii_rxn : in STD_LOGIC;
    sgmii_rxp : in STD_LOGIC;
    sgmii_txn : out STD_LOGIC;
    sgmii_txp : out STD_LOGIC;
    mdio_mdc : out STD_LOGIC;
    mdio_mdio_i : in STD_LOGIC;
    mdio_mdio_o : out STD_LOGIC;
    mdio_mdio_t : out STD_LOGIC;
    mgt_clk_clk_n : in STD_LOGIC;
    mgt_clk_clk_p : in STD_LOGIC
  );
  end component design_1_axi_ethernet_0_0;
  component design_1_fifo_generator_0_1 is
  port (
    rst : in STD_LOGIC;
    wr_clk : in STD_LOGIC;
    rd_clk : in STD_LOGIC;
    din : in STD_LOGIC_VECTOR ( 31 downto 0 );
    wr_en : in STD_LOGIC;
    rd_en : in STD_LOGIC;
    dout : out STD_LOGIC_VECTOR ( 31 downto 0 );
    full : out STD_LOGIC;
    empty : out STD_LOGIC
  );
  end component design_1_fifo_generator_0_1;
  component design_1_myip_ethernet_0_2 is
  port (
    CLK_read : in STD_LOGIC;
    RST : in STD_LOGIC;
    FIFO_RD_EN : out STD_LOGIC;
    FIFO_Empty : in STD_LOGIC;
    FIFO_dato_Read : in STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_axi_awaddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_axi_awprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    m00_axi_awvalid : out STD_LOGIC;
    m00_axi_awready : in STD_LOGIC;
    m00_axi_wdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_axi_wstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m00_axi_wvalid : out STD_LOGIC;
    m00_axi_wready : in STD_LOGIC;
    m00_axi_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    m00_axi_bvalid : in STD_LOGIC;
    m00_axi_bready : out STD_LOGIC;
    m00_axi_araddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_axi_arprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    m00_axi_arvalid : out STD_LOGIC;
    m00_axi_arready : in STD_LOGIC;
    m00_axi_rdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_axi_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    m00_axi_rvalid : in STD_LOGIC;
    m00_axi_rready : out STD_LOGIC;
    m00_axi_aclk : in STD_LOGIC;
    m00_axi_aresetn : in STD_LOGIC;
    m00_axi_init_axi_txn : in STD_LOGIC;
    m00_axi_error : out STD_LOGIC;
    m00_axi_txn_done : out STD_LOGIC
  );
  end component design_1_myip_ethernet_0_2;
  component design_1_write_fifo_0_1 is
  port (
    clk_Write : in STD_LOGIC;
    RST : in STD_LOGIC;
    FIFO_FULL : in STD_LOGIC;
    FIFO_WR_EN : out STD_LOGIC;
    FIFO_DATO_WR : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  end component design_1_write_fifo_0_1;
  component design_1_clk_wiz_0_0 is
  port (
    reset : in STD_LOGIC;
    clk_in1 : in STD_LOGIC;
    clk_200M : out STD_LOGIC;
    clk_65M : out STD_LOGIC;
    clk_120M : out STD_LOGIC
  );
  end component design_1_clk_wiz_0_0;
  component design_1_GATE_NOT_0_0 is
  port (
    NOT_IN : in STD_LOGIC;
    NOT_OUT : out STD_LOGIC
  );
  end component design_1_GATE_NOT_0_0;
  component design_1_axi_ethernet_0_fifo_0 is
  port (
    interrupt : out STD_LOGIC;
    s_axi_aclk : in STD_LOGIC;
    s_axi_aresetn : in STD_LOGIC;
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_awvalid : in STD_LOGIC;
    s_axi_awready : out STD_LOGIC;
    s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_wvalid : in STD_LOGIC;
    s_axi_wready : out STD_LOGIC;
    s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_bvalid : out STD_LOGIC;
    s_axi_bready : in STD_LOGIC;
    s_axi_araddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_arvalid : in STD_LOGIC;
    s_axi_arready : out STD_LOGIC;
    s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rvalid : out STD_LOGIC;
    s_axi_rready : in STD_LOGIC;
    mm2s_prmry_reset_out_n : out STD_LOGIC;
    axi_str_txd_tvalid : out STD_LOGIC;
    axi_str_txd_tready : in STD_LOGIC;
    axi_str_txd_tlast : out STD_LOGIC;
    axi_str_txd_tkeep : out STD_LOGIC_VECTOR ( 3 downto 0 );
    axi_str_txd_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    mm2s_cntrl_reset_out_n : out STD_LOGIC;
    axi_str_txc_tvalid : out STD_LOGIC;
    axi_str_txc_tready : in STD_LOGIC;
    axi_str_txc_tlast : out STD_LOGIC;
    axi_str_txc_tkeep : out STD_LOGIC_VECTOR ( 3 downto 0 );
    axi_str_txc_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s2mm_prmry_reset_out_n : out STD_LOGIC;
    axi_str_rxd_tvalid : in STD_LOGIC;
    axi_str_rxd_tready : out STD_LOGIC;
    axi_str_rxd_tlast : in STD_LOGIC;
    axi_str_rxd_tkeep : in STD_LOGIC_VECTOR ( 3 downto 0 );
    axi_str_rxd_tdata : in STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  end component design_1_axi_ethernet_0_fifo_0;
  component design_1_rst_clk_wiz_0_200M_1 is
  port (
    slowest_sync_clk : in STD_LOGIC;
    ext_reset_in : in STD_LOGIC;
    aux_reset_in : in STD_LOGIC;
    mb_debug_sys_rst : in STD_LOGIC;
    dcm_locked : in STD_LOGIC;
    mb_reset : out STD_LOGIC;
    bus_struct_reset : out STD_LOGIC_VECTOR ( 0 to 0 );
    peripheral_reset : out STD_LOGIC_VECTOR ( 0 to 0 );
    interconnect_aresetn : out STD_LOGIC_VECTOR ( 0 to 0 );
    peripheral_aresetn : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component design_1_rst_clk_wiz_0_200M_1;
  signal GATE_NOT_0_NOT_OUT : STD_LOGIC;
  signal axi_ethernet_0_fifo_AXI_STR_TXC_TDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal axi_ethernet_0_fifo_AXI_STR_TXC_TKEEP : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal axi_ethernet_0_fifo_AXI_STR_TXC_TLAST : STD_LOGIC;
  signal axi_ethernet_0_fifo_AXI_STR_TXC_TREADY : STD_LOGIC;
  signal axi_ethernet_0_fifo_AXI_STR_TXC_TVALID : STD_LOGIC;
  signal axi_ethernet_0_fifo_AXI_STR_TXD_TDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal axi_ethernet_0_fifo_AXI_STR_TXD_TKEEP : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal axi_ethernet_0_fifo_AXI_STR_TXD_TLAST : STD_LOGIC;
  signal axi_ethernet_0_fifo_AXI_STR_TXD_TREADY : STD_LOGIC;
  signal axi_ethernet_0_fifo_AXI_STR_TXD_TVALID : STD_LOGIC;
  signal axi_ethernet_0_fifo_mm2s_cntrl_reset_out_n : STD_LOGIC;
  signal axi_ethernet_0_fifo_mm2s_prmry_reset_out_n : STD_LOGIC;
  signal axi_ethernet_0_fifo_s2mm_prmry_reset_out_n : STD_LOGIC;
  signal axi_ethernet_0_m_axis_rxd_TDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal axi_ethernet_0_m_axis_rxd_TKEEP : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal axi_ethernet_0_m_axis_rxd_TLAST : STD_LOGIC;
  signal axi_ethernet_0_m_axis_rxd_TREADY : STD_LOGIC;
  signal axi_ethernet_0_m_axis_rxd_TVALID : STD_LOGIC;
  signal axi_ethernet_0_mdio_MDC : STD_LOGIC;
  signal axi_ethernet_0_mdio_MDIO_I : STD_LOGIC;
  signal axi_ethernet_0_mdio_MDIO_O : STD_LOGIC;
  signal axi_ethernet_0_mdio_MDIO_T : STD_LOGIC;
  signal axi_ethernet_0_phy_rst_n : STD_LOGIC_VECTOR ( 0 to 0 );
  signal axi_ethernet_0_sgmii_RXN : STD_LOGIC;
  signal axi_ethernet_0_sgmii_RXP : STD_LOGIC;
  signal axi_ethernet_0_sgmii_TXN : STD_LOGIC;
  signal axi_ethernet_0_sgmii_TXP : STD_LOGIC;
  signal clk_100MHz_1 : STD_LOGIC;
  signal clk_wiz_0_clk_200M : STD_LOGIC;
  signal clk_wiz_0_clk_65M : STD_LOGIC;
  signal diff_clock_rtl_1_1_CLK_N : STD_LOGIC;
  signal diff_clock_rtl_1_1_CLK_P : STD_LOGIC;
  signal fifo_generator_0_dout : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal fifo_generator_0_empty : STD_LOGIC;
  signal fifo_generator_0_full : STD_LOGIC;
  signal myip_ethernet_0_FIFO_RD_EN : STD_LOGIC;
  signal myip_ethernet_0_M00_AXI_ARADDR : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal myip_ethernet_0_M00_AXI_ARPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal myip_ethernet_0_M00_AXI_ARREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal myip_ethernet_0_M00_AXI_ARVALID : STD_LOGIC;
  signal myip_ethernet_0_M00_AXI_AWADDR : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal myip_ethernet_0_M00_AXI_AWPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal myip_ethernet_0_M00_AXI_AWREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal myip_ethernet_0_M00_AXI_AWVALID : STD_LOGIC;
  signal myip_ethernet_0_M00_AXI_BREADY : STD_LOGIC;
  signal myip_ethernet_0_M00_AXI_BRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal myip_ethernet_0_M00_AXI_BVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal myip_ethernet_0_M00_AXI_RDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal myip_ethernet_0_M00_AXI_RREADY : STD_LOGIC;
  signal myip_ethernet_0_M00_AXI_RRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal myip_ethernet_0_M00_AXI_RVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal myip_ethernet_0_M00_AXI_WDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal myip_ethernet_0_M00_AXI_WREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal myip_ethernet_0_M00_AXI_WSTRB : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal myip_ethernet_0_M00_AXI_WVALID : STD_LOGIC;
  signal myip_ethernet_0_axi_periph_M00_AXI_ARADDR : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal myip_ethernet_0_axi_periph_M00_AXI_ARREADY : STD_LOGIC;
  signal myip_ethernet_0_axi_periph_M00_AXI_ARVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal myip_ethernet_0_axi_periph_M00_AXI_AWADDR : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal myip_ethernet_0_axi_periph_M00_AXI_AWREADY : STD_LOGIC;
  signal myip_ethernet_0_axi_periph_M00_AXI_AWVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal myip_ethernet_0_axi_periph_M00_AXI_BREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal myip_ethernet_0_axi_periph_M00_AXI_BRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal myip_ethernet_0_axi_periph_M00_AXI_BVALID : STD_LOGIC;
  signal myip_ethernet_0_axi_periph_M00_AXI_RDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal myip_ethernet_0_axi_periph_M00_AXI_RREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal myip_ethernet_0_axi_periph_M00_AXI_RRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal myip_ethernet_0_axi_periph_M00_AXI_RVALID : STD_LOGIC;
  signal myip_ethernet_0_axi_periph_M00_AXI_WDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal myip_ethernet_0_axi_periph_M00_AXI_WREADY : STD_LOGIC;
  signal myip_ethernet_0_axi_periph_M00_AXI_WSTRB : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal myip_ethernet_0_axi_periph_M00_AXI_WVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal myip_ethernet_0_axi_periph_M01_AXI_ARADDR : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal myip_ethernet_0_axi_periph_M01_AXI_ARREADY : STD_LOGIC;
  signal myip_ethernet_0_axi_periph_M01_AXI_ARVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal myip_ethernet_0_axi_periph_M01_AXI_AWADDR : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal myip_ethernet_0_axi_periph_M01_AXI_AWREADY : STD_LOGIC;
  signal myip_ethernet_0_axi_periph_M01_AXI_AWVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal myip_ethernet_0_axi_periph_M01_AXI_BREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal myip_ethernet_0_axi_periph_M01_AXI_BRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal myip_ethernet_0_axi_periph_M01_AXI_BVALID : STD_LOGIC;
  signal myip_ethernet_0_axi_periph_M01_AXI_RDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal myip_ethernet_0_axi_periph_M01_AXI_RREADY : STD_LOGIC_VECTOR ( 0 to 0 );
  signal myip_ethernet_0_axi_periph_M01_AXI_RRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal myip_ethernet_0_axi_periph_M01_AXI_RVALID : STD_LOGIC;
  signal myip_ethernet_0_axi_periph_M01_AXI_WDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal myip_ethernet_0_axi_periph_M01_AXI_WREADY : STD_LOGIC;
  signal myip_ethernet_0_axi_periph_M01_AXI_WSTRB : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal myip_ethernet_0_axi_periph_M01_AXI_WVALID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal rst_0_1 : STD_LOGIC;
  signal rst_clk_wiz_0_200M_peripheral_aresetn : STD_LOGIC_VECTOR ( 0 to 0 );
  signal write_fifo_0_FIFO_DATO_WR : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal write_fifo_0_FIFO_WR_EN : STD_LOGIC;
  signal NLW_axi_ethernet_0_gt0_pll0lock_out_UNCONNECTED : STD_LOGIC;
  signal NLW_axi_ethernet_0_gt0_pll0outclk_out_UNCONNECTED : STD_LOGIC;
  signal NLW_axi_ethernet_0_gt0_pll0outrefclk_out_UNCONNECTED : STD_LOGIC;
  signal NLW_axi_ethernet_0_gt0_pll0refclklost_out_UNCONNECTED : STD_LOGIC;
  signal NLW_axi_ethernet_0_gt0_pll1outclk_out_UNCONNECTED : STD_LOGIC;
  signal NLW_axi_ethernet_0_gt0_pll1outrefclk_out_UNCONNECTED : STD_LOGIC;
  signal NLW_axi_ethernet_0_gtref_clk_buf_out_UNCONNECTED : STD_LOGIC;
  signal NLW_axi_ethernet_0_gtref_clk_out_UNCONNECTED : STD_LOGIC;
  signal NLW_axi_ethernet_0_interrupt_UNCONNECTED : STD_LOGIC;
  signal NLW_axi_ethernet_0_m_axis_rxs_tlast_UNCONNECTED : STD_LOGIC;
  signal NLW_axi_ethernet_0_m_axis_rxs_tvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_axi_ethernet_0_mac_irq_UNCONNECTED : STD_LOGIC;
  signal NLW_axi_ethernet_0_mmcm_locked_out_UNCONNECTED : STD_LOGIC;
  signal NLW_axi_ethernet_0_pma_reset_out_UNCONNECTED : STD_LOGIC;
  signal NLW_axi_ethernet_0_rxuserclk2_out_UNCONNECTED : STD_LOGIC;
  signal NLW_axi_ethernet_0_rxuserclk_out_UNCONNECTED : STD_LOGIC;
  signal NLW_axi_ethernet_0_userclk2_out_UNCONNECTED : STD_LOGIC;
  signal NLW_axi_ethernet_0_userclk_out_UNCONNECTED : STD_LOGIC;
  signal NLW_axi_ethernet_0_m_axis_rxs_tdata_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_axi_ethernet_0_m_axis_rxs_tkeep_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_axi_ethernet_0_fifo_interrupt_UNCONNECTED : STD_LOGIC;
  signal NLW_clk_wiz_0_clk_120M_UNCONNECTED : STD_LOGIC;
  signal NLW_myip_ethernet_0_m00_axi_error_UNCONNECTED : STD_LOGIC;
  signal NLW_myip_ethernet_0_m00_axi_txn_done_UNCONNECTED : STD_LOGIC;
  signal NLW_rst_clk_wiz_0_200M_mb_reset_UNCONNECTED : STD_LOGIC;
  signal NLW_rst_clk_wiz_0_200M_bus_struct_reset_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_rst_clk_wiz_0_200M_interconnect_aresetn_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_rst_clk_wiz_0_200M_peripheral_reset_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of clk_100MHz : signal is "xilinx.com:signal:clock:1.0 CLK.CLK_100MHZ CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of clk_100MHz : signal is "XIL_INTERFACENAME CLK.CLK_100MHZ, CLK_DOMAIN design_1_clk_100MHz, FREQ_HZ 100000000, INSERT_VIP 0, PHASE 0.000";
  attribute X_INTERFACE_INFO of diff_clock_rtl_1_clk_n : signal is "xilinx.com:interface:diff_clock:1.0 diff_clock_rtl_1 CLK_N";
  attribute X_INTERFACE_PARAMETER of diff_clock_rtl_1_clk_n : signal is "XIL_INTERFACENAME diff_clock_rtl_1, CAN_DEBUG false, FREQ_HZ 100000000";
  attribute X_INTERFACE_INFO of diff_clock_rtl_1_clk_p : signal is "xilinx.com:interface:diff_clock:1.0 diff_clock_rtl_1 CLK_P";
  attribute X_INTERFACE_INFO of mdio_rtl_0_mdc : signal is "xilinx.com:interface:mdio:1.0 mdio_rtl_0 MDC";
  attribute X_INTERFACE_PARAMETER of mdio_rtl_0_mdc : signal is "XIL_INTERFACENAME mdio_rtl_0, CAN_DEBUG false";
  attribute X_INTERFACE_INFO of mdio_rtl_0_mdio_i : signal is "xilinx.com:interface:mdio:1.0 mdio_rtl_0 MDIO_I";
  attribute X_INTERFACE_INFO of mdio_rtl_0_mdio_o : signal is "xilinx.com:interface:mdio:1.0 mdio_rtl_0 MDIO_O";
  attribute X_INTERFACE_INFO of mdio_rtl_0_mdio_t : signal is "xilinx.com:interface:mdio:1.0 mdio_rtl_0 MDIO_T";
  attribute X_INTERFACE_INFO of sgmii_rtl_0_rxn : signal is "xilinx.com:interface:sgmii:1.0 sgmii_rtl_0 RXN";
  attribute X_INTERFACE_INFO of sgmii_rtl_0_rxp : signal is "xilinx.com:interface:sgmii:1.0 sgmii_rtl_0 RXP";
  attribute X_INTERFACE_INFO of sgmii_rtl_0_txn : signal is "xilinx.com:interface:sgmii:1.0 sgmii_rtl_0 TXN";
  attribute X_INTERFACE_INFO of sgmii_rtl_0_txp : signal is "xilinx.com:interface:sgmii:1.0 sgmii_rtl_0 TXP";
  attribute X_INTERFACE_INFO of reset_rtl_0 : signal is "xilinx.com:signal:reset:1.0 RST.RESET_RTL_0 RST";
  attribute X_INTERFACE_PARAMETER of reset_rtl_0 : signal is "XIL_INTERFACENAME RST.RESET_RTL_0, INSERT_VIP 0, POLARITY ACTIVE_LOW";
begin
  axi_ethernet_0_mdio_MDIO_I <= mdio_rtl_0_mdio_i;
  axi_ethernet_0_sgmii_RXN <= sgmii_rtl_0_rxn;
  axi_ethernet_0_sgmii_RXP <= sgmii_rtl_0_rxp;
  clk_100MHz_1 <= clk_100MHz;
  diff_clock_rtl_1_1_CLK_N <= diff_clock_rtl_1_clk_n;
  diff_clock_rtl_1_1_CLK_P <= diff_clock_rtl_1_clk_p;
  mdio_rtl_0_mdc <= axi_ethernet_0_mdio_MDC;
  mdio_rtl_0_mdio_o <= axi_ethernet_0_mdio_MDIO_O;
  mdio_rtl_0_mdio_t <= axi_ethernet_0_mdio_MDIO_T;
  reset_rtl_0(0) <= axi_ethernet_0_phy_rst_n(0);
  rst_0_1 <= rst_0;
  sgmii_rtl_0_txn <= axi_ethernet_0_sgmii_TXN;
  sgmii_rtl_0_txp <= axi_ethernet_0_sgmii_TXP;
GATE_NOT_0: component design_1_GATE_NOT_0_0
     port map (
      NOT_IN => rst_0_1,
      NOT_OUT => GATE_NOT_0_NOT_OUT
    );
axi_ethernet_0: component design_1_axi_ethernet_0_0
     port map (
      axi_rxd_arstn => axi_ethernet_0_fifo_s2mm_prmry_reset_out_n,
      axi_rxs_arstn => axi_ethernet_0_fifo_s2mm_prmry_reset_out_n,
      axi_txc_arstn => axi_ethernet_0_fifo_mm2s_cntrl_reset_out_n,
      axi_txd_arstn => axi_ethernet_0_fifo_mm2s_prmry_reset_out_n,
      axis_clk => clk_wiz_0_clk_200M,
      gt0_pll0lock_out => NLW_axi_ethernet_0_gt0_pll0lock_out_UNCONNECTED,
      gt0_pll0outclk_out => NLW_axi_ethernet_0_gt0_pll0outclk_out_UNCONNECTED,
      gt0_pll0outrefclk_out => NLW_axi_ethernet_0_gt0_pll0outrefclk_out_UNCONNECTED,
      gt0_pll0refclklost_out => NLW_axi_ethernet_0_gt0_pll0refclklost_out_UNCONNECTED,
      gt0_pll1outclk_out => NLW_axi_ethernet_0_gt0_pll1outclk_out_UNCONNECTED,
      gt0_pll1outrefclk_out => NLW_axi_ethernet_0_gt0_pll1outrefclk_out_UNCONNECTED,
      gtref_clk_buf_out => NLW_axi_ethernet_0_gtref_clk_buf_out_UNCONNECTED,
      gtref_clk_out => NLW_axi_ethernet_0_gtref_clk_out_UNCONNECTED,
      interrupt => NLW_axi_ethernet_0_interrupt_UNCONNECTED,
      m_axis_rxd_tdata(31 downto 0) => axi_ethernet_0_m_axis_rxd_TDATA(31 downto 0),
      m_axis_rxd_tkeep(3 downto 0) => axi_ethernet_0_m_axis_rxd_TKEEP(3 downto 0),
      m_axis_rxd_tlast => axi_ethernet_0_m_axis_rxd_TLAST,
      m_axis_rxd_tready => axi_ethernet_0_m_axis_rxd_TREADY,
      m_axis_rxd_tvalid => axi_ethernet_0_m_axis_rxd_TVALID,
      m_axis_rxs_tdata(31 downto 0) => NLW_axi_ethernet_0_m_axis_rxs_tdata_UNCONNECTED(31 downto 0),
      m_axis_rxs_tkeep(3 downto 0) => NLW_axi_ethernet_0_m_axis_rxs_tkeep_UNCONNECTED(3 downto 0),
      m_axis_rxs_tlast => NLW_axi_ethernet_0_m_axis_rxs_tlast_UNCONNECTED,
      m_axis_rxs_tready => '1',
      m_axis_rxs_tvalid => NLW_axi_ethernet_0_m_axis_rxs_tvalid_UNCONNECTED,
      mac_irq => NLW_axi_ethernet_0_mac_irq_UNCONNECTED,
      mdio_mdc => axi_ethernet_0_mdio_MDC,
      mdio_mdio_i => axi_ethernet_0_mdio_MDIO_I,
      mdio_mdio_o => axi_ethernet_0_mdio_MDIO_O,
      mdio_mdio_t => axi_ethernet_0_mdio_MDIO_T,
      mgt_clk_clk_n => diff_clock_rtl_1_1_CLK_N,
      mgt_clk_clk_p => diff_clock_rtl_1_1_CLK_P,
      mmcm_locked_out => NLW_axi_ethernet_0_mmcm_locked_out_UNCONNECTED,
      phy_rst_n(0) => axi_ethernet_0_phy_rst_n(0),
      pma_reset_out => NLW_axi_ethernet_0_pma_reset_out_UNCONNECTED,
      ref_clk => clk_wiz_0_clk_200M,
      rxuserclk2_out => NLW_axi_ethernet_0_rxuserclk2_out_UNCONNECTED,
      rxuserclk_out => NLW_axi_ethernet_0_rxuserclk_out_UNCONNECTED,
      s_axi_araddr(17 downto 0) => myip_ethernet_0_axi_periph_M00_AXI_ARADDR(17 downto 0),
      s_axi_arready => myip_ethernet_0_axi_periph_M00_AXI_ARREADY,
      s_axi_arvalid => myip_ethernet_0_axi_periph_M00_AXI_ARVALID(0),
      s_axi_awaddr(17 downto 0) => myip_ethernet_0_axi_periph_M00_AXI_AWADDR(17 downto 0),
      s_axi_awready => myip_ethernet_0_axi_periph_M00_AXI_AWREADY,
      s_axi_awvalid => myip_ethernet_0_axi_periph_M00_AXI_AWVALID(0),
      s_axi_bready => myip_ethernet_0_axi_periph_M00_AXI_BREADY(0),
      s_axi_bresp(1 downto 0) => myip_ethernet_0_axi_periph_M00_AXI_BRESP(1 downto 0),
      s_axi_bvalid => myip_ethernet_0_axi_periph_M00_AXI_BVALID,
      s_axi_lite_clk => clk_wiz_0_clk_200M,
      s_axi_lite_resetn => rst_clk_wiz_0_200M_peripheral_aresetn(0),
      s_axi_rdata(31 downto 0) => myip_ethernet_0_axi_periph_M00_AXI_RDATA(31 downto 0),
      s_axi_rready => myip_ethernet_0_axi_periph_M00_AXI_RREADY(0),
      s_axi_rresp(1 downto 0) => myip_ethernet_0_axi_periph_M00_AXI_RRESP(1 downto 0),
      s_axi_rvalid => myip_ethernet_0_axi_periph_M00_AXI_RVALID,
      s_axi_wdata(31 downto 0) => myip_ethernet_0_axi_periph_M00_AXI_WDATA(31 downto 0),
      s_axi_wready => myip_ethernet_0_axi_periph_M00_AXI_WREADY,
      s_axi_wstrb(3 downto 0) => myip_ethernet_0_axi_periph_M00_AXI_WSTRB(3 downto 0),
      s_axi_wvalid => myip_ethernet_0_axi_periph_M00_AXI_WVALID(0),
      s_axis_txc_tdata(31 downto 0) => axi_ethernet_0_fifo_AXI_STR_TXC_TDATA(31 downto 0),
      s_axis_txc_tkeep(3 downto 0) => axi_ethernet_0_fifo_AXI_STR_TXC_TKEEP(3 downto 0),
      s_axis_txc_tlast => axi_ethernet_0_fifo_AXI_STR_TXC_TLAST,
      s_axis_txc_tready => axi_ethernet_0_fifo_AXI_STR_TXC_TREADY,
      s_axis_txc_tvalid => axi_ethernet_0_fifo_AXI_STR_TXC_TVALID,
      s_axis_txd_tdata(31 downto 0) => axi_ethernet_0_fifo_AXI_STR_TXD_TDATA(31 downto 0),
      s_axis_txd_tkeep(3 downto 0) => axi_ethernet_0_fifo_AXI_STR_TXD_TKEEP(3 downto 0),
      s_axis_txd_tlast => axi_ethernet_0_fifo_AXI_STR_TXD_TLAST,
      s_axis_txd_tready => axi_ethernet_0_fifo_AXI_STR_TXD_TREADY,
      s_axis_txd_tvalid => axi_ethernet_0_fifo_AXI_STR_TXD_TVALID,
      sgmii_rxn => axi_ethernet_0_sgmii_RXN,
      sgmii_rxp => axi_ethernet_0_sgmii_RXP,
      sgmii_txn => axi_ethernet_0_sgmii_TXN,
      sgmii_txp => axi_ethernet_0_sgmii_TXP,
      signal_detect => '1',
      userclk2_out => NLW_axi_ethernet_0_userclk2_out_UNCONNECTED,
      userclk_out => NLW_axi_ethernet_0_userclk_out_UNCONNECTED
    );
axi_ethernet_0_fifo: component design_1_axi_ethernet_0_fifo_0
     port map (
      axi_str_rxd_tdata(31 downto 0) => axi_ethernet_0_m_axis_rxd_TDATA(31 downto 0),
      axi_str_rxd_tkeep(3 downto 0) => axi_ethernet_0_m_axis_rxd_TKEEP(3 downto 0),
      axi_str_rxd_tlast => axi_ethernet_0_m_axis_rxd_TLAST,
      axi_str_rxd_tready => axi_ethernet_0_m_axis_rxd_TREADY,
      axi_str_rxd_tvalid => axi_ethernet_0_m_axis_rxd_TVALID,
      axi_str_txc_tdata(31 downto 0) => axi_ethernet_0_fifo_AXI_STR_TXC_TDATA(31 downto 0),
      axi_str_txc_tkeep(3 downto 0) => axi_ethernet_0_fifo_AXI_STR_TXC_TKEEP(3 downto 0),
      axi_str_txc_tlast => axi_ethernet_0_fifo_AXI_STR_TXC_TLAST,
      axi_str_txc_tready => axi_ethernet_0_fifo_AXI_STR_TXC_TREADY,
      axi_str_txc_tvalid => axi_ethernet_0_fifo_AXI_STR_TXC_TVALID,
      axi_str_txd_tdata(31 downto 0) => axi_ethernet_0_fifo_AXI_STR_TXD_TDATA(31 downto 0),
      axi_str_txd_tkeep(3 downto 0) => axi_ethernet_0_fifo_AXI_STR_TXD_TKEEP(3 downto 0),
      axi_str_txd_tlast => axi_ethernet_0_fifo_AXI_STR_TXD_TLAST,
      axi_str_txd_tready => axi_ethernet_0_fifo_AXI_STR_TXD_TREADY,
      axi_str_txd_tvalid => axi_ethernet_0_fifo_AXI_STR_TXD_TVALID,
      interrupt => NLW_axi_ethernet_0_fifo_interrupt_UNCONNECTED,
      mm2s_cntrl_reset_out_n => axi_ethernet_0_fifo_mm2s_cntrl_reset_out_n,
      mm2s_prmry_reset_out_n => axi_ethernet_0_fifo_mm2s_prmry_reset_out_n,
      s2mm_prmry_reset_out_n => axi_ethernet_0_fifo_s2mm_prmry_reset_out_n,
      s_axi_aclk => clk_wiz_0_clk_200M,
      s_axi_araddr(31 downto 0) => myip_ethernet_0_axi_periph_M01_AXI_ARADDR(31 downto 0),
      s_axi_aresetn => rst_clk_wiz_0_200M_peripheral_aresetn(0),
      s_axi_arready => myip_ethernet_0_axi_periph_M01_AXI_ARREADY,
      s_axi_arvalid => myip_ethernet_0_axi_periph_M01_AXI_ARVALID(0),
      s_axi_awaddr(31 downto 0) => myip_ethernet_0_axi_periph_M01_AXI_AWADDR(31 downto 0),
      s_axi_awready => myip_ethernet_0_axi_periph_M01_AXI_AWREADY,
      s_axi_awvalid => myip_ethernet_0_axi_periph_M01_AXI_AWVALID(0),
      s_axi_bready => myip_ethernet_0_axi_periph_M01_AXI_BREADY(0),
      s_axi_bresp(1 downto 0) => myip_ethernet_0_axi_periph_M01_AXI_BRESP(1 downto 0),
      s_axi_bvalid => myip_ethernet_0_axi_periph_M01_AXI_BVALID,
      s_axi_rdata(31 downto 0) => myip_ethernet_0_axi_periph_M01_AXI_RDATA(31 downto 0),
      s_axi_rready => myip_ethernet_0_axi_periph_M01_AXI_RREADY(0),
      s_axi_rresp(1 downto 0) => myip_ethernet_0_axi_periph_M01_AXI_RRESP(1 downto 0),
      s_axi_rvalid => myip_ethernet_0_axi_periph_M01_AXI_RVALID,
      s_axi_wdata(31 downto 0) => myip_ethernet_0_axi_periph_M01_AXI_WDATA(31 downto 0),
      s_axi_wready => myip_ethernet_0_axi_periph_M01_AXI_WREADY,
      s_axi_wstrb(3 downto 0) => myip_ethernet_0_axi_periph_M01_AXI_WSTRB(3 downto 0),
      s_axi_wvalid => myip_ethernet_0_axi_periph_M01_AXI_WVALID(0)
    );
clk_wiz_0: component design_1_clk_wiz_0_0
     port map (
      clk_120M => NLW_clk_wiz_0_clk_120M_UNCONNECTED,
      clk_200M => clk_wiz_0_clk_200M,
      clk_65M => clk_wiz_0_clk_65M,
      clk_in1 => clk_100MHz_1,
      reset => rst_0_1
    );
fifo_generator_0: component design_1_fifo_generator_0_1
     port map (
      din(31 downto 0) => write_fifo_0_FIFO_DATO_WR(31 downto 0),
      dout(31 downto 0) => fifo_generator_0_dout(31 downto 0),
      empty => fifo_generator_0_empty,
      full => fifo_generator_0_full,
      rd_clk => clk_wiz_0_clk_200M,
      rd_en => myip_ethernet_0_FIFO_RD_EN,
      rst => rst_0_1,
      wr_clk => clk_wiz_0_clk_65M,
      wr_en => write_fifo_0_FIFO_WR_EN
    );
myip_ethernet_0: component design_1_myip_ethernet_0_2
     port map (
      CLK_read => '0',
      FIFO_Empty => fifo_generator_0_empty,
      FIFO_RD_EN => myip_ethernet_0_FIFO_RD_EN,
      FIFO_dato_Read(31 downto 0) => fifo_generator_0_dout(31 downto 0),
      RST => GATE_NOT_0_NOT_OUT,
      m00_axi_aclk => clk_wiz_0_clk_200M,
      m00_axi_araddr(31 downto 0) => myip_ethernet_0_M00_AXI_ARADDR(31 downto 0),
      m00_axi_aresetn => GATE_NOT_0_NOT_OUT,
      m00_axi_arprot(2 downto 0) => myip_ethernet_0_M00_AXI_ARPROT(2 downto 0),
      m00_axi_arready => myip_ethernet_0_M00_AXI_ARREADY(0),
      m00_axi_arvalid => myip_ethernet_0_M00_AXI_ARVALID,
      m00_axi_awaddr(31 downto 0) => myip_ethernet_0_M00_AXI_AWADDR(31 downto 0),
      m00_axi_awprot(2 downto 0) => myip_ethernet_0_M00_AXI_AWPROT(2 downto 0),
      m00_axi_awready => myip_ethernet_0_M00_AXI_AWREADY(0),
      m00_axi_awvalid => myip_ethernet_0_M00_AXI_AWVALID,
      m00_axi_bready => myip_ethernet_0_M00_AXI_BREADY,
      m00_axi_bresp(1 downto 0) => myip_ethernet_0_M00_AXI_BRESP(1 downto 0),
      m00_axi_bvalid => myip_ethernet_0_M00_AXI_BVALID(0),
      m00_axi_error => NLW_myip_ethernet_0_m00_axi_error_UNCONNECTED,
      m00_axi_init_axi_txn => '0',
      m00_axi_rdata(31 downto 0) => myip_ethernet_0_M00_AXI_RDATA(31 downto 0),
      m00_axi_rready => myip_ethernet_0_M00_AXI_RREADY,
      m00_axi_rresp(1 downto 0) => myip_ethernet_0_M00_AXI_RRESP(1 downto 0),
      m00_axi_rvalid => myip_ethernet_0_M00_AXI_RVALID(0),
      m00_axi_txn_done => NLW_myip_ethernet_0_m00_axi_txn_done_UNCONNECTED,
      m00_axi_wdata(31 downto 0) => myip_ethernet_0_M00_AXI_WDATA(31 downto 0),
      m00_axi_wready => myip_ethernet_0_M00_AXI_WREADY(0),
      m00_axi_wstrb(3 downto 0) => myip_ethernet_0_M00_AXI_WSTRB(3 downto 0),
      m00_axi_wvalid => myip_ethernet_0_M00_AXI_WVALID
    );
myip_ethernet_0_axi_periph: entity work.design_1_myip_ethernet_0_axi_periph_3
     port map (
      ACLK => clk_wiz_0_clk_200M,
      ARESETN => rst_clk_wiz_0_200M_peripheral_aresetn(0),
      M00_ACLK => clk_wiz_0_clk_200M,
      M00_ARESETN => rst_clk_wiz_0_200M_peripheral_aresetn(0),
      M00_AXI_araddr(31 downto 0) => myip_ethernet_0_axi_periph_M00_AXI_ARADDR(31 downto 0),
      M00_AXI_arready(0) => myip_ethernet_0_axi_periph_M00_AXI_ARREADY,
      M00_AXI_arvalid(0) => myip_ethernet_0_axi_periph_M00_AXI_ARVALID(0),
      M00_AXI_awaddr(31 downto 0) => myip_ethernet_0_axi_periph_M00_AXI_AWADDR(31 downto 0),
      M00_AXI_awready(0) => myip_ethernet_0_axi_periph_M00_AXI_AWREADY,
      M00_AXI_awvalid(0) => myip_ethernet_0_axi_periph_M00_AXI_AWVALID(0),
      M00_AXI_bready(0) => myip_ethernet_0_axi_periph_M00_AXI_BREADY(0),
      M00_AXI_bresp(1 downto 0) => myip_ethernet_0_axi_periph_M00_AXI_BRESP(1 downto 0),
      M00_AXI_bvalid(0) => myip_ethernet_0_axi_periph_M00_AXI_BVALID,
      M00_AXI_rdata(31 downto 0) => myip_ethernet_0_axi_periph_M00_AXI_RDATA(31 downto 0),
      M00_AXI_rready(0) => myip_ethernet_0_axi_periph_M00_AXI_RREADY(0),
      M00_AXI_rresp(1 downto 0) => myip_ethernet_0_axi_periph_M00_AXI_RRESP(1 downto 0),
      M00_AXI_rvalid(0) => myip_ethernet_0_axi_periph_M00_AXI_RVALID,
      M00_AXI_wdata(31 downto 0) => myip_ethernet_0_axi_periph_M00_AXI_WDATA(31 downto 0),
      M00_AXI_wready(0) => myip_ethernet_0_axi_periph_M00_AXI_WREADY,
      M00_AXI_wstrb(3 downto 0) => myip_ethernet_0_axi_periph_M00_AXI_WSTRB(3 downto 0),
      M00_AXI_wvalid(0) => myip_ethernet_0_axi_periph_M00_AXI_WVALID(0),
      M01_ACLK => clk_wiz_0_clk_200M,
      M01_ARESETN => rst_clk_wiz_0_200M_peripheral_aresetn(0),
      M01_AXI_araddr(31 downto 0) => myip_ethernet_0_axi_periph_M01_AXI_ARADDR(31 downto 0),
      M01_AXI_arready(0) => myip_ethernet_0_axi_periph_M01_AXI_ARREADY,
      M01_AXI_arvalid(0) => myip_ethernet_0_axi_periph_M01_AXI_ARVALID(0),
      M01_AXI_awaddr(31 downto 0) => myip_ethernet_0_axi_periph_M01_AXI_AWADDR(31 downto 0),
      M01_AXI_awready(0) => myip_ethernet_0_axi_periph_M01_AXI_AWREADY,
      M01_AXI_awvalid(0) => myip_ethernet_0_axi_periph_M01_AXI_AWVALID(0),
      M01_AXI_bready(0) => myip_ethernet_0_axi_periph_M01_AXI_BREADY(0),
      M01_AXI_bresp(1 downto 0) => myip_ethernet_0_axi_periph_M01_AXI_BRESP(1 downto 0),
      M01_AXI_bvalid(0) => myip_ethernet_0_axi_periph_M01_AXI_BVALID,
      M01_AXI_rdata(31 downto 0) => myip_ethernet_0_axi_periph_M01_AXI_RDATA(31 downto 0),
      M01_AXI_rready(0) => myip_ethernet_0_axi_periph_M01_AXI_RREADY(0),
      M01_AXI_rresp(1 downto 0) => myip_ethernet_0_axi_periph_M01_AXI_RRESP(1 downto 0),
      M01_AXI_rvalid(0) => myip_ethernet_0_axi_periph_M01_AXI_RVALID,
      M01_AXI_wdata(31 downto 0) => myip_ethernet_0_axi_periph_M01_AXI_WDATA(31 downto 0),
      M01_AXI_wready(0) => myip_ethernet_0_axi_periph_M01_AXI_WREADY,
      M01_AXI_wstrb(3 downto 0) => myip_ethernet_0_axi_periph_M01_AXI_WSTRB(3 downto 0),
      M01_AXI_wvalid(0) => myip_ethernet_0_axi_periph_M01_AXI_WVALID(0),
      S00_ACLK => clk_wiz_0_clk_200M,
      S00_ARESETN => rst_clk_wiz_0_200M_peripheral_aresetn(0),
      S00_AXI_araddr(31 downto 0) => myip_ethernet_0_M00_AXI_ARADDR(31 downto 0),
      S00_AXI_arprot(2 downto 0) => myip_ethernet_0_M00_AXI_ARPROT(2 downto 0),
      S00_AXI_arready(0) => myip_ethernet_0_M00_AXI_ARREADY(0),
      S00_AXI_arvalid(0) => myip_ethernet_0_M00_AXI_ARVALID,
      S00_AXI_awaddr(31 downto 0) => myip_ethernet_0_M00_AXI_AWADDR(31 downto 0),
      S00_AXI_awprot(2 downto 0) => myip_ethernet_0_M00_AXI_AWPROT(2 downto 0),
      S00_AXI_awready(0) => myip_ethernet_0_M00_AXI_AWREADY(0),
      S00_AXI_awvalid(0) => myip_ethernet_0_M00_AXI_AWVALID,
      S00_AXI_bready(0) => myip_ethernet_0_M00_AXI_BREADY,
      S00_AXI_bresp(1 downto 0) => myip_ethernet_0_M00_AXI_BRESP(1 downto 0),
      S00_AXI_bvalid(0) => myip_ethernet_0_M00_AXI_BVALID(0),
      S00_AXI_rdata(31 downto 0) => myip_ethernet_0_M00_AXI_RDATA(31 downto 0),
      S00_AXI_rready(0) => myip_ethernet_0_M00_AXI_RREADY,
      S00_AXI_rresp(1 downto 0) => myip_ethernet_0_M00_AXI_RRESP(1 downto 0),
      S00_AXI_rvalid(0) => myip_ethernet_0_M00_AXI_RVALID(0),
      S00_AXI_wdata(31 downto 0) => myip_ethernet_0_M00_AXI_WDATA(31 downto 0),
      S00_AXI_wready(0) => myip_ethernet_0_M00_AXI_WREADY(0),
      S00_AXI_wstrb(3 downto 0) => myip_ethernet_0_M00_AXI_WSTRB(3 downto 0),
      S00_AXI_wvalid(0) => myip_ethernet_0_M00_AXI_WVALID
    );
rst_clk_wiz_0_200M: component design_1_rst_clk_wiz_0_200M_1
     port map (
      aux_reset_in => '1',
      bus_struct_reset(0) => NLW_rst_clk_wiz_0_200M_bus_struct_reset_UNCONNECTED(0),
      dcm_locked => '1',
      ext_reset_in => rst_0_1,
      interconnect_aresetn(0) => NLW_rst_clk_wiz_0_200M_interconnect_aresetn_UNCONNECTED(0),
      mb_debug_sys_rst => '0',
      mb_reset => NLW_rst_clk_wiz_0_200M_mb_reset_UNCONNECTED,
      peripheral_aresetn(0) => rst_clk_wiz_0_200M_peripheral_aresetn(0),
      peripheral_reset(0) => NLW_rst_clk_wiz_0_200M_peripheral_reset_UNCONNECTED(0),
      slowest_sync_clk => clk_wiz_0_clk_200M
    );
write_fifo_0: component design_1_write_fifo_0_1
     port map (
      FIFO_DATO_WR(31 downto 0) => write_fifo_0_FIFO_DATO_WR(31 downto 0),
      FIFO_FULL => fifo_generator_0_full,
      FIFO_WR_EN => write_fifo_0_FIFO_WR_EN,
      RST => rst_0_1,
      clk_Write => clk_wiz_0_clk_65M
    );
end STRUCTURE;
