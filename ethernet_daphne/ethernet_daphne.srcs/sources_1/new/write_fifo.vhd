----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06.07.2021 14:14:01
-- Design Name: 
-- Module Name: write_fifo - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity write_fifo is
  Port (clk_Write   : in std_logic;
        RST   : in std_logic;
        
        --%%%%%%%%%%%%%%%%
        --    SIGNALS FIFO
        
        FIFO_FULL : in std_logic;
        FIFO_WR_EN: out std_logic;
        FIFO_DATO_WR: out std_logic_vector(31 downto 0)
                );
end write_fifo;

architecture Behavioral of write_fifo is

signal count_fifo : std_logic_vector(31 downto 0);
begin

process(CLK_Write, RST)
begin
  if RST = '1' then
    count_fifo <= (others => '0');
    FIFO_WR_EN <= '0';
  elsif clk_write = '1' and clk_write'event then
    if FIFO_FULL = '0' then
       FIFO_WR_EN <= '1';
       FIFO_DATO_WR <= count_fifo;
       count_fifo <= count_fifo + x"00000001";
    else
      FIFO_WR_EN <= '0';
    end if;
 end if;

end process;         
    
end Behavioral;
